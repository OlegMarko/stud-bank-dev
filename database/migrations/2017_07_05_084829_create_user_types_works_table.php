<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypesWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types_works', function (Blueprint $table) {
            $table->integer('user_id')->comment('ид пользователя');
            $table->integer('type_work_id')->comment('ид типа работы');

            $table->primary(['user_id', 'type_work_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types_works');
    }
}
