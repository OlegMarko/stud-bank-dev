<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Main\Banner;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable()->comment('Поле изображения');
            $table->string('title')->nullable()->comment('Поле подписи в баннере');
            $table->string('url')->nullable()->comment('Поле ссылки');
            $table->string('button_text')->nullable()->comment('Поле для текста кнопки');
            $table->integer('sort')->nullable()->comment('Поле сортировка');
            $table->tinyInteger('position')->default(Banner::ALL_USERS)->comment('Поле позиция банера');
            $table->tinyInteger('status')->default(Banner::DISPLAY_TRUE)->comment('Поле статуса банера вкл. или выкл.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
