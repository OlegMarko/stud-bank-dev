<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Main\AcademicLevel;

class CreateAcademicLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('Название учебной степени');
            $table->integer('sort')->nullable()->comment('Поле сортировка');
            $table->tinyInteger('status')->default(AcademicLevel::DISPLAY_TRUE)->comment('Поле статуса учебной степени вкл. или выкл.');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_levels');
    }
}
