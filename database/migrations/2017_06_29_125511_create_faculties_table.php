<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Main\Faculty;

class CreateFacultiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faculties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('Название факультета');
            $table->integer('sort')->nullable()->comment('Поле сортировка');
            $table->tinyInteger('status')->default(Faculty::DISPLAY_TRUE)->comment('Поле статуса факультета вкл. или выкл.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faculties');
    }
}
