<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Main\Discipline;

class CreateDisciplinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('Название дисциплины');
            $table->tinyInteger('sort')->nullable()->comment('Поле сортировки');
            $table->tinyInteger('status')->default(Discipline::DISPLAY_TRUE)->comment('Поле статуса дисциплины вкл. или выкл.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disciplines');
    }
}
