<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendUsersTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname')->nullable()
                ->comment('Фамилия пользователя');
            $table->string('login', 150)
                ->comment('Логин - account name');
            $table->string('phone', 20);
            $table->string('city', 255)->nullable();
            $table->string('university', 255)->nullable()
                ->comment('Название ВУЗа');
            $table->string('specialty', 255)->nullable()
                ->comment('Специальность');
            $table->unsignedSmallInteger('study_finish_date')->nullable()
                ->comment('Дата окончания обучения');
            $table->tinyInteger('is_agent')->default(\App\User::IS_AGENT_FALSE)
                ->comment('Поле "Я агент при регистрации"');
            $table->tinyInteger('mail_subscription')->default(\App\User::NEWS_SUBSCRIPTION_FALSE)
                ->comment('Чекбокс при регистрации "Я хочу получать уведомления и новости сайта на почту"');
            $table->tinyInteger('mail_message_notify')->default(\App\User::NOTIFY_SUBSCRIPTION_FALSE)
                ->comment('Чекбокс при регистрации "Оповещать меня по почте, когда отвечают на проект или пишут личное сообщение"');
            $table->tinyInteger('role')->default(\App\User::ROLE_STUDENT)->comment('Роль пользователя [1 - cтудент, 2 - автор, 3 - админ, 4 - менеджер]');

            $table->unique('login');
            $table->unique('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['surname', 'login', 'phone', 'city', 'university', 'specialty', 'study_finish_date', 'is_agent', 'mail_subscription', 'mail_message_notify', 'role']);
        });
    }
}
