<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('Поле название шаблона ответа');;
            $table->text('text')->comment('Поле текста шаблона ответа');;
            $table->integer('type_work_id')->comment('Поле типа работы');;
            $table->integer('user_id')->comment('Ид пользователя');;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_responses');
    }
}
