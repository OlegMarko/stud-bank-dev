<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Main\TypeWork;

class CreateTypeWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('Название типа работ');
            $table->integer('sort')->nullable()->comment('Поле сортировка');
            $table->tinyInteger('status')->default(TypeWork::DISPLAY_TRUE)->comment('Поле статуса типа работ вкл. или выкл.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_works');
    }
}
