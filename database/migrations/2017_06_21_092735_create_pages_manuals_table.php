<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Main\Manual;

class CreatePagesManualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages_manuals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('h2')->comment('Поле для тега h2');
            $table->string('slug')->comment('Поле для cсылки');
            $table->text('text')->comment('Поле текста страницы');
            $table->integer('sort')->nullable()->comment('Поле сортировки страницы');
            $table->tinyInteger('in_menu')->default(Manual::DISPLAY_FALSE)->comment('Поле нахождение страниц в меню Да нет');
            $table->tinyInteger('status')->default(Manual::DISPLAY_FALSE)->comment('Поле статуса страницы Вкл. или Выкл');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_instrukcii');
    }
}
