<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Main\Science;

class CreateSciencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sciences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('Название науки');
            $table->tinyInteger('sort')->nullable()->comment('Поле сортировки');
            $table->tinyInteger('status')->default(Science::DISPLAY_TRUE)->comment('Поле статуса науки вкл. или выкл.');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sciences');
    }
}
