<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendUsersTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('birthday')->nullable();
            $table->date('session_begin_date')->nullable();
            $table->string('faculty')->nullable();
            $table->mediumText('bio')->nullable();
//            $table->dropColumn('phone');
        });

        Schema::create('user_phones', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('phone', 20);
            $table->primary(['user_id', 'phone']);
        });

        Schema::create('user_avatars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('avatar_path',255);
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_phones');
        Schema::dropIfExists('user_avatars');
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone', 20);
            $table->dropColumn(['birthday', 'session_begin_date', 'faculty']);
        });
    }
}
