<?php

use Illuminate\Database\Seeder;
use App\Models\Main\Town;

class TownsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('towns')->insert([
            'title' => 'Test title#1',
            'status' => Town::DISPLAY_TRUE
        ]);
    }
}
