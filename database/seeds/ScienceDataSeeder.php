<?php

use Illuminate\Database\Seeder;
use App\Models\Main\Science;

class ScienceDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sciences')->insert([
            'title' => 'Test title#1',
            'status' => Science::DISPLAY_TRUE
        ]);
    }
}
