<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Main\Speciality;

class SpecialityDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        DB::table('specialties')->insert([
            'title' => 'Test title#1',
            'status' => Speciality::DISPLAY_TRUE
        ]);
    }
}
