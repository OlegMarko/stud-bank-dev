<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Main\Manual;

class ManualDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages_manuals')->insert([
            'h2' => 'Test',
            'slug' => '/manual',
            'text' =>'<p>test</p>',
            'sort' => 1,
            'in_menu' => Manual::DISPLAY_TRUE,
            'status' => 1,
        ]);

        DB::table('pages_manuals')->insert([
            'h2' => 'Test',
            'slug' => '/manual/new',
            'text' =>'<p>test-new</p>',
            'sort' => 1,
            'in_menu' => Manual::DISPLAY_TRUE,
            'status' => 1,
        ]);
    }
}
