<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiteSettingsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'facebook_footer_link' => 'facebook-val',
            'twitter_footer_link'  => 'twitter-val',
            'google_footer_link'   => 'google-val',
            'vk_footer_link'       => 'vk-val'
        ];

        foreach($array as $key => $item){
            DB::table('site_settings')->insert([
                'key' => $key,
                'value' => $item
            ]);
        }
    }
}
