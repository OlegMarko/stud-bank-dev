<?php

use Illuminate\Database\Seeder;
use App\Models\Main\AcademicLevel;

class AcademicLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academic_levels')->insert([
            'title' => 'Студент',
            'status' => AcademicLevel::DISPLAY_TRUE
        ]);
    }
}
