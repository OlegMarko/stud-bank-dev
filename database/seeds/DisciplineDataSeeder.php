<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Main\Discipline;


class DisciplineDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            'title' => 'Test title#1',
            'status' => Discipline::DISPLAY_TRUE
        ]);
    }
}
