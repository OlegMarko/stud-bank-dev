<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Main\TypeWork;

class TypeWorkDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        DB::table('type_works')->insert([
            'title' => 'Test title#1',
            'status' => TypeWork::DISPLAY_TRUE
        ]);
    }
}
