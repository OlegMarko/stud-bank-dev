<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SeoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seo')->insert([
            'page_url' => '/',
            'type' => 1,
            'title' => 'Студбиржа главная',
            'description' => 'Студбиржа описание',
            'keywords' => 'студ, биржа, слова',
        ]);
    }
}
