<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(AcademicLevelSeeder::class);
         $this->call(BannersTableSeeder::class);
         $this->call(DisciplineDataSeeder::class);
         $this->call(FacultySeeder::class);
         $this->call(FeedBackDataSeeder::class);
         $this->call(ManualDataSeeder::class);
         $this->call(ScienceDataSeeder::class);
         $this->call(SeoDataSeeder::class);
         $this->call(SiteSettingsDataSeeder::class);
         $this->call(SubjectDataSeeder::class);
         $this->call(SpecialityDataSeeder::class);
         $this->call(SubjectDataSeeder::class);
         $this->call(TownsDataSeeder::class);
         $this->call(TypeWorkDataSeeder::class);
         $this->call(UserDataSeeder::class);
    }
}
