<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Main\Banner;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            'image' => '',
            'url' => 'url',
            'title' => 'Test',
            'button_text' => 'button_text',
            'sort' => '1',
            'position' => Banner::ALL_USERS,
            'status' => Banner::DISPLAY_TRUE,
        ]);
    }
}
