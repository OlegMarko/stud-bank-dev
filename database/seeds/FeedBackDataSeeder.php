<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeedBackDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=5;$i++){
            DB::table('feedbacks')->insert([
                'text' => 'Текст слайда №'.$i,
                'name' => 'Автор №'.$i,
                'image' => 'uploads/photo-m.png',
                'sort' => 0,
                'status' => 1,
            ]);
        }
    }
}
