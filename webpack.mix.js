const {mix} = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/authApp.js', 'public/js')
    .js('resources/assets/js/pages/author-edit-page.js', 'public/js')
    .js('resources/assets/js/pages/customer-edit-page.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')

  .copyDirectory('resources/assets/css', 'public/css')
  .copyDirectory('resources/assets/js', 'public/js')
  .copyDirectory('resources/assets/js/appFront', 'public/js/appFront')
  .copyDirectory('resources/assets/fonts', 'public/fonts')
  .copyDirectory('resources/assets/img', 'public/img');

