/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 40);
/******/ })
/************************************************************************/
/******/ ({

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(41);


/***/ }),

/***/ 41:
/***/ (function(module, exports) {

var Child = {
    template: '<div>Пользовательский компонент!</div>'
};

var profileEdit = new Vue({
    el: '.profileEdit',
    data: {
        disabledButton: false,
        editTemplate: false,
        phone: '',

        qwe: '',
        allSelected: false,

        completeMain: false,
        completeEdit: false,
        completeEducation: false,
        completePassword: false,
        completeContact: false,
        completeTypeWork: false,
        completeAddTemplate: false,
        completeSubjects: false,

        templates: [],
        templateId: '',

        messageComplete: '',
        totalField: 0,
        cntField: 0,
        flagAddField: true,
        sampleElement: ' <div class="form-group col-xs-12 col-md-4"><label>Телефон</label> ' + '<div class="row">' + '<div class="col-xs-12">' + '<input type="text" placeholder="Телефон" name="phone_user[]" value="" class="form-control phoneNumber"></div></div></div>'
    },
    created: function created() {
        this.totalField = $('.phoneNumber').length;
    },
    mounted: function mounted() {
        this.templates = JSON.parse($(this.$el).attr('data-templates'));
    },

    components: {
        // <my-component> будет доступен только в шаблоне родителя

        'my-component': Child
    },
    methods: {
        deleteTemplateState: function deleteTemplateState(id) {
            var _this = this;

            this.templates.forEach(function (template, index) {
                if (template.id == id) {
                    template.status = '0';

                    var data = new FormData();
                    data.append('id', id);

                    axios.post('/ajax/edit-author/delete_template', data).then(function (response) {
                        var data = response.data.data;

                        if (response.data.result) {
                            _this.completeAddTemplate = true;

                            _this.clearText('Вы успешно удалили шаблон ответа');
                        }
                    }).catch(function (response) {});
                }
            });

            // this.templates.forEach((template, index) => {
            //     // if (template.id == id) {
            //     //     template.status = '0';
            //     // }
            //     console.log(index);
            //
            // })
        },


        //////========Auth
        validateBeforeSubmitAuthorEditMain: function validateBeforeSubmitAuthorEditMain(e) {
            var _this2 = this;

            e.preventDefault();

            var dataAuthorEditMain = {
                'name_author': this.$refs.editName.value,
                'surname_author': this.$refs.editLastName.value,
                'login_author': this.$refs.editLogin.value
            };
            this.$validator.validateAll(dataAuthorEditMain).then(function (result) {
                if (result) {
                    _this2.ajaxEditAuthorMain();
                }
            });
        },
        ajaxEditAuthorMain: function ajaxEditAuthorMain() {
            var _this3 = this;

            var data = new FormData();
            data.append('name', this.$refs.editName.value);
            data.append('surname', this.$refs.editLastName.value);
            data.append('login', this.$refs.editLogin.value);
            data.append('birthday', this.$refs.editBirthday.value);
            data.append('bio', this.$refs.editBio.value);

            axios.post('/ajax/edit-author/main', data).then(function (response) {
                var data = response.data.data;

                console.log(data);
                if (response.data.result) {
                    _this3.completeMain = true;
                    _this3.clearText('Вы успешно отредактировали основную информацию');
                } else {
                    _this3.disabledButton = true;
                    _this3.authError = data;
                }
            }).catch(function (response) {
                Object.keys(response.response.data).forEach(function (test) {
                    _this3.errors.add(test + '_author', 'Ошибка в поле');
                });
            });
        },
        ajaxEditAuthorEducation: function ajaxEditAuthorEducation() {
            var _this4 = this;

            var data = new FormData();
            data.append('university', this.$refs.editUniversity.value);
            data.append('study_finish_date', this.$refs.editFinishStudy.value);
            data.append('session_begin_date', this.$refs.editSessionBegin.value);
            data.append('specialty', this.$refs.editSpeciality.value);
            data.append('faculty', this.$refs.editFaculty.value);

            axios.post('/ajax/edit-author/education', data).then(function (response) {
                var data = response.data.data;

                if (response.data.result) {
                    _this4.completeEducation = true;
                    _this4.clearText('Вы успешно отредактировали информацию об образовании');
                } else {
                    _this4.disabledButton = true;
                    _this4.authError = data;
                }
            }).catch(function (response) {
                // Object.keys(response.response.data).forEach(test => {
                //     this.errors.add(test + '_author','Ошибка в поле');
                // });
            });
        },

        //================ EditPassword
        validateBeforeSubmitAuthorEditPassword: function validateBeforeSubmitAuthorEditPassword(e) {
            var _this5 = this;

            e.preventDefault();

            var dataAuthorEditPassword = {
                'old_password': this.$refs.editOldPassword.value,
                'password': this.$refs.editNewPassword.value,
                'password_confirmation': this.$refs.editPasswordConfirmation.value
            };

            this.$validator.validateAll(dataAuthorEditPassword).then(function (result) {
                if (result) {
                    _this5.disabledButton = true;
                    _this5.ajaxEditAuthorPassword();
                }
            });
        },
        ajaxEditAuthorPassword: function ajaxEditAuthorPassword() {
            var _this6 = this;

            var data = new FormData();
            data.append('old_password', this.$refs.editOldPassword.value);
            data.append('password', this.$refs.editNewPassword.value);
            data.append('password_confirmation', this.$refs.editPasswordConfirmation.value);

            axios.post('/ajax/edit-author/password', data).then(function (response) {
                var data = response.data.data;

                console.log(response);
                if (response.data.result) {
                    _this6.completePassword = true;
                    _this6.$refs.editOldPassword.value = '';
                    _this6.$refs.editNewPassword.value = '';
                    _this6.$refs.editPasswordConfirmation.value = '';

                    _this6.clearText('Вы успешно отредактировали пароль');
                } else {
                    _this6.disabledButton = true;
                }
            }).catch(function (response) {
                if (response.response.data.old_password[0] != '') {
                    _this6.errors.add('old_password', response.response.data.old_password[0]);
                }
                _this6.disabledButton = false;
            });
        },

        //=========== Contactdata
        validateBeforeSubmitAuthorEditContact: function validateBeforeSubmitAuthorEditContact(e) {
            var _this7 = this;

            e.preventDefault();
            var dataAuthorEditContact = {
                'email_author': this.$refs.editEmail.value
            };

            this.$validator.validateAll(dataAuthorEditContact).then(function (result) {
                if (result) {
                    _this7.disabledButton = true;
                    _this7.ajaxEditAuthorContact();
                }
            });
        },
        addFieldPhoneNumber: function addFieldPhoneNumber(e) {
            e.preventDefault();
            var element = $('#fields').append(this.sampleElement);
            this.cntField = this.totalField++;

            if (this.cntField == 4) {
                this.flagAddField = false;
            }
        },
        ajaxEditAuthorContact: function ajaxEditAuthorContact() {
            var _this8 = this;

            var data = new FormData();
            data.append('email', this.$refs.editEmail.value);

            $('.phoneNumber').each(function (key, val) {
                data.append('phone[' + key + ']', $(val).val());
            });

            axios.post('/ajax/edit-author/contact', data).then(function (response) {
                var data = response.data.data;

                if (response.data.result) {
                    _this8.completeContact = true;
                    _this8.clearText('Вы успешно отредактировали контактные данные');
                } else {
                    _this8.disabledButton = true;
                }
            }).catch(function (response) {
                _this8.disabledButton = false;
            });
        },
        ajaxEditAuthorTypeWork: function ajaxEditAuthorTypeWork() {
            var _this9 = this;

            var arrayTypes = [];
            $('input:checkbox[name=typesWork]:checked').each(function () {
                arrayTypes.push($(this).val());
            });

            var data = new FormData();

            data.append('types', arrayTypes);

            axios.post('/ajax/edit-author/type_work', data).then(function (response) {
                var data = response.data.data;

                if (response.data.result) {
                    _this9.completeTypeWork = true;
                    _this9.clearText('Вы успешно отредактировали типы выполняемых работ');
                } else {
                    _this9.disabledButton = true;
                }
            }).catch(function (response) {
                _this9.disabledButton = false;
            });
        },
        ajaxEditAuthorSubjects: function ajaxEditAuthorSubjects() {
            var _this10 = this;

            var arraySubjects = [];
            $('input:checkbox[name=subjects]:checked').each(function () {
                arraySubjects.push($(this).val());
            });

            var data = new FormData();

            data.append('subjects', arraySubjects);

            axios.post('/ajax/edit-author/subject', data).then(function (response) {
                var data = response.data.data;

                if (response.data.result) {
                    _this10.completeSubjects = true;
                    _this10.clearText('Вы успешно отредактировали дисциплины');
                } else {
                    _this10.disabledButton = true;
                }
            }).catch(function (response) {
                _this10.disabledButton = false;
            });
        },

        selectAll: function selectAll() {
            if (!this.allSelected) {
                $('input:checkbox[name=typesWork]').each(function () {
                    $(this).prop('checked', true);
                });

                this.allSelected = true;
            } else {
                $('input:checkbox[name=typesWork]').each(function () {
                    $(this).prop('checked', false);
                });

                this.allSelected = false;
            }
        },

        clearText: function clearText(str) {
            var _this11 = this;

            this.disabledButton = false;
            this.messageComplete = str;

            setTimeout(function () {
                _this11.changeFlagMessage();
                _this11.messageComplete = '';
            }, 2000);
        },
        changeFlagMessage: function changeFlagMessage() {
            this.completeEdit = false;
            this.completeMain = false;
            this.completeEdit = false;
            this.completeEducation = false;
            this.completePassword = false;
            this.completeContact = false;
            this.completeTypeWork = false;
            this.completeSubjects = false;
        },
        validateBeforeSubmitAuthorAddTemplate: function validateBeforeSubmitAuthorAddTemplate(e) {
            var _this12 = this;

            e.preventDefault();

            var dataAuthorAddTemplate = {
                'name_template_author': this.$refs.nameTemplateAuthor.value,
                'text_template_author': this.$refs.textTemplateAuthor.value,
                'type_template_author': this.$refs.typeTemplateAuthor.value
            };

            this.$validator.validateAll(dataAuthorAddTemplate).then(function (result) {
                if (result) {

                    _this12.ajaxAuthorAddTemplateResponse();
                }
            });
        },
        toggleTemplateState: function toggleTemplateState(id) {
            var _this13 = this;

            this.templates.forEach(function (template, index) {
                if (template.id == id) {
                    _this13.$refs.nameTemplateAuthor.value = template.title;
                    _this13.$refs.textTemplateAuthor.value = template.text;
                    _this13.$refs.typeTemplateAuthor.value = template.type_work;

                    template.flag = 1;
                    _this13.editTemplate = true;
                    _this13.templateId = id;
                    // const data = new FormData();
                    // data.append('bio', this.$refs.editBio.value);
                }
            });
        },
        ajaxAuthorAddTemplateResponse: function ajaxAuthorAddTemplateResponse() {
            var data = new FormData();

            data.append('title', this.$refs.nameTemplateAuthor.value);
            data.append('text', this.$refs.textTemplateAuthor.value);
            data.append('type_work_id', this.$refs.typeTemplateAuthor.value);

            if (this.editTemplate == false) {
                this.sendTemplateFormForAdd();
            } else {
                this.sendTemplateFormForEdit();
            }
        },
        sendTemplateFormForAdd: function sendTemplateFormForAdd() {
            var _this14 = this;

            var data = new FormData();

            data.append('title', this.$refs.nameTemplateAuthor.value);
            data.append('text', this.$refs.textTemplateAuthor.value);
            data.append('type_work_id', this.$refs.typeTemplateAuthor.value);

            axios.post('/ajax/edit-author/add_template', data).then(function (response) {
                var data = response.data.data;

                if (response.data.result) {
                    _this14.completeAddTemplate = true;
                    _this14.clearText('Вы успешно добавили шаблон ответа');

                    _this14.templates.push({
                        title: _this14.$refs.nameTemplateAuthor.value,
                        text: _this14.$refs.textTemplateAuthor.value,
                        status: 1,
                        id: data
                    });

                    _this14.$refs.nameTemplateAuthor.value = '';
                    _this14.$refs.textTemplateAuthor.value = '';
                    _this14.$refs.typeTemplateAuthor.value = '';

                    // console.log( this.$refs.nameTemplateAuthor.value);
                } else {
                        // this.disabledButton = true;
                    }
            }).catch(function (response) {
                // if (response.response.data.old_password[0] != '') {
                //     this.errors.add('old_password', response.response.data.old_password[0]);
                // }
                // this.disabledButton = false;
            });
        },
        sendTemplateFormForEdit: function sendTemplateFormForEdit() {
            var _this15 = this;

            var data = new FormData();

            data.append('title', this.$refs.nameTemplateAuthor.value);
            data.append('text', this.$refs.textTemplateAuthor.value);
            data.append('type_work_id', this.$refs.typeTemplateAuthor.value);

            axios.post('/ajax/edit-author/edit_template', data).then(function (response) {

                if (response.data.result) {
                    _this15.completeAddTemplate = true;
                    _this15.editTemplate = false;

                    _this15.templates.forEach(function (template, index) {
                        if (template.id == _this15.templateId) {
                            template.title = _this15.$refs.nameTemplateAuthor.value;
                            template.text = _this15.$refs.textTemplateAuthor.value;
                            template.flag = 0;
                            //
                            // const data = new FormData();
                            // data.append('id', id);
                            //
                            // axios.post('/ajax/edit-author/delete-template', data).then((response) => {
                            //     var data = response.data.data;
                            //
                            //     if (response.data.result) {
                            //         this.completeAddTemplate = true;
                            //
                            //         this.clearText('Вы успешно удалили шаблон ответа');
                            //     }
                            // }).catch(response => {
                            //
                            // });
                        }
                    });

                    _this15.$refs.nameTemplateAuthor.value = '';
                    _this15.$refs.textTemplateAuthor.value = '';
                    _this15.$refs.typeTemplateAuthor.value = '';

                    _this15.clearText('Вы успешно отредактировали шаблон ответа');
                    // this.templates.push({
                    //     title: this.$refs.nameTemplateAuthor.value,
                    //     text: this.$refs.textTemplateAuthor.value,
                    //     status: 1,
                    //     id: data,
                    // });
                    //
                    // this.$refs.nameTemplateAuthor.value = '';
                    // this.$refs.textTemplateAuthor.value = '';
                    // this.$refs.typeTemplateAuthor.value = '';

                    // console.log( this.$refs.nameTemplateAuthor.value);
                } else {
                        // this.disabledButton = true;
                    }
            }).catch(function (response) {
                // if (response.response.data.old_password[0] != '') {
                //     this.errors.add('old_password', response.response.data.old_password[0]);
                // }
                // this.disabledButton = false;
            });
        }
    }
});

/***/ })

/******/ });