function buildFormData(dictionary) {
    let data = new FormData();
    Object.keys(dictionary).forEach(key => data.append(key, dictionary[key]));
    return data;
}

const profileEdit = new Vue({
    el: '.profileEdit',
    data: {
        disabledButton: false,
        phone: '',

        qwe: '',
        allSelected: false,

        completeMain: false,
        completeEducation: false,
        completeContact: false,
        completePassword: false,

        messageComplete: '',
        totalField: 0,
        cntField: 0,
        flagAddField: true
    },



    methods: {
        validateBeforeSubmitCustomerEditMain(e){
            e.preventDefault();

            const dataCustomerEditMain = {
                'name_customer': this.$refs.editName.value,
                'surname_customer': this.$refs.editLastName.value,
                'login_customer': this.$refs.editLogin.value,
            };

            this.$validator.validateAll(dataCustomerEditMain).then(result => {
                if (result) {
                    this.ajaxEditCustomerMain();
                }
            });
        },

        ajaxEditCustomerMain () {
            const data = buildFormData({
                'name': this.$refs.editLastName.value,
                'surname': this.$refs.editLastName.value,
                'login': this.$refs.editLogin.value,
                'birthday': this.$refs.editBirthday.value,
                'bio': this.$refs.editBio.value
            });

            axios.post('/ajax/edit-customer/main', data).then((response) => {
                var data = response.data.data;

            console.log(data);
            if (response.data.result) {
                this.completeMain = true;
                this.clearText('Вы успешно отредактировали основную информацию');
            } else {
                this.disabledButton = true;
                this.authError = data;
            }
        }).catch(response => {
                Object.keys(response.response.data).forEach(test => {
                this.errors.add(test + '_customer', 'Ошибка в поле');
        });
        });
        },

        ajaxEditCustomerEducation () {
            const data = buildFormData({
                'university': this.$refs.editUniversity.value,
                'study_finish_date': this.editFinishStudy.value,
                'session_begin_date': this.$refs.editSessionBegin.value,
                'specialty': this.$refs.editSpeciality.value,
                'faculty': this.$refs.editFaculty.value,
            });

            axios.post('/ajax/edit-customer/education', data).then((response) => {
                var data = response.data.data;

            console.log(data);

            if (response.data.result) {
                this.completeMain = true;
                this.clearText('Вы успешно отредактировали основную информацию');
            } else {
                this.disabledButton = true;
                this.authError = data;
            }
        }).catch(response => {
                Object.keys(response.response.data).forEach(test => {
                this.errors.add(test + '_customer', 'Ошибка в поле');
        });
        });
        },


        clearText(str){
            this.disabledButton = false;
            this.messageComplete = str;

            setTimeout(() => {
                this.changeFlagMessage();
            this.messageComplete = '';
        }, 2000);
        },
        changeFlagMessage(){
            this.completeEdit = false;
            this.completeMain = false;
            this.completeEdit = false;
            this.completeEducation = false;
            this.completePassword = false;
            this.completeContact = false;
        },


    },
});