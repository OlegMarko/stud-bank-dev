var Child = {
    template: '<div>Пользовательский компонент!</div>'
}


const profileEdit = new Vue({
    el: '.profileEdit',
    data: {
        disabledButton: false,
        editTemplate: false,
        phone: '',

        qwe: '',
        allSelected: false,

        completeMain: false,
        completeEdit: false,
        completeEducation: false,
        completePassword: false,
        completeContact: false,
        completeTypeWork: false,
        completeAddTemplate: false,
        completeSubjects: false,

        templates: [],
        templateId: '',

        messageComplete: '',
        totalField: 0,
        cntField: 0,
        flagAddField: true,
        sampleElement: ' <div class="form-group col-xs-12 col-md-4"><label>Телефон</label> ' +
            '<div class="row">' +
            '<div class="col-xs-12">' +
            '<input type="text" placeholder="Телефон" name="phone_user[]" value="" class="form-control phoneNumber"></div></div></div>',
    },
    created: function() {
        this.totalField = $('.phoneNumber').length;
    },
    mounted() {
        this.templates = JSON.parse($(this.$el).attr('data-templates'));
    },
    components: {
        // <my-component> будет доступен только в шаблоне родителя

        'my-component': Child
    },
    methods: {
        deleteTemplateState(id) {

            this.templates.forEach((template, index) => {
                if (template.id == id) {
                    template.status = '0';

                    const data = new FormData();
                    data.append('id', id);

                    axios.post('/ajax/edit-author/delete_template', data).then((response) => {
                        var data = response.data.data;

                        if (response.data.result) {
                            this.completeAddTemplate = true;

                            this.clearText('Вы успешно удалили шаблон ответа');
                        }
                    }).catch(response => {

                    });
                }
            })




            // this.templates.forEach((template, index) => {
            //     // if (template.id == id) {
            //     //     template.status = '0';
            //     // }
            //     console.log(index);
            //
            // })
        },

        //////========Auth
        validateBeforeSubmitAuthorEditMain(e) {
            e.preventDefault();

            const dataAuthorEditMain = {
                'name_author': this.$refs.editName.value,
                'surname_author': this.$refs.editLastName.value,
                'login_author': this.$refs.editLogin.value,
            };
            this.$validator.validateAll(dataAuthorEditMain).then(result => {
                if (result) {
                    this.ajaxEditAuthorMain();
                }
            });
        },

        ajaxEditAuthorMain() {
            const data = new FormData();
            data.append('name', this.$refs.editName.value);
            data.append('surname', this.$refs.editLastName.value);
            data.append('login', this.$refs.editLogin.value);
            data.append('birthday', this.$refs.editBirthday.value);
            data.append('bio', this.$refs.editBio.value);

            axios.post('/ajax/edit-author/main', data).then((response) => {
                var data = response.data.data;

                console.log(data);
                if (response.data.result) {
                    this.completeMain = true;
                    this.clearText('Вы успешно отредактировали основную информацию');
                } else {
                    this.disabledButton = true;
                    this.authError = data;
                }
            }).catch(response => {
                Object.keys(response.response.data).forEach(test => {
                    this.errors.add(test + '_author', 'Ошибка в поле');
                });
            });
        },

        ajaxEditAuthorEducation() {
            const data = new FormData();
            data.append('university', this.$refs.editUniversity.value);
            data.append('study_finish_date', this.$refs.editFinishStudy.value);
            data.append('session_begin_date', this.$refs.editSessionBegin.value);
            data.append('specialty', this.$refs.editSpeciality.value);
            data.append('faculty', this.$refs.editFaculty.value);

            axios.post('/ajax/edit-author/education', data).then((response) => {
                var data = response.data.data;

                if (response.data.result) {
                    this.completeEducation = true;
                    this.clearText('Вы успешно отредактировали информацию об образовании');
                } else {
                    this.disabledButton = true;
                    this.authError = data;
                }
            }).catch(response => {
                // Object.keys(response.response.data).forEach(test => {
                //     this.errors.add(test + '_author','Ошибка в поле');
                // });
            });


        },
        //================ EditPassword
        validateBeforeSubmitAuthorEditPassword(e) {
            e.preventDefault();

            const dataAuthorEditPassword = {
                'old_password': this.$refs.editOldPassword.value,
                'password': this.$refs.editNewPassword.value,
                'password_confirmation': this.$refs.editPasswordConfirmation.value,
            };

            this.$validator.validateAll(dataAuthorEditPassword).then(result => {
                if (result) {
                    this.disabledButton = true;
                    this.ajaxEditAuthorPassword();
                }
            });
        },
        ajaxEditAuthorPassword() {
            const data = new FormData();
            data.append('old_password', this.$refs.editOldPassword.value);
            data.append('password', this.$refs.editNewPassword.value);
            data.append('password_confirmation', this.$refs.editPasswordConfirmation.value);

            axios.post('/ajax/edit-author/password', data).then((response) => {
                var data = response.data.data;

                console.log(response);
                if (response.data.result) {
                    this.completePassword = true;
                    this.$refs.editOldPassword.value = '';
                    this.$refs.editNewPassword.value = '';
                    this.$refs.editPasswordConfirmation.value = '';

                    this.clearText('Вы успешно отредактировали пароль');
                } else {
                    this.disabledButton = true;
                }
            }).catch(response => {
                if (response.response.data.old_password[0] != '') {
                    this.errors.add('old_password', response.response.data.old_password[0]);
                }
                this.disabledButton = false;
            });
        },
        //=========== Contactdata
        validateBeforeSubmitAuthorEditContact(e) {
            e.preventDefault();
            const dataAuthorEditContact = {
                'email_author': this.$refs.editEmail.value,
            };

            this.$validator.validateAll(dataAuthorEditContact).then(result => {
                if (result) {
                    this.disabledButton = true;
                    this.ajaxEditAuthorContact();
                }
            });
        },
        addFieldPhoneNumber(e) {
            e.preventDefault();
            var element = $('#fields').append(this.sampleElement);
            this.cntField = this.totalField++;

            if (this.cntField == 4) {
                this.flagAddField = false;
            }
        },

        ajaxEditAuthorContact() {
            const data = new FormData();
            data.append('email', this.$refs.editEmail.value);

            $('.phoneNumber').each(function(key, val) {
                data.append('phone[' + key + ']', $(val).val());
            });

            axios.post('/ajax/edit-author/contact', data).then((response) => {
                var data = response.data.data;

                if (response.data.result) {
                    this.completeContact = true;
                    this.clearText('Вы успешно отредактировали контактные данные');
                } else {
                    this.disabledButton = true;
                }
            }).catch(response => {
                this.disabledButton = false;
            });
        },

        ajaxEditAuthorTypeWork() {
            var arrayTypes = [];
            $('input:checkbox[name=typesWork]:checked').each(function() {
                arrayTypes.push($(this).val());
            });

            const data = new FormData();

            data.append('types', arrayTypes);

            axios.post('/ajax/edit-author/type_work', data).then((response) => {
                var data = response.data.data;

                if (response.data.result) {
                    this.completeTypeWork = true;
                    this.clearText('Вы успешно отредактировали типы выполняемых работ');
                } else {
                    this.disabledButton = true;
                }
            }).catch(response => {
                this.disabledButton = false;
            });

        },
        ajaxEditAuthorSubjects() {
            var arraySubjects = [];
            $('input:checkbox[name=subjects]:checked').each(function() {
                arraySubjects.push($(this).val());
            });

            const data = new FormData();

            data.append('subjects', arraySubjects);

            axios.post('/ajax/edit-author/subject', data).then((response) => {
                var data = response.data.data;

                if (response.data.result) {
                    this.completeSubjects = true;
                    this.clearText('Вы успешно отредактировали дисциплины');
                } else {
                    this.disabledButton = true;
                }
            }).catch(response => {
                this.disabledButton = false;
            });
        },
        selectAll: function() {
            if (!this.allSelected) {
                $('input:checkbox[name=typesWork]').each(function() {
                    $(this).prop('checked', true);
                });

                this.allSelected = true;
            } else {
                $('input:checkbox[name=typesWork]').each(function() {
                    $(this).prop('checked', false);
                });

                this.allSelected = false;
            }
        },


        clearText(str) {
            this.disabledButton = false;
            this.messageComplete = str;

            setTimeout(() => {
                this.changeFlagMessage();
                this.messageComplete = '';
            }, 2000);
        },
        changeFlagMessage() {
            this.completeEdit = false;
            this.completeMain = false;
            this.completeEdit = false;
            this.completeEducation = false;
            this.completePassword = false;
            this.completeContact = false;
            this.completeTypeWork = false;
            this.completeSubjects = false;
        },


        validateBeforeSubmitAuthorAddTemplate(e) {
            e.preventDefault();

            const dataAuthorAddTemplate = {
                'name_template_author': this.$refs.nameTemplateAuthor.value,
                'text_template_author': this.$refs.textTemplateAuthor.value,
                'type_template_author': this.$refs.typeTemplateAuthor.value,
            };

            this.$validator.validateAll(dataAuthorAddTemplate).then(result => {
                if (result) {

                    this.ajaxAuthorAddTemplateResponse();
                }
            });
        },
        toggleTemplateState(id) {
            this.templates.forEach((template, index) => {
                if (template.id == id) {
                    this.$refs.nameTemplateAuthor.value = template.title;
                    this.$refs.textTemplateAuthor.value = template.text;
                    this.$refs.typeTemplateAuthor.value = template.type_work;

                    template.flag = 1;
                    this.editTemplate = true;
                    this.templateId = id;
                    // const data = new FormData();
                    // data.append('bio', this.$refs.editBio.value);
                }
            })
        },
        ajaxAuthorAddTemplateResponse() {
            const data = new FormData();

            data.append('title', this.$refs.nameTemplateAuthor.value);
            data.append('text', this.$refs.textTemplateAuthor.value);
            data.append('type_work_id', this.$refs.typeTemplateAuthor.value);

            if (this.editTemplate == false) {
                this.sendTemplateFormForAdd();
            } else {
                this.sendTemplateFormForEdit();
            }
        },
        sendTemplateFormForAdd() {
            const data = new FormData();

            data.append('title', this.$refs.nameTemplateAuthor.value);
            data.append('text', this.$refs.textTemplateAuthor.value);
            data.append('type_work_id', this.$refs.typeTemplateAuthor.value);

            axios.post('/ajax/edit-author/add_template', data).then((response) => {
                var data = response.data.data;

                if (response.data.result) {
                    this.completeAddTemplate = true;
                    this.clearText('Вы успешно добавили шаблон ответа');

                    this.templates.push({
                        title: this.$refs.nameTemplateAuthor.value,
                        text: this.$refs.textTemplateAuthor.value,
                        status: 1,
                        id: data,
                    });

                    this.$refs.nameTemplateAuthor.value = '';
                    this.$refs.textTemplateAuthor.value = '';
                    this.$refs.typeTemplateAuthor.value = '';

                    // console.log( this.$refs.nameTemplateAuthor.value);
                } else {
                    // this.disabledButton = true;
                }
            }).catch(response => {
                // if (response.response.data.old_password[0] != '') {
                //     this.errors.add('old_password', response.response.data.old_password[0]);
                // }
                // this.disabledButton = false;
            });
        },
        sendTemplateFormForEdit() {
            const data = new FormData();

            data.append('title', this.$refs.nameTemplateAuthor.value);
            data.append('text', this.$refs.textTemplateAuthor.value);
            data.append('type_work_id', this.$refs.typeTemplateAuthor.value);

            axios.post('/ajax/edit-author/edit_template', data).then((response) => {

                if (response.data.result) {
                    this.completeAddTemplate = true;
                    this.editTemplate = false;


                    this.templates.forEach((template, index) => {
                        if (template.id == this.templateId) {
                            template.title = this.$refs.nameTemplateAuthor.value;
                            template.text = this.$refs.textTemplateAuthor.value;
                            template.flag = 0;
                            //
                            // const data = new FormData();
                            // data.append('id', id);
                            //
                            // axios.post('/ajax/edit-author/delete-template', data).then((response) => {
                            //     var data = response.data.data;
                            //
                            //     if (response.data.result) {
                            //         this.completeAddTemplate = true;
                            //
                            //         this.clearText('Вы успешно удалили шаблон ответа');
                            //     }
                            // }).catch(response => {
                            //
                            // });
                        }
                    })




                    this.$refs.nameTemplateAuthor.value = '';
                    this.$refs.textTemplateAuthor.value = '';
                    this.$refs.typeTemplateAuthor.value = '';

                    this.clearText('Вы успешно отредактировали шаблон ответа');
                    // this.templates.push({
                    //     title: this.$refs.nameTemplateAuthor.value,
                    //     text: this.$refs.textTemplateAuthor.value,
                    //     status: 1,
                    //     id: data,
                    // });
                    //
                    // this.$refs.nameTemplateAuthor.value = '';
                    // this.$refs.textTemplateAuthor.value = '';
                    // this.$refs.typeTemplateAuthor.value = '';

                    // console.log( this.$refs.nameTemplateAuthor.value);
                } else {
                    // this.disabledButton = true;
                }
            }).catch(response => {
                // if (response.response.data.old_password[0] != '') {
                //     this.errors.add('old_password', response.response.data.old_password[0]);
                // }
                // this.disabledButton = false;
            });
        }
    },
});