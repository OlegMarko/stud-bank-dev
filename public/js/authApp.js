/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 38);
/******/ })
/************************************************************************/
/******/ ({

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(39);


/***/ }),

/***/ 39:
/***/ (function(module, exports) {

// import Form from './components/Form.vue';
// import InputTextComponent from './components/InputTextComponent.vue';
// import InputDatePickerComponent from './components/InputDatePickerComponent.vue';
// import TextAreaComponent from './components/TextAreaComponent.vue';
// import ButtonComponent from './components/ButtonFormComponent.vue';

// const myForm = new Vue({
//     el:'#myForm',
//     data:{
//         testParent:'testing'
//     },
//     created:{
//
//     },
//     components:{
//         'basic-data-form':Form,
//     }
// });

/////=======================
var app = new Vue({
    el: '#authForms',
    data: {
        emailError: '',
        passError: '',
        disabledButton: false,
        disabledRegButton: false,
        formError: '',
        regFormError: '',

        regEmail: '',
        regName: '',
        regPhone: '',
        regPass: '',
        regRepeatPass: '',
        regLogin: '',
        regSurname: '',
        regAcademy: '',
        regYear: '',
        regSpec: '',
        regEndYear: '',

        regRules: 0,
        regNotifyAndNews: 0,
        regAgency: 0,
        regNotifyStudent: 0,

        regNotifyAndNewsError: '',
        regNotifyStudentError: '',

        regAuthorFormError: '',
        regAuthorName: '',
        regAuthorFamily: '',
        regAuthorEmail: '',
        regAuthorLogin: '',
        regAuthorPass: '',
        regAuthorRepeatPass: '',
        regAuthorPhone: '',
        regAuthorUniversity: '',
        regAuthorSpeciality: '',
        regAuthorRules: '',
        regAuthorNotifyAndNews: '',

        authEmail: '',
        authError: '',
        authPassword: '',

        searchQuery: '',
        searchQueryIsDirty: false,
        isCalculating: false
    },
    watch: {
        authEmail: function authEmail() {
            this.disabledButton = false;
        },
        authPassword: function authPassword() {
            this.disabledButton = false;
        }
    },
    methods: {
        createFormData: function createFormData() {
            var _this = this;

            var data = new FormData();

            Object.keys(this.data).forEach(function (name) {
                name && _this.data[name] && form.data(name, _this.data[name]);
            });

            return form;
        },


        //////========Auth
        validateAuthBeforeSubmit: function validateAuthBeforeSubmit(e) {
            var _this2 = this;

            e.preventDefault();

            var dataAuth = {
                'auth_email': this.authEmail,
                'auth_password': this.authPassword
            };
            this.$validator.validateAll(dataAuth).then(function (result) {
                if (result) {
                    _this2.ajaxLoginUser();
                }
                _this2.disabledButton = true;
            });
        },
        ajaxLoginUser: function ajaxLoginUser() {
            var _this3 = this;

            // this.disabledButton = true;
            var data = new FormData();
            data.append('email', this.authEmail);
            data.append('password', this.authPassword);

            axios.post('/ajax-login', data).then(function (response) {
                var data = response.data.data;
                if (response.data.result) {
                    document.location = data;
                } else {
                    _this3.disabledButton = true;
                    _this3.authError = data;
                }
            });
            //.catch((error) => console.log(error.response.data))
        },


        //////========Register Author
        validateBeforeSubmit: function validateBeforeSubmit(e) {
            var _this4 = this;

            e.preventDefault();

            var dataAuthor = {
                'name_author': this.regAuthorName,
                'family': this.regAuthorFamily,
                'email': this.regAuthorEmail,
                'login': this.regAuthorLogin,
                'password': this.regAuthorPass,
                'password_confirmation': this.regAuthorRepeatPass,
                'rules': this.regAuthorRules
            };

            this.$validator.validateAll(dataAuthor).then(function (result) {
                if (result) {
                    _this4.ajaxRegisterAuthor();
                }
            });
        },
        ajaxRegisterAuthor: function ajaxRegisterAuthor() {
            var _this5 = this;

            var data = new FormData();

            data.set('name', this.regAuthorName);
            data.set('surname', this.regAuthorFamily);
            data.set('email', this.regAuthorEmail);
            data.set('login', this.regAuthorLogin);
            data.set('password', this.regAuthorPass);
            data.set('phone', this.regAuthorPhone);
            data.set('city', this.getValueInSelectById('town'));
            data.set('university', this.regAuthorUniversity);
            data.set('specialty', this.regAuthorSpeciality);
            data.set('study_finish_date', this.getValueInSelectById('endLearnYear'));
            data.set('mail_subscription', this.getIntegerValueByBoolean(this.regAuthorNotifyAndNews));
            data.set('role', '2');

            axios.post('/ajax-register', data).then(function (response) {
                if (response.data.result) {
                    window.location.assign('/profile');
                } else {
                    _this5.regAuthorFormError = data;
                }
            }).catch(function (response) {
                _this5.regAuthorFormError = response.response.data;

                // this.errors.add('old_password',response.response.data.old_password[0]);
            });
        },
        validateBeforeSubmitCustomer: function validateBeforeSubmitCustomer(e) {
            var _this6 = this;

            e.preventDefault();
            var dataStudent = {
                'name_student': this.regName,
                'login_student': this.regLogin,
                'email_student': this.regEmail,
                'password_student': this.regPass,
                'password_confirmation_student': this.regRepeatPass,
                'rules_student': this.regRules
            };

            console.log(dataStudent);
            this.$validator.validateAll(dataStudent).then(function (result) {
                if (result) {
                    _this6.ajaxRegisterStudent();
                }
            });
        },
        ajaxRegisterStudent: function ajaxRegisterStudent() {
            var _this7 = this;

            var data = new FormData();

            data.set('name', this.regName);
            data.set('email', this.regEmail);
            data.set('login', this.regLogin);
            data.set('password', this.regPass);
            data.set('phone', this.regPhone);
            data.set('role', '1');
            data.set('is_agent', '1');
            data.set('mail_subscription', this.getIntegerValueByBoolean(this.regNotifyAndNews));
            data.set('mail_message_notify', this.getIntegerValueByBoolean(this.regNotifyStudent));

            axios.post('/ajax-register', data).then(function (response) {
                if (response.data.result) {
                    window.location.assign('/profile');
                } else {
                    _this7.regFormError = data;
                }
            }).catch(function (response) {
                _this7.regFormError = response.response.data;
            });
        },

        ///========================

        getValueInSelectById: function getValueInSelectById(id) {
            // const  element =  document.getElementById(id);
            // // element = document.getElementById(id);
            // value = element.options[element.selectedIndex].value;

            return $('#' + id).val();
            // return value;
        },
        getIntegerValueByBoolean: function getIntegerValueByBoolean(bool) {
            return bool ? 1 : 0;
        },


        ////====
        validateLoginFileds: function validateLoginFileds(email, password) {
            if (!email && !password) {
                this.emailError = 'Необходимо заполнить "E-mail"';
                this.passError = 'Необходимо заполнить "Пароль"';
                this.disabledButton = false;
                return false;
            }

            if (!email) {
                this.emailError = 'Необходимо заполнить "E-mail"';
                this.disabledButton = false;
                return false;
            }

            if (!this.emailValidation(email)) {
                this.emailError = 'Не валидный "E-mail"';
                this.disabledButton = false;
                return false;
            }

            if (!password) {
                this.passError = 'Необходимо заполнить "Пароль"';
                this.disabledButton = false;
                return false;
            }

            return true;
        },
        clearLoginErrors: function clearLoginErrors() {
            this.emailError = '';
            this.passError = '';
            this.formError = '';
        },
        clearRegisterErrors: function clearRegisterErrors() {
            this.emailError = '';
            this.passError = '';
            this.formError = '';
        },
        emailValidation: function emailValidation(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        toggleModals: function toggleModals(currentModal) {
            if (currentModal == 'login') {
                $('#login').modal('hide');
                $('#register').modal('show');
            } else {
                $('#login').modal('show');
                $('#register').modal('hide');
            }
        }
    }
});

/***/ })

/******/ });