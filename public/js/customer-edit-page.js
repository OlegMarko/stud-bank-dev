/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 42);
/******/ })
/************************************************************************/
/******/ ({

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(43);


/***/ }),

/***/ 43:
/***/ (function(module, exports) {

function buildFormData(dictionary) {
    var data = new FormData();
    Object.keys(dictionary).forEach(function (key) {
        return data.append(key, dictionary[key]);
    });
    return data;
}

var profileEdit = new Vue({
    el: '.profileEdit',
    data: {
        disabledButton: false,
        phone: '',

        qwe: '',
        allSelected: false,

        completeMain: false,
        completeEducation: false,
        completeContact: false,
        completePassword: false,

        messageComplete: '',
        totalField: 0,
        cntField: 0,
        flagAddField: true,
        sampleElement: ' <div class="form-group col-xs-12 col-md-4"><label>Телефон</label> ' + '<div class="row">' + '<div class="col-xs-12">' + '<input type="text" placeholder="Телефон" name="phone_user[]" value="" class="form-control phoneNumber"></div></div></div>'
    },

    methods: {
        validateBeforeSubmitCustomerEditMain: function validateBeforeSubmitCustomerEditMain(e) {
            var _this = this;

            e.preventDefault();

            var dataCustomerEditMain = {
                'name_customer': this.$refs.editName.value,
                'surname_customer': this.$refs.editLastName.value,
                'login_customer': this.$refs.editLogin.value
            };

            this.$validator.validateAll(dataCustomerEditMain).then(function (result) {
                if (result) {
                    _this.ajaxEditCustomerMain();
                }
            });
        },
        ajaxEditCustomerMain: function ajaxEditCustomerMain() {
            var _this2 = this;

            var data = buildFormData({
                'name': this.$refs.editName.value,
                'surname': this.$refs.editLastName.value,
                'login': this.$refs.editLogin.value,
                'birthday': this.$refs.editBirthday.value,
                'bio': this.$refs.editBio.value
            });

            axios.post('/ajax/edit-customer/main', data).then(function (response) {
                var data = response.data.data;

                console.log(data);
                if (response.data.result) {
                    _this2.completeMain = true;
                    _this2.clearText('Вы успешно отредактировали основную информацию');
                } else {
                    _this2.disabledButton = true;
                    _this2.authError = data;
                }
            }).catch(function (response) {
                Object.keys(response.response.data).forEach(function (test) {
                    _this2.errors.add(test + '_customer', 'Ошибка в поле');
                });
            });
        },
        ajaxEditCustomerEducation: function ajaxEditCustomerEducation() {
            var _this3 = this;

            var data = new FormData();
            data.append('university', this.$refs.editUniversity.value);
            data.append('study_finish_date', this.$refs.editFinishStudy.value);
            data.append('session_begin_date', this.$refs.editSessionBegin.value);
            data.append('specialty', this.$refs.editSpeciality.value);
            data.append('faculty', this.$refs.editFaculty.value);

            axios.post('/ajax/edit-customer/education', data).then(function (response) {
                var data = response.data.data;

                console.log(data);

                if (response.data.result) {
                    _this3.completeMain = true;
                    _this3.clearText('Вы успешно отредактировали основную информацию');
                } else {
                    _this3.disabledButton = true;
                    _this3.authError = data;
                }
            }).catch(function (response) {
                Object.keys(response.response.data).forEach(function (test) {
                    _this3.errors.add(test + '_customer', 'Ошибка в поле');
                });
            });
        },

        //================ EditPassword
        validateBeforeSubmitCustomerEditPassword: function validateBeforeSubmitCustomerEditPassword(e) {
            var _this4 = this;

            e.preventDefault();

            var dataCustomerEditPassword = {
                'old_password': this.$refs.editOldPassword.value,
                'password': this.$refs.editNewPassword.value,
                'password_confirmation': this.$refs.editPasswordConfirmation.value
            };

            this.$validator.validateAll(dataCustomerEditPassword).then(function (result) {
                if (result) {
                    _this4.disabledButton = true;
                    _this4.ajaxEditCustomerPassword();
                }
            });
        },
        ajaxEditCustomerPassword: function ajaxEditCustomerPassword() {
            var _this5 = this;

            var data = new FormData();
            data.append('old_password', this.$refs.editOldPassword.value);
            data.append('password', this.$refs.editNewPassword.value);
            data.append('password_confirmation', this.$refs.editPasswordConfirmation.value);

            axios.post('/ajax/edit-customer/password', data).then(function (response) {
                var data = response.data.data;

                console.log(response);
                if (response.data.result) {
                    _this5.completePassword = true;
                    _this5.$refs.editOldPassword.value = '';
                    _this5.$refs.editNewPassword.value = '';
                    _this5.$refs.editPasswordConfirmation.value = '';

                    _this5.clearText('Вы успешно отредактировали пароль');
                } else {
                    _this5.disabledButton = true;
                }
            }).catch(function (response) {
                if (response.response.data.old_password[0] != '') {
                    _this5.errors.add('old_password', response.response.data.old_password[0]);
                }
                _this5.disabledButton = false;
            });
        },

        //=========== Contactdata
        validateBeforeSubmitCustomerEditContact: function validateBeforeSubmitCustomerEditContact(e) {
            var _this6 = this;

            e.preventDefault();
            var dataCustomerEditContact = {
                'email_author': this.$refs.editEmail.value
            };

            this.$validator.validateAll(dataCustomerEditContact).then(function (result) {
                if (result) {
                    _this6.disabledButton = true;
                    _this6.ajaxEditCustomerContact();
                }
            });
        },
        addFieldPhoneNumber: function addFieldPhoneNumber(e) {
            e.preventDefault();
            var element = $('#fields').append(this.sampleElement);
            this.cntField = this.totalField++;

            if (this.cntField == 4) {
                this.flagAddField = false;
            }
        },
        ajaxEditCustomerContact: function ajaxEditCustomerContact() {
            var _this7 = this;

            var data = new FormData();
            data.append('email', this.$refs.editEmail.value);

            $('.phoneNumber').each(function (key, val) {
                data.append('phone[' + key + ']', $(val).val());
            });

            axios.post('/ajax/edit-customer/contact', data).then(function (response) {
                var data = response.data.data;

                if (response.data.result) {
                    _this7.completeContact = true;
                    _this7.clearText('Вы успешно отредактировали контактные данные');
                } else {
                    _this7.disabledButton = true;
                }
            }).catch(function (response) {
                _this7.disabledButton = false;
            });
        },
        clearText: function clearText(str) {
            var _this8 = this;

            this.disabledButton = false;
            this.messageComplete = str;

            setTimeout(function () {
                _this8.changeFlagMessage();
                _this8.messageComplete = '';
            }, 2000);
        },
        changeFlagMessage: function changeFlagMessage() {
            this.completeEdit = false;
            this.completeMain = false;
            this.completeEdit = false;
            this.completeEducation = false;
            this.completePassword = false;
            this.completeContact = false;
        }
    }
});

/***/ })

/******/ });