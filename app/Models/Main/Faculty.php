<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{

    protected $table = 'faculties';
    /**
     * Display faculty status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE = 1;

    /**
     * @return array
     */
    public static function getList()
    {
        $array = [];
        $models = self::where('status', self::DISPLAY_TRUE)->orderBy('sort', 'asc')->get();
        foreach ($models as $model) {
            $array[$model->id] = $model->title;
        }

        return $array;
    }
}
