<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use App\Models\Main\UserTypeWork;

class TypeWork extends Model
{
    protected $table = 'type_works';

    /**
     * Display type-work status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE = 1;

    /**
     * @return array
     */
    public static function getList()
    {
        $array = [];
        $models = self::where('status', self::DISPLAY_TRUE)->orderBy('sort', 'asc')->get();
//        foreach ($models as $model) {
//            $array[$model->id] = $model->title;
//        }

        return $models;
    }

    public function setCheckedCheckBox($id){
        $currentTypesIds = UserTypeWork::getCurrentList();

        return (!is_null($currentTypesIds) && in_array($id,$currentTypesIds)) ? 'checked' : null;
    }
}
