<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

class AcademicLevel extends Model
{
    /**
     * Display academic-level status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE = 1;
}
