<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Seo
 * @package App\Models\Frontend
 */
class Seo extends Model
{
    /**
     * @var array
     */
    private static $defaultSeoResult = ['title' => '', 'description' => '', 'keywords' => ''];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seo';

    /**
     * @param $pageUrl
     * @return array
     */
    public static function getSeoData($pageUrl)
    {
        $model = Seo::where('page_url', $pageUrl)->first();
        if ($model) {
            return $model;
        }

        return self::$defaultSeoResult;
    }
}
