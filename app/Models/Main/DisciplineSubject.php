<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

class DisciplineSubject extends Model
{
    protected $table = 'discipline_subject';
    protected $fillable = ['discipline_id', 'subject_id'];

    public $timestamps = false;

    /**
     * @param $subject_id
     * @return null
     */
    public static function getDisciplineByIdSubject($subject_id)
    {
        $discipline = self::where('subject_id', $subject_id)->first();

        return !is_null($discipline) ? $discipline->discipline : null;
    }

    /**
     * @param $discipline_id
     * @param $subject_id
     */
    public static function addDisciplineForSubject($discipline_id, $subject_id)
    {
        $model = self::where('subject_id', $subject_id)->first();

        if (!is_null($model)) {
            self::where('subject_id', $subject_id)->delete();
        }

        self::create(['discipline_id' => $discipline_id, 'subject_id' => $subject_id]);
    }

    /**
     * @param $subject_id
     * @return null
     */
    public static function getDisciplineIdBySubjectId($subject_id)
    {
        $discipline = self::where('subject_id', $subject_id)->first();

        return !is_null($discipline) ? $discipline->discipline_id : null;
    }

    /**
     * @param $disciplineId
     * @return array
     */
    public static function getListByDisciplineId($disciplineId)
    {
        $output = [];

        $models = self::where('discipline_id', $disciplineId)->get();

        foreach ($models as $model) {
            if ($model->subject->status) {
                $output[$model->subject_id] = [
                    'title' => $model->subject->title,
                    'model' => $model
                ];
            }
        }
        return $output;
    }

    public function setCheckedCheckBox($id)
    {
        $currentSubjectsIds = UserSubject::getCurrentList();

        return (!is_null($currentSubjectsIds) && in_array($id, $currentSubjectsIds)) ? 'checked' : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function discipline()
    {
        return $this->hasOne('App\Models\Main\Discipline', 'id', 'discipline_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subject()
    {
        return $this->hasOne('App\Models\Main\Subject', 'id', 'subject_id');
    }
}
