<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TemplateResponse extends Model
{
    protected $table = 'template_responses';
    protected $fillable = ['title', 'text', 'type_work_id', 'user_id'];

    public $timestamps = false;

    /**
     * @param $userId
     * @param $array
     * @return bool
     */
    public static function addTemplateByUserId($userId, $array)
    {
        $model = self::create([
            'title' => $array->title,
            'text' => $array->text,
            'type_work_id' => $array->type_work_id,
            'user_id' => $userId
        ]);

        return ($model == true) ? $model->id : false;
    }

    /**
     * @return string
     */
    public static function getListJson()
    {
        $output = [];
        $models = self::where('user_id', Auth::user()->id)->get();

        foreach ($models as $model) {
            $output[] = [
                'id' => $model->id,
                'title' => $model->title,
                'text' => $model->text,
                'type_work' => $model->type_work_id,
                'flag' => 0,
                'status' => 1,
            ];
        }
        return json_encode($output);
    }

    /**
     * edit template response
     * @param $request
     * @return mixed
     */
    public static function edit($request)
    {
        return self::where('user_id', Auth::user()->id)->update([
            'title'=>$request->title,
            'text'=>$request->text,
            'type_work_id'=>$request->type_work_id
        ]);
    }
}
