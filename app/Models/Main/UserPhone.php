<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserPhone extends Model
{
    protected $table = 'user_phones';

    public $timestamps = false;

    protected $fillable = ['user_id', 'phone'];

    /**
     * @param $userId
     * @param $array
     */
    public static function addPhoneByUserId($userId, $array)
    {
        self::where('user_id', $userId)->delete();
        foreach ($array as $phone) {
            if (!is_null($phone)) {
                self::create(['user_id' => $userId, 'phone' => $phone]);
            }
        }
    }

    /**
     * @return mixed
     */
    public static function getList()
    {
        return self::where('user_id', Auth::user()->id)->get()->toArray();
    }
}
