<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ScienceDiscipline
 * @package App\Models\Main
 */
class ScienceDiscipline extends Model
{
    protected $table = 'science_discipline';
    protected $fillable = ['science_id', 'discipline_id'];

    public $timestamps = false;


    /**
     * @param $science_id
     * @param $discipline_id
     */
    public static function addScienceForDiscipline($science_id, $discipline_id)
    {
        $model = self::where('discipline_id', $discipline_id)->first();

        if (!is_null($model)) {
            self::where('discipline_id', $discipline_id)->delete();
        }

        self::create(['science_id' => $science_id, 'discipline_id' => $discipline_id]);
    }

    /**
     * @param $discipline_id
     * @return mixed
     */
    public static function getScienceByIdDiscipline($discipline_id)
    {
        $science = self::where('discipline_id', $discipline_id)->first();

        return !is_null($science) ? $science->science : null;
    }

    /**
     * @param $discipline_id
     * @return mixed
     */
    public static function getScienceIdByDisciplineId($discipline_id)
    {
        $science = self::where('discipline_id', $discipline_id)->first();

        return !is_null($science) ? $science->science_id : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function science()
    {
        return $this->hasOne('App\Models\Main\Science', 'id', 'science_id');
    }
}
