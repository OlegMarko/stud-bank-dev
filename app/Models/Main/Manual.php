<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

/**
 * Class Instrukcii
 * @package App\Models\Main
 */
class Manual extends Model
{
    /**
     * @var string
     */
    protected $table = 'pages_manuals';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Display instruction pages status and menu
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE  = 1;

    /**
     * @return mixed
     */
    public static function getListMenu(){
        return self::select('slug','h2')
            ->where('status',self::DISPLAY_TRUE)
            ->where('in_menu',self::DISPLAY_TRUE)
            ->orderBy('sort','asc')
            ->get();
    }

    /**
     * @param $url
     * @return string
     */
    public static function setNeedFormatSlug(){
        return '/'.Request::path();
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function findByUrl($url){
        $slug = self::setNeedFormatSlug($url);
        $page = self::
            select('h2','text')
            ->where('status',self::DISPLAY_TRUE)
            ->where('slug',$slug)
            ->first();

        if(is_null($page)){
            abort(404);
        }
        return $page;
    }

    /**
     * set active class for link
     */
    public function setActive($url){
        return  (self::setNeedFormatSlug($url) == $url) ? 'active':'';
    }
}
