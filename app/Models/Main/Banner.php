<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';
    public $timestamps = false;
    public $dir = 'uploads';

    /**
     * Display banner status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE = 1;

    /**
     * Position banner in site
     */
    const AUTHOR_CABINET = 0;
    const CUSTOMER_CABINET = 1;
    const ALL_USERS = 2;

    /**
     * get list banners by role user
     * @param $int
     * @return mixed
     */
    public static function getListByRole($int)
    {
        $models = self::whereIn('position', [$int, self::ALL_USERS])
            ->where('status', self::DISPLAY_TRUE)
            ->orderBy('sort', 'asc')
            ->get();

        return $models;
    }

    /**
     * @param $iteration
     * @return null|string
     */
    public function setActiveItem($iteration)
    {
        return $iteration == 0 ? "active" : null;
    }
}
