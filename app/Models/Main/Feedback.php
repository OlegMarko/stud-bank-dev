<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Feedback
 * @package App\Models\Main
 */
class Feedback extends Model
{
    protected $table = 'feedbacks';
    public $timestamps = false;

    /**
     * Display feedback status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE  = 1;

    /**
     * @return bool|array|object
     */
    public static function getRecords()
    {
        $records = self::select('name', 'text', 'image')
            ->where('status', 1)
            ->orderBy('sort', 'asc')
            ->orderBy('id', 'desc')
            ->get();

        if (!$records) {
            return false;
        }

        return $records;
    }
}
