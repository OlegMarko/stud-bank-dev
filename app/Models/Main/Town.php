<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    protected $table = 'towns';

    /**
     * Display town status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE = 1;
}
