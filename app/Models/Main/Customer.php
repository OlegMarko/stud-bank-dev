<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class Customer extends Model
{
    //
    protected $table = 'users';

    /**
     * @param $action
     * @return bool|mixed
     */
    public static function getRulesByKey($action)
    {
        $array = [
            'main' => [
                'name' => 'required|min:2',
                'surname' => 'required|min:2',
                'login' => 'required|min:2',
            ],

            'password' => [
                'old_password' => 'required|old_password:' . Auth::user()->password,
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
            ],

            'contact' => [
                'email' => 'required|email|unique:users,email,' . Auth::user()->id
            ]
        ];

        return array_key_exists($action, $array) ? $array[$action] : false;
    }

    /**
     * @param $action
     * @param $array
     * @return mixed
     */
    public static function validate($action, $array)
    {
        $rules = self::getRulesByKey($action);

        $message = [
            'surname.required' => 'Поле обязательно для заполнения',
            'surname.min' => 'Поле слишком короткое',
            'login.required' => 'Поле обязательно для заполнения',
            'name.required' => 'Поле обязательно для заполнения',
            'name.min' => 'Поле слишком короткое',
            'old_password.old_password' => 'Старый пароль введен неверно',
        ];

        $validator = Validator::make($array->all(), $rules, $message);

        return $validator;
    }

    /**
     * @param $request
     * @param $user
     * @return string
     */
    public static function changePassword($request, $user)
    {
        if (Hash::check($request->old_password, $user->password)) {
            $model = self::where('id', $user->id)->first();
            $model->password = Hash::make($request->password);
            $result = $model->save();
        } else {
            $result = json_encode(['old_password' => 'Старый пароль введен неверно']);
        }

        return $result;
    }

    /**
     * update main and education information for user
     * @param $array
     * @return mixed
     */
    public static function updateMain($array)
    {
        return self::where('id', Auth::user()->id)->update($array);
    }

    /**
     * @param $request
     * @return bool
     */
    public static function updateContactData($request)
    {
        $resultForMain = self::where('id', Auth::user()->id)->update(['email' => $request->email]);

        if ($request->has('phone')) {
            UserPhone::addPhoneByUserId(Auth::user()->id, $request->phone);
        }

        return ($resultForMain) ? true : false;
    }


    /**
     * @return mixed
     */
    protected static function getUser()
    {
        return Auth::user();
    }

    /**
     * @return array
     */
    public static function getArrayYears()
    {
        $array = [];
        $range = range(date('Y'), '1950');
        foreach ($range as $value) {
            $array[$value] = $value;
        }
        return $array;
    }
}
