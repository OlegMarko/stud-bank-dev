<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteSettings
 * @package App\Models\Main
 */
class SiteSettings extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'site_settings';

    /**
     * @const array
     */
    const KEYS_SOCIAL_LINKS = ['facebook_footer_link','twitter_footer_link','google_footer_link','vk_footer_link'];

    /**
     * @param $key
     * @return string
     */
    public static function getSetting($key)
    {
        $model = self::where('key', $key)->first();;
        if ($model) {
            return $model->value;
        }

        return 'There is no setting with key - ' . $key;
    }

    /**
     * @param array $keysArray
     * @return array
     */
    public static function getMultiSettings(array $keysArray){
        $array = [];
        $models = self::whereIn('key', $keysArray)->get();

        foreach ($models as $model){
            $array[$model->key] = $model->value;
        }

        return $array;
    }

}