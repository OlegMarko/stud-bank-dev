<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User as BaseUser;

/**
 * Class User
 * @package App\Models\Main
 */
class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname',
        'login', 'phone', 'city', 'university',
        'specialty', 'study_finish_date', 'mail_subscription', 'role'
    ];

    /**
     * @param int $int
     * @return string
     */
    public static function getUrlByInt($int)
    {
        return in_array($int,[BaseUser::ROLE_STUDENT,BaseUser::ROLE_AUTHOR]) ? 'profile' : '404';
    }

    /**
     * @param string $str
     * @return mixed
     */
    public static function setHashForPassword($str)
    {
        return Hash::make($str);
    }

    /**
     * @param array $array
     * @return boolean
     */
    public static function registerUser($array)
    {
        $user = self::create($array);
        if ($user->id) {
            return true;
        }
        return false;
    }

    /**
     * @param $array
     * @return mixed
     */
    public static function validate($array)
    {
        $rules = [
            'surname' => 'min:6',
            'login' => 'unique:users,login',
            'phone' => 'required|unique:users,phone',
            'email' => 'unique:users,email',
        ];

        $messages = [
            'surname.min' => 'Фамилия слишком короткая'
        ];

        return Validator::make($array, $rules, $messages);;
    }
}
