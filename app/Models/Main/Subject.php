<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';

    /**
     * Display subject status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE = 1;
}
