<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use App\CoreClasses\Helpers\StringHelper;
use Illuminate\Support\Facades\Auth;

class UserSubject extends Model
{
    protected $table = 'user_subjects';
    protected $fillable = ['user_id', 'subject_id'];

    public $timestamps = false;

    public static function getCurrentList(){
        $array = self::where('user_id', Auth::user()->id)->get()->toArray();
        if (count($array)) {
            $collection = collect($array);
            $plucked = $collection->pluck('subject_id');

            return $plucked->all();
        }
    }

    public static function addSubjectByUserId($userId,$strIds){
        self::where('user_id', $userId)->delete();
        if (!is_null($strIds)) {
            $arrayIds = StringHelper::makeArrayBySymbol($strIds);
            $output = [];

            foreach ($arrayIds as $subjectId) {
                $output[] = [
                    'user_id' => $userId,
                    'subject_id' => $subjectId
                ];
            }

            return self::insert($output);
        }
    }
}
