<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;
use App\CoreClasses\Helpers\StringHelper;
use Illuminate\Support\Facades\Auth;

class UserTypeWork extends Model
{
    protected $table = 'user_types_works';
    protected $fillable = ['user_id', 'phone'];

    public $timestamps = false;

    /**
     * @param $user_id
     * @param $strIds
     * @return string
     */
    public static function addTypeWorkByUserId($user_id, $strIds)
    {
        self::where('user_id', $user_id)->delete();
        if (!is_null($strIds)) {
            $arrayIds = StringHelper::makeArrayBySymbol($strIds);
            $output = [];

            foreach ($arrayIds as $typeId) {
                $output[] = [
                    'user_id' => $user_id,
                    'type_work_id' => $typeId
                ];
            }
            return self::insert($output);
        }
    }

    /**
     * @return array
     */
    public static function getCurrentList()
    {
        $array = self::where('user_id', Auth::user()->id)->get()->toArray();
        if (count($array)) {
            $collection = collect($array);
            $plucked = $collection->pluck('type_work_id');

            return $plucked->all();
        }
    }
}
