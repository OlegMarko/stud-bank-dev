<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{

    protected $table = 'disciplines';

    /**
     * Display discipline status
     */
    const DISPLAY_FALSE = 0;
    const DISPLAY_TRUE = 1;

    /**
     * @return array
     */
    public static function getList()
    {
        $array = [];

        $models = self::select('id', 'title')
            ->where('status', self::DISPLAY_TRUE)
            ->orderBy('sort', 'asc')
            ->get();

        foreach ($models as $model) {
            $array[$model->id] = $model->title;
        }

        return $array;
    }

    /**
     * @param $iteration
     * @param null $prefix
     * @return null|string
     */
    public function setActive($iteration, $prefix = null)
    {
        $str = '';
        if (!is_null($prefix)) {
            $str .= ' ' . $prefix.' ';
        }

        return ($iteration == 1) ?  $str.'active' : null;
    }

    /**
     * @return array
     */
    public static function getListWithItems()
    {
        $output = [];
        $i = 1;
        $models = self::where('status', self::DISPLAY_TRUE)->orderBy('sort', 'asc')->get();
        foreach ($models as $model) {
            $output[$model->title]['model'] = $model;
            $output[$model->title]['tabs'] = 'tabs_' . $i;
            $output[$model->title]['child'] = DisciplineSubject::getListByDisciplineId($model->id);
            $i++;
        }

        return $output;
    }
}
