<?php

namespace App\Models\Admin;

use App\Models\Main\ScienceDiscipline;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Discipline extends Model
{
    use CrudTrait;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $table = 'disciplines';
    //protected $primaryKey = 'id';
    public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['title', 'sort', 'status'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return string
     */
    public function getSlugWithLink()
    {
        $str = 'Не добавлена';

        $science = ScienceDiscipline::getScienceByIdDiscipline($this->id);

        if (!is_null($science)) {
            $str = '<a href="/admin/science/' . $science->id . '/edit" target="_blank">' . $science->title . '</a>';
        }
        return $str;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
