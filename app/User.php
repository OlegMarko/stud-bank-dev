<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable, CrudTrait, HasRoles;

    /**
     * User roles
     */
    const ROLE_STUDENT = 1;
    const ROLE_AUTHOR = 2;
    const ROLE_ADMIN = 3;
    const ROLE_MANAGER = 4;

    /**
     * Register checkbox "Я агенство"
     */
    const IS_AGENT_TRUE = 1;
    const IS_AGENT_FALSE = 0;

    /**
     * User mail subscription when registered
     */
    const NEWS_SUBSCRIPTION_FALSE = 0;
    const NEWS_SUBSCRIPTION_TRUE = 1;

    /**
     *
     */
    const NOTIFY_SUBSCRIPTION_FALSE = 0;
    const NOTIFY_SUBSCRIPTION_TRUE = 1;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * @return bool
     */
    public function isAdministratorMember()
    {
        if (in_array($this->getAttribute('role'), [User::ROLE_ADMIN, User::ROLE_MANAGER])) {
            return true;
        }
        return false;
    }

    /**
     * @return integer
     */
    public static function getRoleInteger(){
        return Auth::user()->role;
    }

    /**
     * @return string
     */
    public function getUserTown()
    {
        return 'г. '.ucfirst(Auth::user()->city);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $user = Auth::user();
        return ucfirst($user->name) . ' ' . ucfirst($user->surname);
    }
}
