<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21.06.17
 * Time: 18:14
 */

namespace App\CoreClasses\Helpers;

/**
 * Class StringHelper
 * @package App\CoreClasses\Helpers
 */
class StringHelper
{
    /**
     * @param $str
     * @param string $symbol
     * @return array
     */
    public static function makeArrayBySymbol($str, $symbol = ',')
    {
        $output = explode($symbol, $str);

        return $output;
    }

    /**
     * @param $title
     * @return mixed|string
     */
    public static function makeTranslateForSlug($title)
    {
        $tr = array(
            "А" => "A",
            "Б" => "B",
            "В" => "V",
            "Г" => "G",
            "Д" => "D",
            "Е" => "E",
            "Ё" => "E",
            "Ж" => "J",
            "З" => "Z",
            "И" => "I",
            "Й" => "Y",
            "К" => "K",
            "Л" => "L",
            "М" => "M",
            "Н" => "N",
            "О" => "O",
            "П" => "P",
            "Р" => "R",
            "С" => "S",
            "Т" => "T",
            "У" => "U",
            "Ф" => "F",
            "Х" => "H",
            "Ц" => "TS",
            "Ч" => "CH",
            "Ш" => "SH",
            "Щ" => "SCH",
            "Ъ" => "",
            "Ы" => "YI",
            "Ь" => "",
            "Э" => "E",
            "Ю" => "YU",
            "Я" => "YA",
            "а" => "a",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "д" => "d",
            "е" => "e",
            "ё" => "e",
            "ж" => "j",
            "з" => "z",
            "и" => "i",
            "й" => "y",
            "к" => "k",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "о" => "o",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "u",
            "ф" => "f",
            "х" => "h",
            "ц" => "ts",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "sch",
            "ъ" => "y",
            "ы" => "yi",
            "ь" => "",
            "э" => "e",
            "ю" => "yu",
            "я" => "ya",
            "«" => "",
            "»" => "",
            "№" => "",
            "Ӏ" => "",
            "’" => "",
            "ˮ" => "",
            "_" => "-",
            "'" => "",
            "`" => "",
            "^" => "",
            "\." => "",
            "," => "",
            ":" => "",
            "<" => "",
            ">" => "",
            "!" => ""
        );

        foreach ($tr as $ru => $en) {
            $title = mb_eregi_replace($ru, $en, $title);
        }

        $title = mb_strtolower($title);

        $title = str_replace(' ', '-', $title);
        return $title;
    }
}