<?php

namespace App\CoreClasses\BaseControllers;

use App\Http\Controllers\Controller as BaseAppController;

/**
 * Class AbstractAjaxController
 * @package App\CoreClasses\Abstracted\Controllers
 */
class BaseAjaxController extends BaseAppController
{
    /**
     * Here is default response data
     * @var array
     */
    private $response = ['result' => false, 'data' => null, 'statusCode' => 500];

    /**
     * @return string
     */
    protected function getResponse()
    {
        return response()->json($this->response);
    }

    /**
     * @param array $responseArray
     * @return $this
     */
    protected function setResponse($responseArray)
    {
        $this->response = $responseArray;
        return $this;
    }

    /**
     * @param bool $responseResult
     * @return $this
     */
    protected function setResponseResult($responseResult)
    {
        $this->response['result'] = $responseResult;
        return $this;
    }

    /**
     * @param $responseData
     * @return $this
     */
    protected function setResponseData($responseData)
    {
        $this->response['data'] = $responseData;
        return $this;
    }

    /**
     * @param int $responseStatusCode
     * @return $this
     */
    protected function setResponseStatusCode($responseStatusCode)
    {
        $this->response['statusCode'] = $responseStatusCode;
        return $this;
    }
}
