<?php

namespace App\CoreClasses\BaseControllers;

use App\Http\Controllers\Controller as BaseAppController;
use App\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class BaseProfileController
 * @package App\CoreClasses\BaseControllers
 */
class BaseProfileController extends BaseAppController
{
    const AUTHOR_DIRNAME = 'author';
    const CUSTOMER_DIRNAME = 'customer';

    protected $layout;
    private $viewPathTemplate = 'main.[viewDir].[view]';

    /**
     * @param $view
     * @return mixed
     */
    protected function getProfileViewPath($view)
    {
        if(User::getRoleInteger() == User::ROLE_AUTHOR) {
            return $this->prepareViewPath(self::AUTHOR_DIRNAME, $view);
        }

        return $this->prepareViewPath(self::CUSTOMER_DIRNAME, $view);
    }

    /**
     * @param $viewDir
     * @param $view
     * @return mixed
     */
    private function prepareViewPath($viewDir, $view)
    {
        $preparedView = str_replace('[viewDir]', $viewDir, $this->viewPathTemplate);
        $preparedView = str_replace('[view]', $view, $preparedView);

        return $preparedView;
    }
}
