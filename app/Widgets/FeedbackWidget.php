<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Main\Feedback as FeedBackModel;

/**
 * Class FeedbackWidget
 * @package App\Widgets
 */
class FeedbackWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    public $displayArrows, $items, $totalCount;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $this->items = FeedBackModel::getRecords();
        $this->totalCount = count($this->items);
        $this->displayArrows = (count($this->items) > 1);

        return view('widgets.feedBackWidget.feed_back', ['widget' => $this]);
    }

    /**
     *
     * @param $loopIteration
     * @return string
     */
    public function getImageClass($loopIteration)
    {
        $className = 'hidden';
        if(in_array($loopIteration,[1, 3])){
            $className = 'opacity45';
        }elseif($loopIteration == 2){
            $className = '';
        }

        return $className;
    }

    /**
     * @param $loopIteration
     * @return string
     */
    public function getTextClass($loopIteration)
    {
        return ($loopIteration != 2) ? 'hidden' : '';;
    }
}
