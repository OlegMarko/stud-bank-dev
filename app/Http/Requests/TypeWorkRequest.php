<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TypeWorkRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $titleRule = 'unique:type_works,title';

        if (!is_null($this->sort)) {
            $rules['sort'] = 'integer';
        }
        if(isset($this->id)){
            $titleRule .= ','.$this->id;
        }

        $rules['title'] = 'required|'.$titleRule;

        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Поле Тип работы обязательно к заполнению',
            'title.unique' => 'Введеный тип работы уже есть в базе',
        ];
    }
}
