<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FeedbackRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'sort' => 'integer',
            'text' => 'required',
            'image' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'image.required' => 'Поле изображение обязательно для заполнения',
            'name.required' => 'Поле Имя автора обязательно для заполнения',
            'sort.integer' => 'Поле Сортировка может быть только числом ',
            'text.required' => 'Поле Текст обязательно для заполнения'
        ];
    }
}
