<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ManualRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slugRule = 'required|regex:/manual/|unique:pages_manuals,slug';

        if(isset($this->id)){
            $slugRule .= ','.$this->id;
        }

        return [
             'h2' => 'required',
             'slug' => $slugRule,
             'text' => 'required',
             'sort' => 'integer'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'id'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'text.required'=>'Поле Текст обязательно для заполнения',
            'h2.required'=>'Поле H2 обязательно для заполнения',
            'sort.integer'=>'Поле Сортировка должна быть цифрой',
            'slug.unique'=>'Данная ссылка уже есть в базе',
            'slug.regex'=>'Ссылка в данном разделе должна начинатся на /manual/*',
        ];
    }
}
