<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannerRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'image' => 'required',
        ];

        if(!is_null($this->url)){
            $rules['url'] = 'url';
        }

        if(!is_null($this->sort)){
            $rules['sort'] = 'integer';
        }
        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'image.dimensions' => 'Изображение должно горизонтальным минимум 1100х300 px',
            'image.mimes' => 'Изображение должно быть одним из расширений (jpeg,jpg,png)',
            'sort.integer' => 'Поле сортировка может быть только числом',
            'url.url' => 'В поле ссылку можно внести только ссылку',
        ];
    }
}
