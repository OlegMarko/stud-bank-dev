<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DisciplineRequest as StoreRequest;
use App\Http\Requests\DisciplineRequest as UpdateRequest;
use App\Models\Main\Science;
use App\Models\Main\ScienceDiscipline;

class DisciplineCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Admin\Discipline');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/discipline');
        $this->crud->setEntityNameStrings('дисциплину', 'дисциплины');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        // ------ CRUD FIELDS

        $this->crud->addField( [
            'name' => 'science_id',
            'label' => 'Наука',
            'type' => 'select_from_array',
            'options' => Science::getList(),
        ], 'update/create')
            ->beforeField('sort');

//        $this->crud->addField( [
//            'label' => "Tags",
//            'type' => 'select_multiple',
//            'name' => 'tags', // the method that defines the relationship in your Model
//            'entity' => 'tags', // the method that defines the relationship in your Model
//            'attribute' => 'title', // foreign key attribute that is shown to user
//            'model' => "App\Models\Main\Science", // foreign key model
////            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
//        ], 'update/create')
//            ->beforeField('sort');
//
//        $this->crud->addField([  // Select
//            'label' => "Category",
//            'type' => 'select',
//            'name' => 'category_id', // the db column for the foreign key
//            'entity' => 'category', // the method that defines the relationship in your Model
//            'attribute' => 'name', // foreign key attribute that is shown to user
//            'model' => "App\Models\Main\Science" // foreign key model
//        ], 'update')
//            ->beforeField('sort');

        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Название дисциплины',
                'type' => 'text',
            ],
            [
                'name' => 'sort',
                'label' => 'Сортировка',
                'type' => 'text',
            ],
            [
                'name' => 'status',
                'label' => 'Активен',
                'type' => 'radio',
                'options'     => [
                    1 => "Опубликовать",
                    0 => "Не опубликовано"
                ]
            ]
        ], 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS

        $this->crud->removeColumns(['title','status','sort']); // remove a column from the stack

        $this->crud->addColumn([
            'name' => 'title',
            'label' => 'Название дисципины',
            'type' => 'text',
        ]);


//        $this->crud->addColumn([
////            'name' => 'to_science',
////            'label' => '',
//            // 1-n relationship
//            'label' => "Относится к", // Table column heading
//            'type' => "text",
//            'name' => 'science', // the column that contains the ID of that connected entity;
//            'entity' => 'parent', // the method that defines the relationship in your Model
//            'attribute' => "name", // foreign key attribute that is shown to user
//            'model' => "App\Models\Category", // foreign key model
//
//        ]);




        $this->crud->addColumn([// run a function on the CRUD model and show its return value
            'label' => "Науки", // Table column heading
           'type' => "model_function",
           'function_name' => 'getSlugWithLink', // the method in your Model
        ]);

        $this->crud->addColumn([
            'name' => 'sort',
            'label' => 'Сортировка',
            'type' => 'text',
            // optionally override the Yes/No texts

        ]);
        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Статус',
            'type' => 'boolean',
            'options' => [1 => 'Активно', 0 => 'Не активно']
        ]);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
         $this->crud->orderBy('status','desc');
         $this->crud->orderBy('sort','asc');
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['saveAction'] = $this->getSaveAction();

        $this->data['current_option'] = ScienceDiscipline::getScienceIdByDisciplineId($id);
        $this->data['id'] = $id;

        return view('crud::edit', $this->data);
    }

    public function store(StoreRequest $request)
    {

        // your additional operations before save here
        $redirect_location = parent::storeCrud();

        $science_id = $request->science_id;
        $discipline_id = $this->crud->getModel()->max('id');

        ScienceDiscipline::addScienceForDiscipline($science_id,$discipline_id);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {

        ScienceDiscipline::addScienceForDiscipline($request->science_id,$request->id);
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
