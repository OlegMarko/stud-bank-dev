<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BannerRequest as StoreRequest;
use App\Http\Requests\BannerRequest as UpdateRequest;

class BannerCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Admin\Banner');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/banner');
        $this->crud->setEntityNameStrings('баннер', 'баннеры');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        $this->crud->addFields([
            [
                'name' => 'image',
                'label' => 'Изображение баннера *',
                'type' => 'image',
                'hint'   => 'Изображение должно быть горизонтальным',
                'crop' => true, // set to true to allow cropping, false to disable
//                'aspect_ratio' => 1 // ommit or set to 0 to allow any aspect ratio
                'prefix' => '/uploads/'
            ],
            [
                'name' => 'title',
                'label' => 'Подпись',
                'type' => 'text',
                'hint'   => 'Можно не заполнять',
            ],
            [
                'name' => 'url',
                'label' => 'Ссылка',
                'type' => 'text',
                'hint'   => 'Можно не заполнять',
            ],
            [
                'name' => 'button_text',
                'label' => 'Текст кнопки',
                'type' => 'text',
                'hint'   => 'Можно не заполнять',
            ],
            [
                'name' => 'sort',
                'label' => 'Сортировка',
                'type' => 'text',
            ],
            [
                'name' => 'position',
                'label' => 'Для кого баннер',
                'type' => 'radio',
                'default' => 2,
                'options'     => [
                    2 => "Для всех",
                    1 => "Для заказчика",
                    0 => "Для автора"
                ]
            ],
            [
                'name' => 'status',
                'label' => 'Активен',
                'type' => 'radio',
                'options'     => [
                    1 => "Опубликовать",
                    0 => "Не опубликовано"
                ]
            ]
        ], 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->removeColumns(['image','title','status','position','button_text','sort','url']); // remove a column from the stack

        $this->crud->addColumn([
                'name' => 'image',
                'type' => 'model_function',
                'function_name' => 'getImageBanner'
        ]);
        $this->crud->addColumn([
                'name' => 'status',
                'label' => 'Статус',
                'type' => 'boolean',
                // optionally override the Yes/No texts
                 'options' => [1 => 'Активен', 0 => 'Отключен']

        ]);
        $this->crud->addColumn([
                'name' => 'sort',
                'label' => 'Сортировка',
                'type' => 'text',
                // optionally override the Yes/No texts

        ]);

        $this->crud->addColumn([
                'name' => 'position',
                'label' => 'Для кого баннер',
                'type' => 'radio',
                // optionally override the Yes/No texts
                 'options' => [0 => 'Авторов', 1 => 'Заказчиков', 2 => 'Всех']

        ]);

        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
         $this->crud->setColumnDetails('position', ['label' => 'Позиция']); // adjusts the properties of the passed in column (by name)
         $this->crud->setColumnDetails('sort', ['label' => 'Сортировка']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
         $this->crud->orderBy('position','desc');
         $this->crud->orderBy('sort','asc');
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
