<?php
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthorProfileController extends Controller
{
    public function index(){
        return view('main.author.profile.index');
    }
}
