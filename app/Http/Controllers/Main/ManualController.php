<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Main\Seo;
use App\Models\Main\Manual;

/**
 * Class ManualController
 * @package App\Http\Controllers\Main
 */
class ManualController extends Controller
{
    /**
     * @param $url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($url){
        return view('pages.manual.view',[
            'page'=>Manual::findByUrl($url),
            'seoData' => Seo::getSeoData($this->getCurrentRoute())
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

        return view('pages.manual.index',[
            'page'=>Manual::findByUrl('/'),
            'seoData' => Seo::getSeoData($this->getCurrentRoute())
        ]);
    }
}
