<?php

namespace App\Http\Controllers\Main;

use App\CoreClasses\BaseControllers\BaseAjaxController;
use App\Models\Main\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerOptionController extends BaseAjaxController
{

    /** for validate key array in Model Author
     * @var string
     */
    private $action = '';

    /** list methods in controller
     * @var array
     */
    private $actions = [
        'add-template', 'main', 'education',
        'type_work', 'subject', 'contact',
        'password', 'add_template', 'delete_template',
        'edit_template'];

    /**
     * @param $action
     * @param Request $request
     * @return string
     */
    public function doAction($action, Request $request)
    {        
        if (in_array($action, $this->actions)) {
            $this->action = $action;
            $result = $this->{$action}($request);

            ($result == true) ? $this->setResponseResult(true) : $this->setResponseResult(false);
            return $this->getResponse();
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('main.customer.options.index');
    }

    /**
     * @param $request
     * @return mixed
     */
    public function main($request)
    {
        $validator = Customer::validate($this->action, $request);
        if ($validator->fails()) {
            return $validator->validate();
        }

        return Customer::updateMain($request->all());

        //$validator = Customer::validate($this->action, $request);
        //if ($validator->fails()) {
        //    return $validator->validate();
        //}

        //return Customer::updateMain($request->all());
    }

    /**
     * @param $request
     * @return mixed
     */
    public function education($request)
    {
//        $validator = Author::validate($this->action, $request);
//        if ($validator->fails()) {
//            return $validator->validate();
//        }

        return Customer::updateMain($request->all());
    }

    /**
     * @param $request
     * @return bool
     */
    public function contact($request)
    {
        return Customer::updateContactData($request);
    }

    /**
     * change password for user
     * @param $request
     * @return string
     */
    public function password($request)
    {
        $validator = Customer::validate($this->action, $request);
        if ($validator->fails()) {
            return $validator->validate();
        }
        return Customer::changePassword($request, Auth::user());
    }

    /**
     * @param $action
     * @param Request $request
     * @return string
     */
    public function save ($action, Request $request)
    {
        if (method_exists(self::class, $action)) {
            return $this->getResponse();
        }

        //return $this->getResponse();
        //dd($request);
    }

}