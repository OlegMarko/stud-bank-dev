<?php

namespace App\Http\Controllers\Main;

use App\CoreClasses\BaseControllers\BaseProfileController;
use Illuminate\Support\Facades\Auth;

class ProfileController extends BaseProfileController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        return view($this->getProfileViewPath('profile'), [

        ]);
    }
}
