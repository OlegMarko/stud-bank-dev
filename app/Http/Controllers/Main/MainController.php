<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Main\Seo;

/**
 * Class MainController
 * @package App\Http\Controllers\Auth
 */
class MainController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('main.views.index',[
            'seoData' => Seo::getSeoData($this->getCurrentRoute())
        ]);
    }

    public function authors()
    {
        return view('main.views.authors');
    }

    public function advices()
    {
        return view('main.views.advices');
    }
}
