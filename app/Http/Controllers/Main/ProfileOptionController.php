<?php

namespace App\Http\Controllers\Main;

use App\CoreClasses\BaseControllers\BaseProfileController;
use App\Models\Main\Author;
use App\Models\Main\Customer;
use App\Models\Main\Discipline;
use App\Models\Main\Faculty;
use App\Models\Main\Speciality;
use App\Models\Main\TypeWork;
use App\Models\Main\UserPhone;
use App\Models\Main\UserTypeWork;
use App\Models\Main\TemplateResponse;
use Illuminate\Support\Facades\Auth;

class ProfileOptionController extends BaseProfileController
{
    public function index()
    {
        return view($this->getProfileViewPath('options.index'), [
            'user' => Auth::user(),
            'arrayYears' => Author::getArrayYears(),
            'arrayYearsCustomer' => Customer::getArrayYears(),
            'faculties' => Faculty::getList(),
            'specialities' => Speciality::getList(),
            'phones' => UserPhone::getList(),
            'typeWorks' => TypeWork::getList(),
            'disciplinesWithSubjects' => Discipline::getListWithItems(),
            'templatesResponse' => TemplateResponse::getListJson(),
        ]);
    }
}
