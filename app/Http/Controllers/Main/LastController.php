<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LastController extends Controller
{
    public function index(){
        return view('main.last.index');
    }
}
