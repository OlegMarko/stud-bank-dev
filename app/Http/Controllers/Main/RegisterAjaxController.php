<?php

namespace App\Http\Controllers\Main;

use App\CoreClasses\BaseControllers\BaseAjaxController;
use Illuminate\Http\Request;
use App\Models\Main\User;
use Illuminate\Support\Facades\Auth;

class RegisterAjaxController extends BaseAjaxController
{
    /**
     * @param Request $request
     * @return string
     */
    public function register(Request $request)
    {
        $validate = User::validate($request->all());

        if ($validate->passes()) {
            $passwordForAuth = $request->password;
            $request->merge(['password' => User::setHashForPassword($request->password)]);
            $result = User::registerUser($request->all());
            if ($result == true) {
                Auth::attempt(['email' => $request->email, 'password' => $passwordForAuth]);

                $this->setResponseResult(true);
                $this->setResponseData(User::getUrlByInt($request->role));
            }
        } else {
            $this->setResponseData($validate->validate());
        }

        return $this->getResponse();
    }
}
