<?php


namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\CoreClasses\BaseControllers\BaseAjaxController;
use Illuminate\Support\Facades\Auth;

/**
 * Class AjaxController
 * @package App\Http\Controllers\Main
 */
class AuthAjaxController extends BaseAjaxController
{

    /**
     * @param Request $request
     * @return string
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->has('remember'))) {
            $this->setResponseResult(true);
            if (Auth::user()->isAdministratorMember()) {
                $this->setResponseData('/admin');
            } else {
                $this->setResponseData('/profile');
            }
        } else {
            $this->setResponseData('Неправильный логин или пароль');
        }

        return $this->getResponse();
    }

}