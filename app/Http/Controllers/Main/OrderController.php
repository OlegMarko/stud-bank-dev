<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        return view('main.order.index');
    }

    public function success(){
        return view('main.order.success');
    }
}
