<?php


namespace App\Http\Controllers\Main;

use App\CoreClasses\BaseControllers\BaseAjaxController;

/**
 * Class AjaxController
 * @package App\Http\Controllers\Main
 */
class AjaxController extends BaseAjaxController
{
    /**
     * @return string
     */
    public function test()
    {
        return $this->getResponse();
    }

}