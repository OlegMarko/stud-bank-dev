<?php

namespace App\Http\Controllers\Main;

use App\CoreClasses\BaseControllers\BaseAjaxController;
use App\Models\Main\Author;
use App\Models\Main\TemplateResponse;
use App\Models\Main\UserSubject;
use App\Models\Main\UserTypeWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class AuthorOptionController extends BaseAjaxController
{

    /** for validate key array in Model Author
     * @var string
     */
    private $action = '';

    /** list methods in controller
     * @var array
     */
    private $actions = [
        'add-template', 'main', 'education',
        'type_work', 'subject', 'contact',
        'password', 'add_template', 'delete_template',
        'edit_template'];

    /**
     * @param $action
     * @param Request $request
     * @return string
     */
    public function doAction($action, Request $request)
    {
        if (in_array($action, $this->actions)) {
            $this->action = $action;
            $result = $this->{$action}($request);

            ($result == true) ? $this->setResponseResult(true) : $this->setResponseResult(false);
            return $this->getResponse();
        }
    }

    /** update main and education information author
     * @param $request
     * @return mixed
     */
    public function main($request)
    {
        $validator = Author::validate($this->action, $request);
        if ($validator->fails()) {
            return $validator->validate();
        }

        return Author::updateMain($request->all());
    }

    /**
     * @param $request
     * @return string
     */
    public function type_work($request)
    {
//        $validator = Author::validate($this->action, $request);
//        if ($validator->fails()) {
//            return $validator->validate();
//        }

        return UserTypeWork::addTypeWorkByUserId(Auth::user()->id, $request->types);
    }

    /**
     * @param $request
     * @return bool
     */
    public function add_template($request)
    {
        return TemplateResponse::addTemplateByUserId(Auth::user()->id, $request);
    }

    /**
     * @param $request
     * @return int
     */
    public function delete_template($request)
    {
        return TemplateResponse::destroy([$request->id]);
    }

    public function edit_template($request)
    {
        return TemplateResponse::edit($request);
    }

    /**
     * change password for user
     * @param $request
     * @return string
     */
    public function password($request)
    {
        $validator = Author::validate($this->action, $request);
        if ($validator->fails()) {
            return $validator->validate();
        }
        return Author::changePassword($request, Auth::user());
    }

    /**
     * @param $request
     * @return mixed
     */
    public function subject($request)
    {
        return UserSubject::addSubjectByUserId(Auth::user()->id, $request->subjects);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function education($request)
    {
//        $validator = Author::validate($this->action, $request);
//        if ($validator->fails()) {
//            return $validator->validate();
//        }

        return Author::updateMain($request->all());
    }

    /**
     * @param $request
     * @return bool
     */
    public function contact($request)
    {
        return Author::updateContactData($request);
    }

    /**
     * @param $action
     * @param Request $request
     * @return string
     */
    public function edit($action, Request $request)
    {
        $validator = Author::validate($action, $request);
        if (!$validator->fails()) {
            $result = Author::updateOption($request->all(), $action);
            $this->setResponseResult(true);
            $this->setResponseData($result);
        } else {
            return $validator->validate();
        }

        return $this->getResponse();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('main.author.options.index');
    }
}
