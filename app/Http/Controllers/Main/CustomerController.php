<?php
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function index(){
        return view('main.customer.index');
    }
}
