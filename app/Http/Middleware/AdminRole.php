<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class AdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->role == User::ROLE_ADMIN){
            return $next($request);
        }else{
            return redirect('404');
        }
    }
}
