<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CustomerRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->role == User::ROLE_STUDENT){
            return $next($request);
        }
    }
}
