<?php
/**
 *
 */
?>
@extends('main.layout')

@section('content')
    <div id="home">
        <nav id="nav">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 col-md-3">
                        <a href="#" class="logo"><img src="{{asset('img')}}/logo.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="col-xs-2 col-sm-3 col-md-5">
                        <ul class="row text-center hidden-xs hidden-sm">
                            <li role="presentation" class="col-md-5"><a href="#" class="dashed"><span>Как мы работаем</span></a>
                            </li>
                            <li role="presentation" class="col-md-4"><a href="#">Готовые работы</a></li>
                            <li role="presentation" class="col-md-3"><a href="#">Вы автор?</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-2 col-md-1">
                        @if(\Illuminate\Support\Facades\Auth::guest())
                            <a href="#" class="btn btn-link btn-sm" data-toggle="modal" data-target="#login">Вход</a>

                        @else
                            <a href="/logout" class="btn btn-link btn-sm">Выход</a>
                            @if(\Illuminate\Support\Facades\Auth::user()->isAdministratorMember())
                                <a href="/admin" class="btn btn-link btn-sm">Админка</a>
                            @endif
                        @endif
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-2">
                        <a href="#" class="btn btn-reg btn-sm" data-toggle="modal" data-target="#register">Регистрация</a>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container">
            <p class="big1">Контрольные, курсовые и дипломные работы</p>
            <p class="text-center">
                <small>Выберите лично исполнителя для своей работы</small>
            </p>
            <form class="row center-block">
                <div class="form-group form-group-lg col-xs-12 col-sm-4">
                    <select class="form-control">
                        <option>Тип работы</option>
                        <option>Тип работы</option>
                        <option>Тип работы</option>
                    </select>
                </div>
                <div class="form-group form-group-lg col-xs-12 col-sm-4">
                    <input type="email" class="form-control" id="" placeholder="Ваш e-mail">
                </div>
                <div class="form-group form-group-lg col-xs-12 col-sm-4">
                    <button type="submit" class="btn btn-default">Продолжить</button>
                    <!--<p class="text-center"><a href="#">Вы автор?</a></p>-->
                </div>
            </form>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <span class="icony icon7"></span>
                    <h4>Короткие сроки</h4>
                    <small>Сроки написания работы — <br>от трех часов.</small>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <span class="icony icon8"></span>
                    <h4>Гарантия</h4>
                    <small>Автор бесплатно будет вносить корректировки по выполненному заказу</small>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <span class="icony icon9"></span>
                    <h4>Минимальная<br> стоимость</h4>
                    <small>от 15 руб. за страницу</small>
                </div>
            </div>
        </div>
        <div class="container-fluid text-center">
            <p class="big2">Закажите работу в WorkPrice - успех гарантирован!</p>
            <div class="row chars">
                <div class="col-xs-6 col-sm-3 col-md-2">
                    <span class="char">Д</span>
                    <p><a href="#">Дипломная работа</a></p>
                    <small><strong>от 3000 руб</strong></small>
                    <small>от 3х дней</small>
                    <a href="#" class="btn btn-success btn-sm">заказать</a>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-2">
                    <span class="char">К</span>
                    <p><a href="#">Контрольная работа</a></p>
                    <small><strong>от 3000 руб</strong></small>
                    <small>от 3х дней</small>
                    <a href="#" class="btn btn-success btn-sm">заказать</a>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-2">
                    <span class="char">К</span>
                    <p><a href="#">Контрольная работа</a></p>
                    <small><strong>от 3000 руб</strong></small>
                    <small>от 3х дней</small>
                    <a href="#" class="btn btn-success btn-sm">заказать</a>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-2">
                    <span class="char">К</span>
                    <p><a href="#">Контрольная работа</a></p>
                    <small><strong>от 3000 руб</strong></small>
                    <small>от 3х дней</small>
                    <a href="#" class="btn btn-success btn-sm">заказать</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <span class="char">К</span>
                    <p><a href="#">Контрольная работа</a></p>
                    <small><strong>от 3000 руб</strong></small>
                    <small>от 3х дней</small>
                    <a href="#" class="btn btn-success btn-sm">заказать</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <span class="char">К</span>
                    <p><a href="#">Контрольная работа</a></p>
                    <small><strong>от 3000 руб</strong></small>
                    <small>от 3х дней</small>
                    <a href="#" class="btn btn-success btn-sm">заказать</a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2">
                    <span class="char allchars"></span>
                    <p>А также другие
                    <p>
                    <p><a href="#" class="dashed"><span>студенческие работы</span></a></p>
                </div>
            </div>
            <p class="big1">Как работает наша фриланс-биржа<br>студенческих работ?</p>
            <div class="cornb"></div>
        </div>
    </div>
    <div id="plan" class="container-fluid">
        <div class="row">
            <div class="corn"></div>
            <div class="col-xs-6 plan1">
                <ul class="text-right">
                    <li><span class="icopl icopl1 hidden-xs"></span>Студент<br> создаёт<br> заказ</li>
                    <li><span class="icopl icopl2 hidden-xs"></span>Студент<br> выбирает<br> автора</li>
                    <li><span class="icopl icopl3 hidden-xs"></span>Студент<br> успешно<br> получает заказ</li>
                </ul>
                <a href="#" class="btn btn-primary">Заказать работу</a>
            </div>
            <div class="col-xs-6 plan2">
                <ul class="text-left">
                    <li><span class="icopl icopl4 hidden-xs"></span>Авторы <br>откликаются</li>
                    <li><span class="icopl icopl5 hidden-xs"></span>Автор <br>приступает <br>к работе</li>
                </ul>
                <a href="#" class="btn btn-default">Стать автором</a>
            </div>
        </div>
    </div>
    <div id="achievements" class="container leaders text-center">
        <h3>У нас ежедневно выполняются более 500 работ</h3>
        <div class="row">
            <div class="col-xs-6 col-md-3">
                <big>333</big>
                <hr>
                <small>авторов</small>
            </div>
            <div class="col-xs-6 col-md-3">
                <big>323 214</big>
                <hr>
                <small>посетителей</small>
            </div>
            <div class="col-xs-6 col-md-3">
                <big>9.4/10</big>
                <hr>
                <small>текущая оценка качества</small>
            </div>
            <div class="col-xs-6 col-md-3">
                <big>74</big>
                <hr>
                <small>авторов онлайн</small>
            </div>
        </div>
    </div>
    <div id="authors" class="container-fluid leaders text-center">
        <h3>Лучшие авторы</h3>
        <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 opacity40">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Татьяна<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 opacity70">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Ольга<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Наталья<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Елега<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 opacity70">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Светлана<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 opacity40">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Диана<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 hidden">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Галина<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 hidden">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-s.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <h4>Виктория<br>Алексеева</h4>
                        <small>Высшая математика</small>
                        <p class="text-left clearfix">
                            <span class="comments">101</span>
                            <span class="downloads">76</span>
                        </p>
                        <p class="percent">100%
                            <small>оценка успеха</small>
                        </p>
                        <p><a href="#" class="btn btn-default btn-sm" role="button">Нанять</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a href="#prev"></a>
        <a href="#next"></a>
        <p class="readmore"><a href="#">показать всех авторов</a></p>
    </div>
    <div id="latest" class="container-fluid leaders">
        <h3 class="text-center">Последние выполненные заказы</h3>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-3 opacity50">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="comments">101</span>
                        <span class="downloads">76</span>
                        <img src="{{asset('img')}}/cnt/photo-xs.png" alt="" class="img-circle pull-left">
                        <p>Андрей Киреев</p>
                        <span class="rating"><span style="width:100%;"></span></span>
                    </div>
                    <div class="panel-body">
                        <small># 53197554 | Дипломная работа
                            <time class="pull-right">15.03.2016</time>
                        </small>
                        <p><a href="#">Очень длинное название данной димпломной работы по станкостроению / 89 стр.</a></p>
                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно.
                            Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный
                            исполнитель и хороший собеседник. Удачи в</em>
                    </div>
                    <div class="panel-footer">Оценка заказчика<span class="rating"><span style="width:50%;"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="comments">101</span>
                        <span class="downloads">76</span>
                        <img src="{{asset('img')}}/cnt/photo-xs.png" alt="" class="img-circle pull-left">
                        <p>Егор Киреев</p>
                        <span class="rating"><span style="width:100%;"></span></span>
                    </div>
                    <div class="panel-body">
                        <small># 53197554 | Дипломная работа
                            <time class="pull-right">15.03.2016</time>
                        </small>
                        <p><a href="#">Очень длинное название данной димпломной работы по станкостроению / 89 стр.</a></p>
                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно.
                            Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный
                            исполнитель и хороший собеседник. Удачи в</em>
                    </div>
                    <div class="panel-footer">Оценка заказчика<span class="rating"><span style="width:50%;"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="comments">101</span>
                        <span class="downloads">76</span>
                        <img src="{{asset('img')}}/cnt/photo-xs.png" alt="" class="img-circle pull-left">
                        <p>Иван Киреев</p>
                        <span class="rating"><span style="width:100%;"></span></span>
                    </div>
                    <div class="panel-body">
                        <small># 53197554 | Дипломная работа
                            <time class="pull-right">15.03.2016</time>
                        </small>
                        <p><a href="#">Очень длинное название данной димпломной работы по станкостроению / 89 стр.</a></p>
                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно.
                            Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный
                            исполнитель и хороший собеседник. Удачи в</em>
                    </div>
                    <div class="panel-footer">Оценка заказчика<span class="rating"><span style="width:50%;"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3 opacity50">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="comments">101</span>
                        <span class="downloads">76</span>
                        <img src="{{asset('img')}}/cnt/photo-xs.png" alt="" class="img-circle pull-left">
                        <p>Дмитрий Киреев</p>
                        <span class="rating"><span style="width:100%;"></span></span>
                    </div>
                    <div class="panel-body">
                        <small># 53197554 | Дипломная работа
                            <time class="pull-right">15.03.2016</time>
                        </small>
                        <p><a href="#">Очень длинное название данной димпломной работы по станкостроению / 89 стр.</a></p>
                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно.
                            Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный
                            исполнитель и хороший собеседник. Удачи в</em>
                    </div>
                    <div class="panel-footer">Оценка заказчика<span class="rating"><span style="width:50%;"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3 hidden">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="comments">101</span>
                        <span class="downloads">76</span>
                        <img src="{{asset('img')}}/cnt/photo-xs.png" alt="" class="img-circle pull-left">
                        <p>Олег Киреев</p>
                        <span class="rating"><span style="width:100%;"></span></span>
                    </div>
                    <div class="panel-body">
                        <small># 53197554 | Дипломная работа
                            <time class="pull-right">15.03.2016</time>
                        </small>
                        <p><a href="#">Очень длинное название данной димпломной работы по станкостроению / 89 стр.</a></p>
                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно.
                            Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный
                            исполнитель и хороший собеседник. Удачи в</em>
                    </div>
                    <div class="panel-footer">Оценка заказчика<span class="rating"><span style="width:50%;"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3 hidden">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="comments">101</span>
                        <span class="downloads">76</span>
                        <img src="{{asset('img')}}/cnt/photo-xs.png" alt="" class="img-circle pull-left">
                        <p>Сергей Киреев</p>
                        <span class="rating"><span style="width:100%;"></span></span>
                    </div>
                    <div class="panel-body">
                        <small># 53197554 | Дипломная работа
                            <time class="pull-right">15.03.2016</time>
                        </small>
                        <p><a href="#">Очень длинное название данной димпломной работы по станкостроению / 89 стр.</a></p>
                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно.
                            Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный
                            исполнитель и хороший собеседник. Удачи в</em>
                    </div>
                    <div class="panel-footer">Оценка заказчика<span class="rating"><span style="width:50%;"></span></span>
                    </div>
                </div>
            </div>
        </div>
        <a href="#prev"></a>
        <a href="#next"></a>
    </div>

    {{ Widget::run('FeedbackWidget') }}

    <div class="container leaders advice">
        <h3>Советы наших авторов</h3>
        <p class="readmore"><a href="#">показать все советы</a></p>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-m.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <p class="h2">Кристина <br>Куприянова</p>
                        <small>Высшая математика</small>
                        <ul class="smalllist">
                            <li><a href="#">Пять главных правила публицистике</a></li>
                            <li><a href="#">Курсы журналистики четырёхъядерником</a></li>
                            <li><a href="#">Как Скомпонуйте журналистская</a></li>
                            <li><a href="#">Шесть Написание советы, чтобы произвести впечатление на вашего профессора</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-m.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <p class="h2">Кристина <br>Куприянова</p>
                        <small>Высшая математика</small>
                        <ul class="smalllist">
                            <li><a href="#">Пять главных правила публицистике</a></li>
                            <li><a href="#">Курсы журналистики четырёхъядерником</a></li>
                            <li><a href="#">Шесть вещей, которые вы должны ожидать, как Журналистика мажор</a></li>
                            <li><a href="#">Как Скомпонуйте журналистская</a></li>
                            <li><a href="#">Шесть Написание советы, чтобы произвести впечатление на вашего профессора</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="{{asset('img')}}/cnt/photo-m.png" alt="" class="img-circle">
                    <div class="caption">
                        <span class="rating"><span style="width:75%;"></span></span>
                        <p class="h2">Кристина <br>Куприянова</p>
                        <small>Высшая математика</small>
                        <ul class="smalllist">
                            <li><a href="#">Пять главных правила публицистике</a></li>
                            <li><a href="#">Курсы журналистики четырёхъядерником</a></li>
                            <li><a href="#">Как Скомпонуйте журналистская</a></li>
                            <li><a href="#">Шесть Написание советы, чтобы произвести впечатление на вашего профессора</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="reminder">
        <div class="container">
            <span class="icow"></span>
            <p class="h1 text-center">Все еще сомневаетесь?</p>
            <p class="text-center">Разработка презентаций, эссе, документы, работу приложений и т.д ... Планируете ли вы
                написать обширный<br>академический текст? Бакалавр или магистр диссертация, диссертация, другие
                окончательные проекты ...</p>
            <h3 class="text-center">Мы предлагаем вам индивидуальное написание консультаций</h3>
            <form class="row center-block">
                <div class="form-group form-group-lg col-xs-12 col-sm-4">
                    <p>Мне надо</p>
                    <select class="form-control">
                        <option>Эссе</option>
                        <option>Эссе</option>
                        <option>Эссе</option>
                    </select>
                </div>
                <div class="form-group form-group-lg col-xs-12 col-sm-4">
                    <p>Ваш E-mail</p>
                    <input type="email" class="form-control" id="" placeholder="help@woarkprice.net">
                </div>
                <div class="form-group form-group-lg col-xs-12 col-sm-4">
                    <p>&nbsp;</p>
                    <button type="submit" class="btn btn-default">Продолжить</button>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="jumbotron row text-center">
            <span class="icow"></span>
            <p class="h1">Мы нанимаем!</p>
            <p>Or are you a recent graduate student who loves blogging?<br>Apply here for a freelance job</p>
            <p><a class="btn btn-default btn-lg" href="#" role="button">Стать внештатным писателем</a></p>
        </div>
    </div>
    @endsection
