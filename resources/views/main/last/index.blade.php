@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="text-center">Последние выполненные заказы</h1>
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-9">
                <ul class="last">
                    <li class="block">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <span class="char-md">Д</span>
                            </div>
                            <div class="col-xs-10 col-md-11">
                                <div class="row">
                                    <div class="col-xs-8"><small># 53197554 | Дипломная работа | Педагогика | 15 страниц</small></div>
                                    <div class="col-xs-4 text-right"><strong>завершен</strong> 9 часов назад</div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 ratings">
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                    </div>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="img/cnt/photo-m1.png" alt="" class="img-circle img-responsive">
                                            </div>
                                            <div class="col-xs-9">
                                                <big>Андрей Киреев</big>
                                                <p><span class="rating"><span style="width:75%"></span></span></p>
                                                <p><span class="downloads1">45</span>
                                                    <span class="comments1">15</span>
                                                    <span class="like">54%</span></p><span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12"><a href="#" class="btn btn-default1">Запросить этого автора</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="block">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <span class="char-md">Д</span>
                            </div>
                            <div class="col-xs-10 col-md-11">
                                <div class="row">
                                    <div class="col-xs-8"><small># 53197554 | Дипломная работа | Педагогика | 15 страниц</small></div>
                                    <div class="col-xs-4 text-right"><strong>завершен</strong> 9 часов назад</div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 ratings">
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                    </div>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="img/cnt/photo-m1.png" alt="" class="img-circle img-responsive">
                                            </div>
                                            <div class="col-xs-9">
                                                <big>Андрей Киреев</big>
                                                <p><span class="rating"><span style="width:75%"></span></span></p>
                                                <p><span class="downloads1">45</span>
                                                    <span class="comments1">15</span>
                                                    <span class="like">54%</span></p><span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12"><a href="#" class="btn btn-default1">Запросить этого автора</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="block">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <span class="char-md">Д</span>
                            </div>
                            <div class="col-xs-10 col-md-11">
                                <div class="row">
                                    <div class="col-xs-8"><small># 53197554 | Дипломная работа | Педагогика | 15 страниц</small></div>
                                    <div class="col-xs-4 text-right"><strong>завершен</strong> 9 часов назад</div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 ratings">
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                    </div>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="img/cnt/photo-m1.png" alt="" class="img-circle img-responsive">
                                            </div>
                                            <div class="col-xs-9">
                                                <big>Андрей Киреев</big>
                                                <p><span class="rating"><span style="width:75%"></span></span></p>
                                                <p><span class="downloads1">45</span>
                                                    <span class="comments1">15</span>
                                                    <span class="like">54%</span></p><span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12"><a href="#" class="btn btn-default1">Запросить этого автора</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="block">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <span class="char-md">Д</span>
                            </div>
                            <div class="col-xs-10 col-md-11">
                                <div class="row">
                                    <div class="col-xs-8"><small># 53197554 | Дипломная работа | Педагогика | 15 страниц</small></div>
                                    <div class="col-xs-4 text-right"><strong>завершен</strong> 9 часов назад</div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 ratings">
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                        <p>Оценка заказчика <span class="rating"><span style="width:75%"></span></span></p>
                                        <em class="hiddentext">Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</em>
                                    </div>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="img/cnt/photo-m1.png" alt="" class="img-circle img-responsive">
                                            </div>
                                            <div class="col-xs-9">
                                                <big>Андрей Киреев</big>
                                                <p><span class="rating"><span style="width:75%"></span></span></p>
                                                <p><span class="downloads1">45</span>
                                                    <span class="comments1">15</span>
                                                    <span class="like">54%</span></p><span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12"><a href="#" class="btn btn-default1">Запросить этого автора</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <nav class="text-center">
                    <ul class="pagination pagination-lg">
                        <li class="disabled"><a href="#" aria-label="Previous"></a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#" aria-label="Next"></a></li>
                    </ul>
                </nav>
            </div>
            <aside class="col-xs-12 col-sm-5 col-md-3">
                <div class="block">
                    <h4>Узнать стоимость</h4>
                    <div class="row form-group">
                        <div class="col-xs-12"><input type="text" placeholder="Введите ваш E-mail" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12"><select class="form-control"><option>Все предметы</option></select></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12"><select class="form-control"><option>Тип работ</option></select></div>
                    </div>
                    <div class="row form-group">
                        <label class="control-label col-xs-12">Кол-во страниц</label>
                        <div class="col-xs-6"><select class="form-control"><option>5</option></select></div>
                        <div class="col-xs-6"><select class="form-control"><option>140</option></select></div>
                    </div>
                    <div class="row form-group">
                        <label class="control-label col-xs-12">Срок сдачи</label>
                        <div class="col-xs-12">
                            <input type="date" value="12.12.2016" class="form-control">
                            <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                        </div>
                    </div>
                    <p><big>Выбранные авторы:</big></p>
                    <div class="row chose">
                        <div class="col-xs-4">
                            <img src="img/cnt/photo-m1.png" alt="" class="img-circle img-responsive">
                        </div>
                        <div class="col-xs-6">
                            <p><big>Александр Волынец</big></p>
                            <small>Высшая математика</small>
                            <span class="rating"><span style="width:75%"></span></span>
                        </div>
                        <div class="col-xs-2"><a href="#" class="gray"><span class="glyphicon glyphicon-remove"></span></a></div>
                    </div>
                    <div class="row chose">
                        <div class="col-xs-4">
                            <img src="img/cnt/photo-m1.png" alt="" class="img-circle img-responsive">
                        </div>
                        <div class="col-xs-6">
                            <p><big>Александр Волынец</big></p>
                            <small>Высшая математика</small>
                            <span class="rating"><span style="width:75%"></span></span>
                        </div>
                        <div class="col-xs-2"><a href="#" class="gray"><span class="glyphicon glyphicon-remove"></span></a></div>
                    </div>
                    <div class="row"><div class="col-xs-12"><a href="#" class="btn btn-default">отправить запрос</a></div></div>
                </div>
            </aside>
        </div>
    </section>
@stop