@extends('main.layout')

@section('class-body', 'order')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                <div class="block blockicon">
                    <span class="gicon icon12"></span>
                    <h3 class="text-center regular">Ваш заказ успешно создан<br>и размещен в аукционе</h3>
                    <p>Через несколько минут вы начнете получать от исполнителей предложения с ценой. Посмотреть поступившие предложения можно на странице вашего заказа, или в своей почте.</p>
                    <p>WorkPrice - это онлайн биржа учебных работ, где каждый студент может найти автора для выполнения своего задания. В отличие от агентств вы будете работать с автором напрямую, без посредников, что существенно снизит стоимость и срок выполнения работы.</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3"><a href="#" class="btn btn-default btn-lg" data-toggle="modal" data-target="#order">перейти в заказ</a></div>
                    </div>
                </div>
            </div>
        </div>
        <h3 class="text-center regular">Как работает сервис?</h3>
        <p class="text-center">Это онлайн биржа учебных работ, где каждый студент может найти автора для выполнения своего задания.</p>
        <p>&nbsp;</p>
        <div class="row text-center">
            <div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1 nextto">
                <span class="gicon icon4"></span>
                <h4>Дождитесь<br>ставок от авторов</h4>
                <p>предложения начнут поступать через несколько минут</p>
            </div>
            <div class="col-xs-12 col-sm-4 nextto">
                <span class="gicon icon10"></span>
                <h4>Выберите<br>подходящего исполнителя</h4>
                <p>и он сразу же приступит к работе</p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 nextto">
                <span class="gicon icon11"></span>
                <h4>Получите<br>готовую работу</h4>
                <p>и сдайте её научному руководителю</p>
            </div>
        </div>
    </section>
    <div class="container-fluid text-center bottom-block">
        <h3 class="regular">А если вам всё понятно, то можете сразу разместить заказы<br>по другим типам работ и закрыть сессию разом:</h3>
        <div class="row chars">
            <div class="col-xs-6 col-sm-3 col-md-2">
                <span class="char">Д</span>
                <p><a href="#">Дипломная работа</a></p>
                <small><strong>от 3000 руб</strong></small>
                <small>от 3х дней</small>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2">
                <span class="char">К</span>
                <p><a href="#">Контрольная работа</a></p>
                <small><strong>от 3000 руб</strong></small>
                <small>от 3х дней</small>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2">
                <span class="char">К</span>
                <p><a href="#">Контрольная работа</a></p>
                <small><strong>от 3000 руб</strong></small>
                <small>от 3х дней</small>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2">
                <span class="char">К</span>
                <p><a href="#">Контрольная работа</a></p>
                <small><strong>от 3000 руб</strong></small>
                <small>от 3х дней</small>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <span class="char">К</span>
                <p><a href="#">Контрольная работа</a></p>
                <small><strong>от 3000 руб</strong></small>
                <small>от 3х дней</small>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <span class="char">К</span>
                <p><a href="#">Контрольная работа</a></p>
                <small><strong>от 3000 руб</strong></small>
                <small>от 3х дней</small>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-2 other">
                <p>А также другие<p>
                <p><a href="#" class="dashed blue"><span>студенческие работы</span></a></p>
            </div>
        </div>
    </div>

@stop