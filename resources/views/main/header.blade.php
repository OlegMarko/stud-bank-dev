<nav id="nav">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-md-3">
                <a href="#" class="logo"><img src="/img/logo.png" alt="" class="img-responsive"></a>
            </div>
            <div class="col-xs-8 col-md-9">
                <ul class="row text-center">
                    <li class="col-md-2 col-xs-offset-3 hidden-xs hidden-sm"><a href="#">Мои заказы</a></li>
                    <li class="col-md-3 hidden-xs hidden-sm"><a href="#">Аукцион заказов</a></li>
                    <li class="col-xs-5 col-xs-offset-1 col-sm-offset-0 col-sm-6 col-md-2"><a href="#" class="btn btn-default btn-sm"><span class="hidden-xs hidden-md">Сделать заказ</span><span class="hidden-sm hidden-lg">Заказать</span></a></li>
                    <li class="col-xs-6 col-sm-6 col-md-2 dropdown"><a href="#" class="bell"><span class="badge">14</span></a><a href="#" id="menu" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Меню<span class="glyphicon glyphicon-menu-hamburger"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="menu">
                            <li><a href="#">Мои заказы</a></li>
                            <li><a href="#">Топ авторов</a></li>
                            <li><a href="#">Готовые работы</a></li>
                            <li><a href="#">Заработай!</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>