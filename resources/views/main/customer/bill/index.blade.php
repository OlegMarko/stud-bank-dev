@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="nom">Мой счет &nbsp;&nbsp;&nbsp; <a href="#" class="small">Вернуться в кабинет</a></h1>
        <div class="row">
            <div class="col-xs-12">
                <div class="block bill">
                    <div class="row">
                        <div class="col-xs-3">
                            <p><big>На вашем счету</big></p>
                            <p class="price">1.490 руб.</p>
                        </div>
                        <div class="col-xs-3"><a href="#" class="btn btn-success btn-lg">Вывести средства</a></div>
                        <div class="col-xs-6 text-right"><big><a href="#" class="black">Вывести средства</a></big></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <select class="form-control">
                    <option>Все операции</option>
                </select>
            </div>
            <div class="col-xs-7 col-md-6 sort">
                Показать за &nbsp;&nbsp;
                <a href="#" class="dashed"><span>всё время</span></a> &nbsp;&nbsp;
                <a href="#" class="dashed"><span>неделя</span></a> &nbsp;&nbsp;
                <a href="#" class="dashed"><span>месяц</span></a>
            </div>
            <div class="col-xs-5 col-md-3">
                <input type="date" value="12.12.2016 - 24.12.2016" class="form-control">
                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="row hdr">
            <div class="col-xs-3 col-md-2">Референс</div>
            <div class="col-xs-5 col-md-3">Описание</div>
            <div class="col-xs-4 col-md-2 text-center"><a href="#" class="dashed black"><span>Дата</span> <small>&and;</small></a></div>
            <div class="col-xs-4 col-md-2 text-center"><a href="#" class="dashed black"><span>Приход, руб.</span> <small>&and;</small></a></div>
            <div class="col-xs-4 col-md-2 text-center"><a href="#" class="dashed black"><span>Расход, руб.</span> <small>&and;</small></a></div>
            <div class="col-xs-4 col-md-1 text-center"><a href="#" class="dashed black"><span>Статус</span> <small>&and;</small></a></div>
        </div>
        <div class="bdy">
            <div class="row">
                <div class="col-xs-3 col-md-2 ico1">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico2">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico3">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico1">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico2">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico3">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
        </div>
        <nav class="text-center">
            <ul class="pagination pagination-lg">
                <li><a href="#" aria-label="Previous"></a></li>
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#" aria-label="Next"></a></li>
            </ul>
        </nav>
    </section>
@stop