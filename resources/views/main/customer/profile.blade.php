@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class=""></li>
                <li data-target="#carousel" data-slide-to="1" class=""></li>
                <li data-target="#carousel" data-slide-to="2" class="active"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item">
                    <img src="img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item active">
                    <img src="img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="thumbnail autor-block">
                    <img src="img/cnt/photo-m.png" alt="" class="img-circle">
                    <div class="caption">
                        <h4 class="text-center">Андрей Киреев</h4>
                        <small class="text-center">kireev_1984</small>
                        <p class="location">г. Москва</p>
                        <p class="aphone">(8182) 65-01-33</p>
                        <p class="aid">1311231</p>
                        <p><a href="{{ URL::current() }}/options" class="gray">редактировать</a></p>
                    </div>
                </div>
            </div>
            <!--<div class="col-xs-12 col-sm-3">
                <div class="block stick">
                  <span class="gicon pro1">30<small>дней</small></span>
                <h4><strong>PRO</strong> аккаунт истекает через 30 дней</h4>
                <p>Повысьте интерес и доверие к лучших авторов к Вашему проекту</p>
                <a href="#">Продлить ПРО-аккаунт</a>
              </div>
            </div>-->
            <div class="col-xs-12 col-sm-3">
                <div class="block stick">
                    <span class="gicon pro">PRO</span>
                    <h4>Активируй <strong>PRO</strong> аккаунт</h4>
                    <p>Повысьте интерес и доверие к лучших авторов к Вашему проекту</p>
                    <a href="#">Узнать больше</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="block stick">
                    <span class="gicon icon5"></span>
                    <h4>Напоминание</h4>
                    <p>Укажите дату вашей следующей учебной работы и мы подарим Вам персональную скидку</p>
                    <a href="#" data-toggle="modal" data-target="#order">Заказать напоминание</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="block stick">
                    <span class="gicon icon6"></span>
                    <h4>Инструкции по работе</h4>
                    <p>Ознакомьтесь с инструкцией , чтобы Ваша работа в личном кабинете проходила быстрее и эффективнее.</p>
                    <a href="#">Cмотреть инструкцию</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="padding035">
                    <h4>Мои заказы:</h4>
                    <ul class="list">
                        <li>Всего: 0</li>
                        <li>В аукционе: 0</li>
                        <li>Ждет оплату: 0</li>
                        <li>В работе: 0</li>
                        <li>В гарантии: 0</li>
                        <li>Черновики: 0</li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-6">
                <a href="#" class="gray inline1">удалить всё <span class="glyphicon glyphicon-remove"></span></a>
                <h4 class="nomt">Уведомления (14): &nbsp; <span class="badge">5<small>новых</small></span></h4>
                <div class="block">
                    <ul class="notice">
                        <li>
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-6">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-6">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-6">
                                    <h6>Андрей Киреев <time>15.03.2016  18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <p class="text-center"><a href="#" class="gray dashed"><span>показать ещё 11</span></a></p>
                <h4>Активные заказы (4):</h4>
                <div class="search">
                    <ul>
                        <li class="block">
                            <span class="pull-right"><strong>в аукционе</strong></span>
                            <span class="char-md">Д<small>5</small></span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Дипломная работа по педагогике</a><br><small>Сдать до 18.02.2016 (осталось 5 дней)</small></p>
                            <footer class="clearfix">
                                <span class="pull-right">1500-2500р.</span>
                                <a href="#" class="gray">Смотреть все 30 ставок</a>
                            </footer>
                        </li>
                        <li class="block">
                            <span class="pull-right"><strong class="green">в работе</strong></span>
                            <span class="char-md">О<small class="green">5</small></span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Дипломная работа по педагогике</a><br><small>Сдать до 18.02.2016 (осталось 5 дней)</small></p>
                            <footer class="clearfix">
                                <span class="pull-right">1500-2500р.</span>
                                <span class="pull-left rtng">
              	<img src="img/cnt/photo-xs.png" alt="" class="img-circle pull-left">
								Анастастия Карлинская
              	<span class="rating"><span style="width:75%"></span></span>
              </span>
                            </footer>
                        </li>
                        <li class="block">
                            <span class="pull-right"><strong>в аукционе</strong></span>
                            <span class="char-md">К<small>5</small></span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Дипломная работа по педагогике</a><br><small>Сдать до 18.02.2016 (осталось 5 дней)</small></p>
                            <footer class="clearfix">
                                <span class="pull-right">1500-2500р.</span>
                                <a href="#" class="gray">Смотреть все 30 ставок</a>
                            </footer>
                        </li>
                        <li class="block">
                            <span class="pull-right"><strong class="gray">на гарантии</strong></span>
                            <span class="char-md">О</span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Дипломная работа по педагогике</a><br><small>Сдать до 18.02.2016 (осталось 5 дней)</small></p>
                            <footer class="clearfix">
                                <span class="pull-right">1500-2500р.</span>
                                <span class="pull-left rtng">
              	<img src="img/cnt/photo-xs.png" alt="" class="img-circle pull-left">
								Анастастия Карлинская
              	<span class="rating"><span style="width:75%"></span></span>
              </span>
                            </footer>
                        </li>
                    </ul>
                </div>
                <p class="text-center"><a href="#" class="gray dashed"><span>показать ещё 11</span></a></p>
                <p>&nbsp;</p>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="block best">
                    <p><strong>Лучшие авторы вашего заказа</strong></p>
                    <ul>
                        <li>
                            <div class="row">
                                <div class="col-xs-5">
                                    <img src="img/cnt/photo-s.png" alt="" class="img-circle img-responsive">
                                    <p class="rating1">4.5</p>
                                </div>
                                <div class="col-xs-7">
                                    <h4>Татьяна<br>Алексеева</h4>
                                    <small>Высшая математика</small>
                                    <a href="#">заказать у автора</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-5">
                                    <img src="img/cnt/photo-s.png" alt="" class="img-circle img-responsive">
                                    <p class="rating1">4.5</p>
                                </div>
                                <div class="col-xs-7">
                                    <h4>Татьяна<br>Алексеева</h4>
                                    <small>Высшая математика</small>
                                    <a href="#">заказать у автора</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-5">
                                    <img src="img/cnt/photo-s.png" alt="" class="img-circle img-responsive">
                                    <p class="rating1">4.5</p>
                                </div>
                                <div class="col-xs-7">
                                    <h4>Татьяна<br>Алексеева</h4>
                                    <small>Высшая математика</small>
                                    <a href="#">заказать у автора</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <p class="text-center"><a href="#" class="gray dashed"><span>показать все</span></a></p>
                <a href="#"><img src="img/cnt/banner2.jpg" alt="" class="img-responsive"></a>
            </div>
        </div>
    </section>
@stop