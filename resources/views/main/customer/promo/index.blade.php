@extends('main.layout')

@section('content')
    <div id="promo">
        <nav id="nav">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 col-md-3">
                        <a href="#" class="logo"><img src="/img/logo.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="col-xs-8 col-md-9">
                        <ul class="row text-center">
                            <li class="col-md-2 col-xs-offset-3 hidden-xs hidden-sm"><a href="#">Мои заказы</a></li>
                            <li class="col-md-3 hidden-xs hidden-sm"><a href="#">Аукцион заказов</a></li>
                            <li class="col-xs-5 col-xs-offset-1 col-sm-offset-0 col-sm-6 col-md-2"><a href="#" class="btn btn-default btn-sm"><span class="hidden-xs hidden-md">Сделать заказ</span><span class="hidden-sm hidden-lg">Заказать</span></a></li>
                            <li class="col-xs-6 col-sm-6 col-md-2 dropdown"><a href="#" class="bell"><span class="badge">14</span></a><a href="#" id="menu" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Меню<span class="glyphicon glyphicon-menu-hamburger"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="menu">
                                    <li><a href="#">Мои заказы</a></li>
                                    <li><a href="#">Топ авторов</a></li>
                                    <li><a href="#">Готовые работы</a></li>
                                    <li><a href="#">Заработай!</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container text-center">
            <p class="h1">Не хватает авторов, чтобы выполнять ваши заказы?</p>
            <h3 class="bold">Размещайте у нас заказы</h3>
            <p>У нас 10 000 исполнителей</p>
            <p>Аукционная система</p>
            <p>Гарантийный срок</p>
            <p>Режим работы 24 / 7</p>
            <div class="row"><div class="col-xs-12 col-md-4 col-md-offset-4"><a href="#" class="btn btn-success btn-lg">попробовать бесплатно</a></div></div>
            <div class="row">
                <div class="col-xs-4">
                    <strong>700</strong>
                    <p class="text-uppercase">агенств<br>уже работают<br>с нами</p>
                </div>
                <div class="col-xs-4">
                    <strong>45000</strong>
                    <p class="text-uppercase">авторов<br>готовы выполнить<br>ваш заказ</p>
                </div>
                <div class="col-xs-4">
                    <strong>2400</strong>
                    <p class="text-uppercase">заказов каждый день,<br>которые могут<br>быть ваши</p>
                </div>
            </div>
        </div>
    </div>
    <section id="section" class="container">
        <h1 class="text-center">Дополнительные преимущества</h1>
        <div class="row">
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">Персональный менеджер</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">Персональный менеджер</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">Персональный менеджер</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">Персональный менеджер</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. </p>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="text-center">
            <h2>Отзывы агентств</h2>
            <div id="reviews" class="row affiliate-reviews">
                <div class="images">
                    <img src="/img/cnt/photo-m1.png" alt="" class="img-circle opacity45">
                    <img src="/img/cnt/photo-m.png" alt="" class="img-circle">
                    <img src="/img/cnt/photo-m2.png" alt="" class="img-circle opacity45">
                    <img src="/img/cnt/photo-m.png" alt="" class="img-circle hidden">
                    <img src="/img/cnt/photo-m1.png" alt="" class="img-circle hidden">
                </div>
                <div class="review hidden">
                    <h4>Агенство "На 5 баллов"</h4>
                    <p>11.12.2016</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review">
                    <h4>Агенство "На 5 баллов"</h4>
                    <p>11.12.2016</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Агенство "На 5 баллов"</h4>
                    <p>11.12.2016</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Агенство "На 5 баллов"</h4>
                    <p>11.12.2016</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Агенство "На 5 баллов"</h4>
                    <p>11.12.2016</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <a href="#prev"></a>
                <a href="#next"></a>
            </div>
        </div>
        <p class="h1 text-center">Зарегистрируйтесь прямо сейчас<br>и получите бонусные деньги на первый заказ</p>
        <div class="row blocks-promo">
            <div class="col-sm-5 col-md-3 col-sm-offset-1 col-md-offset-3">
                <div class="block">
                    <h3>Размещайте<small>у нас заказы</small></h3>
                    <p>У нас 10 000 исполнителей</p>
                    <p>Аукционная система</p>
                    <p>Гарантийный срок</p>
                    <p>Режим работы 24 / 7</p>
                    <a href="#" class="btn btn-success btn-lg">попробовать бесплатно</a>
                </div>
                <p>Регистрация как заказчик работ</p>
            </div>
            <div class="col-sm-5 col-md-3">
                <div class="block">
                    <h3>Размещайте<small>у нас заказы</small></h3>
                    <p>От 1500 заказов в день</p>
                    <p>Работа с клиентами напрямую</p>
                    <p>Вы можете продать готовые работы</p>
                    <a href="#" class="btn btn-default btn-lg">зарегистрироваться</a>
                </div>
                <p>Регистрация как исполнитель работ</p>
            </div>
        </div>
    </section>
@stop