@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="text-center readmore"><a href="#">Вернуться в кабинет</a></p>
        <ul class="nav nav-tabs row text-center">
            <li role="presentation" class="col-xs-12 col-sm-6 active">
                <a data-toggle="pill" href="#edit">Редактирование профиля</a>
            </li>
            <li role="presentation" class="col-xs-12 col-sm-6">
                <a  data-toggle="pill" href="#subscribe">Настройка подписки</a>
            </li>
        </ul>
        <div class="row tab-content" >
            <div id="edit" class="tab-pane fade in active">
            <div class="col-xs-12 col-sm-4 col-md-3 profile">
                <h4>Моя фотография</h4>
                <p><a href="#" class="gray">удалить фото<span class="glyphicon glyphicon-remove"></span></a></p>
                <p><img src="/img/cnt/photo-m.png" alt="" class="img-circle"></p>
                <p><button class="btn btn-default btn-sm">Загрузить фото</button></p>
                <p><a href="#" class="dashed"><span>Сделать фото с веб камеры</span></a>
                <p><a href="#" class="dashed avatar"><span>Выбрать аватар</span></a></p>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9">
                <h4>Основные данные</h4>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Имя*</label>
                        <input type="text" value="Александр" class="form-control">
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Фамилия*</label>
                        <input type="text" value="Волынец" class="form-control">
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Логин*</label>
                        <input type="text" value="alex_vol" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="form-horizontal col-xs-12 col-md-11">
                        <div class="form-group">
                            <label class="control-label col-xs-3 col-sm-6 col-md-3 col-lg-2">Дата рождения</label>
                            <div class="col-xs-9 col-sm-6 col-md-5 col-lg-3">
                                <input type="date" value="12.12.2016" class="form-control">
                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <textarea placeholder="О себе" class="form-control"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-3">
                        <button class="btn btn-default btn-sm">сохранить</button>
                    </div>
                </div>
                <hr>
                <h4>Образование</h4>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-6">
                        <input type="text" placeholder="Название ВУЗа" class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row">
                            <div class="form-horizontal col-xs-7">
                                <div class="form-group">
                                    <label class="control-label col-xs-6">Год окончания</label>
                                    <div class="col-xs-6">
                                        <select class="form-control">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-5">
                                <select class="form-control">
                                    <option>Статус</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-6">
                        <select class="form-control">
                            <option>Факультет</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <select class="form-control">
                            <option>Специальность</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-horizontal col-xs-12 col-md-11">
                        <div class="form-group">
                            <label class="control-label col-xs-3 col-sm-6 col-md-3 col-lg-2">Начало сессии</label>
                            <div class="col-xs-9 col-sm-6 col-md-5 col-lg-3">
                                <input type="date" value="12.12.2016" class="form-control">
                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-3">
                        <button class="btn btn-default btn-sm">сохранить</button>
                    </div>
                </div>
                <hr>
                <h4>Контактные данные</h4>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-4">
                        <label>E-mail*</label>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="email" placeholder="mail@mail" class="form-control">
                                <span class="form-control-feedback"><a href="#" class="dashed"><span>подтвердить</span></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Страна</label>
                        <select class="form-control">
                            <option>Страна</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Город</label>
                        <select class="form-control">
                            <option>Город</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">Номер мобильного &nbsp; <a href="#" class="dashed">+ <span>добавить номер</span></a></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-3">
                        <button class="btn btn-default btn-sm">сохранить</button>
                    </div>
                </div>
                <hr>
                <h4>Изменить пароль</h4>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Введите старый пароль</label>
                        <input type="password" value="**********" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Введите новый пароль</label>
                        <input type="password" value="**********" class="form-control">
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label>Введите новый пароль еще раз</label>
                        <input type="password" value="**********" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-3">
                        <button class="btn btn-default btn-sm">сохранить</button>
                    </div>
                </div>
                <hr>
                <p><a href="#" class="gray">удалить аккаунт<span class="glyphicon glyphicon-remove"></span></a>
            </div>
            </div>

            <div id="subscribe" class="tab-pane fade">
                <div class="col-xs-12 col-sm-4 col-md-3 profile">
                    <h4>Моя фотография</h4>
                    <p><a href="#" class="gray">удалить фото<span class="glyphicon glyphicon-remove"></span></a></p>
                    <p><img src="/img/cnt/photo-m.png" alt="" class="img-circle"></p>
                    <p><button class="btn btn-default btn-sm">Загрузить фото</button></p>
                    <p><a href="#" class="dashed"><span>Сделать фото с веб камеры</span></a>
                    <p><a href="#" class="dashed avatar"><span>Выбрать аватар</span></a></p>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <h4>Настройка уведомлений</h4>
                    <div class="checkbox">
                        <label><input type="checkbox">Уведомления о комментариях к заказу</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox">Уведомления об изменения статуса заказа</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox">Уведомление о покупке работы</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox">SMS уведомления</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox">Советы</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox">Уведомления в браузер</label>
                    </div>
                    <p>&nbsp;</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-3">
                            <button class="btn btn-default btn-sm">сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            </div>
            </div>
        </div>
    </section>
@stop