@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="thumbnail autor-block">
                    <img src="/img/cnt/photo-m.png" alt="" class="img-circle">
                    <div class="caption">
                        <h4 class="text-center">Андрей Киреев</h4>
                        <small class="text-center">kireev_1984</small>
                        <p class="location">г. Москва</p>
                        <p class="aphone">(8182) 65-01-33</p>
                        <p class="aid">1311231</p>
                        <p><a href="#" class="gray">редактировать</a></p>
                    </div>
                </div>
                <div class="padding035">
                    <h4>Мои заказы:</h4>
                    <ul class="list">
                        <li>Всего: 0</li>
                        <li>В аукционе: 0</li>
                        <li>Ждет оплату: 0</li>
                        <li>В работе: 0</li>
                        <li>В гарантии: 0</li>
                        <li>Черновики: 0</li>
                    </ul>
                </div>
                <p>&nbsp;</p>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9">
                <p class="back">&lt; <a href="#">Вернуться в кабинет</a></p>
                <a href="#" class="gray inline">удалить всё <span class="glyphicon glyphicon-remove"></span></a>
                <h4>Уведомления (14): &nbsp; <span class="badge">5<small>новых</small></span></h4>
                <div class="block">
                    <ul class="notice">
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="padding15">
                    <ul class="notice">
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1 col-sm-2 col-md-1">
                                    <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8">
                                    <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                    <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                </div>
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@stop