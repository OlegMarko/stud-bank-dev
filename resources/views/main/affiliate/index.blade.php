@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="text-center">Партнерка</h1>
        <ul class="nav nav-tabs row text-center">
            <li role="presentation" class="col-xs-12 col-sm-4 active">
                <a data-toggle="pill" href="#about">О партнерской программе</a>
            </li>
            <li role="presentation" class="col-xs-12 col-sm-4">
                <a data-toggle="pill" href="#instruments">Инструменты</a>
            </li>
            <li role="presentation" class="col-xs-12 col-sm-4">
                <a data-toggle="pill" href="#statistics">Статистика</a>
            </li>
        </ul>











        <div class="tab-content">
            <div id="about" class="tab-pane fade in active">
        <div class="block affiliate1" id="about">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <p class="biggest">Пригласи друзей и заработай <strong>до 1500 руб.</strong> с одного заказа!</p>
                    <a href="#" class="btn btn-default btn-lg">Начать зарабатывать</a>
                </div>
                <div class="col-xs-12 col-md-8">
                    <h4>Топ лучших</h4>
                    <div class="row text-center">
                        <div class="col-xs-4">
                            <span>1</span>
                            <p><img src="img/cnt/photo-m.png" alt="" class="img-circle"></p>
                            Александр, 24 года
                            <strong>20 000 руб.</strong>
                        </div>
                        <div class="col-xs-4">
                            <span>2</span>
                            <p><img src="img/cnt/photo-m.png" alt="" class="img-circle"></p>
                            Александр, 24 года
                            <strong>20 000 руб.</strong>
                        </div>
                        <div class="col-xs-4">
                            <span>3</span>
                            <p><img src="img/cnt/photo-m.png" alt="" class="img-circle"></p>
                            Александр, 24 года
                            <strong>20 000 руб.</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="row text-center">
                    <div class="col-xs-4">
                        <span class="gicon icon4"></span>
                        <h4>В удобное время</h4>
                        <p>Когда и где вам удобно, нет графиков и сроков</p>
                    </div>
                    <div class="col-xs-4">
                        <span class="gicon icon0">0</span>
                        <h4>Без затрат</h4>
                        <p>Никаких вложений средств, только немного времени</p>
                    </div>
                    <div class="col-xs-4">
                        <span class="gicon icon3"></span>
                        <h4>Выгодно всем</h4>
                        <p>Вы помогаете друзьям и зарабатываете деньги</p>
                    </div>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p class="text-center">Партнерская программа HomeWork позволит Вам получать доход с заказов тех клиентов, которым Вы<br>рекомендовали нашу компанию. Сумма вознаграждения зависит от стоимости заказа вашего друга. </p>
        <hr>
        <p>&nbsp;</p>
        <div class="row track-header">
            <div class="col-xs-2">Бонус</div>
            <div class="col-xs-10">
                <div class="row text-center">
                    <div class="col-xs-2">300</div>
                    <div class="col-xs-2">500</div>
                    <div class="col-xs-2">700</div>
                    <div class="col-xs-2">1000</div>
                    <div class="col-xs-2">1300</div>
                    <div class="col-xs-2">1500</div>
                </div>
            </div>
        </div>
        <div class="row track">
            <div class="col-xs-2"></div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">Сумма заказа, руб.</div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-2">300</div>
                    <div class="col-xs-2">500</div>
                    <div class="col-xs-2">700</div>
                    <div class="col-xs-2">1000</div>
                    <div class="col-xs-2">1300</div>
                    <div class="col-xs-2">1500</div>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        <hr>
        <div class="text-center">
            <h2>Отзывы</h2>
            <div id="reviews" class="row affiliate-reviews">
                <div class="images">
                    <img src="img/cnt/photo-m1.png" alt="" class="img-circle opacity45">
                    <img src="img/cnt/photo-m.png" alt="" class="img-circle">
                    <img src="img/cnt/photo-m2.png" alt="" class="img-circle opacity45">
                    <img src="img/cnt/photo-m.png" alt="" class="img-circle hidden">
                    <img src="img/cnt/photo-m1.png" alt="" class="img-circle hidden">
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <a href="#prev"></a>
                <a href="#next"></a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <a href="#" class="btn btn-success btn-lg">Участвовать в программе</a>
            </div>
        </div>
            </div>


        <div id="instruments" class="tab-pane fade">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-5">
                    <h3 class="bold">SMS с кодом на скидку</h3>
                    <p><big>Указывайте номера мобильных телефонов ваших друзей. В сообщении им придет приглашение оформить заказ, а также ваш уникальный код на скидку, который будет соответствовать вашему ID.</big></p>
                    <form class="row">
                        <div class="form-group col-xs-8">
                            <input type="tel" class="form-control" placeholder="Телефон">
                        </div>
                        <div class="form-group col-xs-4">
                            <button type="submit" class="btn btn-default">отправить</button>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-5 col-lg-offset-2">
                    <h3 class="bold">Письмо с персональной ссылкой</h3>
                    <p><big>Указывайте адреса друзей. Отправьте друзьям персональную ссылку на адрес электронной почты</big></p>
                    <form class="row">
                        <div class="form-group col-xs-8">
                            <input type="tel" class="form-control" placeholder="Телефон">
                        </div>
                        <div class="form-group col-xs-4">
                            <button type="submit" class="btn btn-default">отправить</button>
                        </div>
                    </form>
                </div>
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-5">
                    <p><big>Размещайте персональную ссылку на своих страничках, студенческих форумах и на специализированных досках объявлений.</big></p>
                    <form class="row">
                        <div class="form-group col-xs-8">
                            <input type="text" class="form-control" value="https://www.homework.ru/?refId=195403">
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-5 col-lg-offset-2">
                    <p><big>Размещайте персональную ссылку в форумах, на которых вы общаетесь.</big></p>
                    <form class="row">
                        <div class="form-group col-xs-8">
                            <input type="text" class="form-control" value="https://www.homework.ru/?refId=195403">
                        </div>
                    </form>
                </div>
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="col-xs-12">
                <div class="row help-block block">
                    <div class="col-xs-6 col-sm-7 col-md-9">
                        <p><strong>Отслеживайте историю приглашений и выводите денежные средства</strong></p>
                    </div>
                    <div class="col-xs-6 col-sm-5 col-md-3">
                        <a href="#" class="btn btn-default">смотреть статистику</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="statistics" class="tab-pane fade">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="block affiliate">
                        <div class="row">
                            <div class="col-xs-4">
                                <strong>234</strong>
                                приглашено<br>друзей
                            </div>
                            <div class="col-xs-4">
                                <strong>0</strong>
                                регистраций<br>по приглашению
                            </div>
                            <div class="col-xs-4">
                                <strong>0</strong>
                                приглашено<br>друзей
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="block affiliate text-center">
                        <p>Пока вы ничего<br>не заработали</p>
                        <a href="#" class="btn btn-success btn-lg">пригласить друзей</a>
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-xs-6"><big>Приглашенный</big></div>
                <div class="col-xs-4"><big>Бонус</big></div>
                <div class="col-xs-2"><big><a href="#" class="black dashed"><span>Дата приглашения</span> &and;</a></big></div>
            </div>
        </div>

        </div>




    </section>
@stop