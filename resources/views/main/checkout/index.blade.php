@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="text-center">Форма заказа учебной работы</h1>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-2 icons">
                <span class="gicon icon17"></span>
                <h5>Короткие сроки</h5>
                <p><small>Сроки написания работы —<br>от трех часов.</small></p>
                <span class="gicon icon18"></span>
                <h5>Гарантия</h5>
                <p><small>Автор бесплатно будет вносить корректировки по выполненному заказу</small></p>
                <span class="gicon icon19"></span>
                <h5>Минимальная стоимость</h5>
                <p><small>от 15 руб. за страницу</small></p>
                <p>&nbsp;</p>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-8 checkout">
                <ul class="nav nav-tabs row text-center" role="tablist">
                    <li role="presentation" class="col-xs-12 col-sm-6 active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Основное</a></li>
                    <li role="presentation" class="col-xs-12 col-sm-6"><a href="#tab2" aria-controls="tab1" role="tab" data-toggle="tab">Технические характеристики</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <h4 class="text-center regular">Узнай стоимость написания твоей работы</h4>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1">
                                <div class="row">
                                    <div class="col-xs-12 form-group"><input type="text" placeholder="Напишите тему вашей работы*" class="form-control"></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-6">
                                        <select class="form-control">
                                            <option>Выберите тип работы*</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6">
                                        <select class="form-control">
                                            <option>Выберите предмет*</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 form-group"><textarea placeholder="Добавить комментарий к заявке (необязательно)" class="form-control"></textarea></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-6"><a href="#" id="file1" class="dashed black"><span>Прикрепить файл</span> (макс 15 Мб)</a></div>
                                    <div class="col-xs-6 required">* - обязательно к запонению</div>
                                </div>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1">
                                <div class="row actions1">
                                    <div class="col-xs-9">
                                        <a href="#">В черновик</a> | <a href="#">Удалить</a>
                                    </div>
                                    <div class="col-xs-3"><a href="#" class="btn btn-default btn-corn">Далее</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab2">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-horizontal">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-md-4 col-md-offset-1">Кол-во страниц*</label>
                                            <div class="col-xs-12 col-md-7">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <select class="form-control">
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <select class="form-control">
                                                            <option>30</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-md-4 col-md-offset-1">Срок сдачи</label>
                                            <div class="col-xs-12 col-md-7">
                                                <input type="date" value="12.12.2016" class="form-control">
                                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-sm-offset-1">
                                <p>ВУЗ: не выбран</p>
                                <p>Шрифт: 14</p>
                                <p>Оригинальность: 50%</p>
                                <p><a href="#">Изменить</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-10 col-md-offset-1">
                                        <p>Гарантийный срок (в днях)</p>
                                        <div class="period">
                                            <span class="active">10</span>
                                            <span class="current">20</span>
                                            <span>30</span>
                                            <span>40</span>
                                            <span>50</span>
                                            <div style="width:25%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12 col-md-11 col-md-offset-1">
                                        <div class="checkbox">
                                            <label><input type="checkbox">Только для PRO-авторов <strong>0 руб.</strong> &nbsp; <a href="#" class="qwe">?</a></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox">Визуальное выделение заказа <strong>75 руб.</strong></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox">Закрепить заказ наверху ленты на</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-6 col-sm-7 form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-md-4 col-md-offset-1"><strong>Бюджет (в руб.)</strong></label>
                                    <div class="col-xs-12 col-md-7">
                                        <input type="text" class="form-control green">
                                    </div>
                                    <p class="col-xs-11 col-md-offset-1 alert-dismissible">Оставьте поле пустым, если бюджет по договоренности</p>
                                </div>
                            </div>
                            <div class="col-xs-5 col-sm-4">
                                <p class="hidden-md hidden-lg">&nbsp;</p>
                                <div class="total">
                                    <p>Рекомендованная цена от:</p>
                                    <strong>1.490 руб.</strong> &nbsp; <a href="#" class="qwe">?</a>
                                </div>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="row">
                            <div class="control-label col-xs-8 actions1">
                                <div class="row">
                                    <div class="col-xs-12 col-md-offset-1">
                                        <a href="#">В черновик</a> | <a href="#">Удалить</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3"><a href="#" class="btn btn-default btn-corn">Далее</a></div>
                        </div>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-10 col-lg-2 statuses">
                <h4 class="regular">Статус заказа</h4>
                <p class="active">Редактирование <span>1</span></p>
                <p>В аукционе<span>2</span></p>
                <p>В работе<span>3</span></p>
                <p>На гарантии<span>4</span></p>
                <p>Завершен<span>5</span></p>
            </div>
        </div>
    </section>
@stop