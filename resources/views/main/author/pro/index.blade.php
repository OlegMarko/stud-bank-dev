@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="nom text-center">Получи профеcсиональный аккаунт автора<br>и зарабатывай больше!</h1>
        <p class="text-center">С PRO аккаунтом ты увеличишь свой доход за счет повышения рейтинга на 20% и доступа к премиум проектам.</p>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="block prices">
                    <strong class="green">1 неделя</strong>
                    <span class="price">1099 руб.</span>
                    <p>экономия<br><big>10%</big></p>
                    <a href="#" class="btn btn-success">активировать</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="block prices">
                    <strong class="blue">1 неделя</strong>
                    <span class="price">1099 руб.</span>
                    <p>экономия<br><big>20%</big></p>
                    <a href="#" class="btn btn-info">активировать</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="block prices">
                    <strong class="orange">3 месяца</strong>
                    <span class="price">1099 руб.</span>
                    <p>экономия<br><big>30%</big></p>
                    <a href="#" class="btn btn-default">активировать</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="block prices">
                    <strong class="red">6 месяцев</strong>
                    <span class="price">1099 руб.</span>
                    <p>экономия<br><big>40%</big></p>
                    <a href="#" class="btn btn-danger">активировать</a>
                </div>
            </div>
        </div>
        <hr>
        <h2 class="text-center">Преимущества<br>PRO аккаунта для автора</h2>
        <div class="row">
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body">
                    <h4 class="media-heading">Метка <mark>PRO</mark> на иконке пользователя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body">
                    <h4 class="media-heading">Метка PRO на иконке пользователя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body">
                    <h4 class="media-heading">Метка PRO на иконке пользователя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body">
                    <h4 class="media-heading">Метка PRO на иконке пользователя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body">
                    <h4 class="media-heading">Метка PRO на иконке пользователя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body">
                    <h4 class="media-heading">Метка PRO на иконке пользователя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
                </div>
            </div>
        </div>
        <h2 class="text-center">Способы получения<br>PRO аккаунта <span class="green">бесплатно</span></h2>
        <div class="row">
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">За получения звания исполнителя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 media">
                <div class="media-left"><img src="/img/cnt/img1.png" alt="" class="media-object"></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">За получения звания исполнителя</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. </p>
                </div>
            </div>
        </div>
        <div class="block padding35 text-center">
            <h3 class="regular">Звания для автора</h3>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <div class="row">
                        <div class="col-xs-4"><span class="label label-default">абитуриент</span></div>
                        <div class="col-xs-4"><span class="label label-default1">бакалавр</span></div>
                        <div class="col-xs-4"><span class="label label-default2">магистр</span></div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="row">
                        <div class="col-xs-4"><span class="label label-warning label-s1">аспирант</span></div>
                        <div class="col-xs-4"><span class="label label-warning label-s2">кандидат</span></div>
                        <div class="col-xs-4"><span class="label label-warning label-s3">доктор</span></div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="row">
                        <div class="col-xs-12"><span class="label label-success label-bs">Академик</span></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop