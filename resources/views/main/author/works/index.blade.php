@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="text-center nom">Готовые работы</h1>
        <div class="block filter">
            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <input type="text" placeholder="Введите тему работы" class="form-control">
                </div>
                <div class="col-xs-12 col-md-4">
                    <select class="form-control">
                        <option>Выберите предмет</option>
                    </select>
                </div>
                <div class="col-xs-12 col-md-3">
                    <select class="form-control">
                        <option>Все типы работ</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4 form-horizontal">
                    <div class="form-group form-group-sm">
                        <label class="control-label col-xs-6">Количество страниц</label>
                        <div class="col-xs-3"><input type="text" value="10" class="form-control"></div>
                        <div class="col-xs-3 limit"><input type="text" value="15" class="form-control"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <input id="slider-line" type="text" value="" data-slider-min="0" data-slider-max="50" data-slider-step="1" data-slider-value="[10,40]">
                </div>
                <div class="col-xs-12 col-md-2">
                    <button class="btn btn-default">Найти</button>
                </div>
            </div>
            <p class="text-center"><small><a href="#" class="dashed gray"><small>&or;</small> <span>дополнительно</span> <small>&or;</small></a></small></p>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="block">
                    <h4 class="text-center">Заказы</h4>
                    <ul class="nav">
                        <li role="presentation"><a href="#">Поиск готовых работ</a></li>
                        <li role="presentation"><a href="#">Я продаю работы <span class="badge">0</span></a></li>
                        <li role="presentation"><a href="#">Я купил работы <span class="badge">0</span></a></li>
                    </ul>
                    <a href="#" class="btn btn-primary">Продать работу</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="row hdr1">
                    <div class="col-xs-6 col-md-8">Тема, предмет, тип работы</div>
                    <div class="col-xs-6 col-md-1 text-right"><a href="#" class="gray dashed"><span>Год</span> &and;</a></div>
                    <div class="col-xs-6 col-md-1 text-right"><a href="#" class="gray dashed"><span>Страниц</span> &and;</a></div>
                    <div class="col-xs-6 col-md-2 text-center"><a href="#" class="gray dashed"><span>Стоимость</span> &and;</a></div>
                </div>
                <ul class="result">
                    <li>
                        <div>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д</span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small>Дипломная работа | Педагогика</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-md-8">
                                            <p class="title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            <img src="/img/cnt/photo-xxs.png" alt="" class="img-circle"> &nbsp; Андрей Киреев
                                        </div>
                                        <div class="col-xs-2 col-md-1 text-center">18.02.2016</div>
                                        <div class="col-xs-1 text-center">13</div>
                                        <div class="col-xs-3 col-md-2 text-center">
                                            <p><strong>150 руб.</strong></p>
                                            <a href="#" class="btn btn-default1 btn-sm1">купить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д</span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small>Дипломная работа | Педагогика</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-md-8">
                                            <p class="title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            <img src="/img/cnt/photo-xxs.png" alt="" class="img-circle"> &nbsp; Андрей Киреев
                                        </div>
                                        <div class="col-xs-2 col-md-1 text-center">18.02.2016</div>
                                        <div class="col-xs-1 text-center">13</div>
                                        <div class="col-xs-3 col-md-2 text-center">
                                            <p><strong>150 руб.</strong></p>
                                            <a href="#" class="btn btn-default1 btn-sm1">купить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д</span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small>Дипломная работа | Педагогика</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-md-8">
                                            <p class="title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            <img src="/img/cnt/photo-xxs.png" alt="" class="img-circle"> &nbsp; Андрей Киреев
                                        </div>
                                        <div class="col-xs-2 col-md-1 text-center">18.02.2016</div>
                                        <div class="col-xs-1 text-center">13</div>
                                        <div class="col-xs-3 col-md-2 text-center">
                                            <p><strong>150 руб.</strong></p>
                                            <a href="#" class="btn btn-default1 btn-sm1">купить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д</span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small>Дипломная работа | Педагогика</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-md-8">
                                            <p class="title"><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            <img src="/img/cnt/photo-xxs.png" alt="" class="img-circle"> &nbsp; Андрей Киреев
                                        </div>
                                        <div class="col-xs-2 col-md-1 text-center">18.02.2016</div>
                                        <div class="col-xs-1 text-center">13</div>
                                        <div class="col-xs-3 col-md-2 text-center">
                                            <p><strong>150 руб.</strong></p>
                                            <a href="#" class="btn btn-default1 btn-sm1">купить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <nav class="text-center">
                    <ul class="pagination pagination-lg">
                        <li class="disabled"><a href="#" aria-label="Previous"></a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#" aria-label="Next"></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
@stop