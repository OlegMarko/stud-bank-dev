@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="nom">Мой счет &nbsp;&nbsp;&nbsp; <a href="#" class="small">Вернуться в кабинет</a></h1>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="block bill">
                    <div class="row">
                        <div class="col-xs-6 col-lg-7">
                            <p><big>Доступный баланс</big></p>
                            <p class="price">1.490 руб.</p>
                        </div>
                        <div class="col-xs-6 col-lg-5"><a href="#" class="btn btn-success btn-sm">Вывести средства</a></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="block bill">
                    <div class="row">
                        <div class="col-xs-6">
                            <p><big>Заморожено средств</big></p>
                            <p class="price">500 руб.</p>
                        </div>
                        <div class="col-xs-6">Замороженные средства поступят к вам на счет после завершения гарантийного срока заказа</div>
                    </div>
                </div>
            </div>
        </div>
        <ul class="nav nav-tabs row text-center">
            <li role="presentation" class="col-xs-12 col-sm-4"><a href="#">Заявки на вывод средств</a></li>
            <li role="presentation" class="col-xs-12 col-sm-4 active"><a href="#">История изменений</a></li>
            <li role="presentation" class="col-xs-12 col-sm-4"><a href="#">Замороженные средства</a></li>
        </ul>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <select class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                    <option>Все операции</option>
                </select>
                <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 262px;">
                    <span class="selection">
                        <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-dk47-container">
                            <span class="select2-selection__rendered" id="select2-dk47-container" title="Все операции">Все операции</span>
                            <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>
                        </span>
                    </span>
                    <span class="dropdown-wrapper" aria-hidden="true"></span>
                </span>
            </div>
            <div class="col-xs-7 col-md-6 sort">
                Показать за &nbsp;&nbsp;
                <a href="#" class="dashed"><span>всё время</span></a> &nbsp;&nbsp;
                <a href="#" class="dashed"><span>неделя</span></a> &nbsp;&nbsp;
                <a href="#" class="dashed"><span>месяц</span></a>
            </div>
            <div class="col-xs-5 col-md-3">
                <input type="date" value="12.12.2016 - 24.12.2016" class="form-control">
                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="row hdr">
            <div class="col-xs-3 col-md-2">Референс</div>
            <div class="col-xs-5 col-md-3">Описание</div>
            <div class="col-xs-4 col-md-2 text-center"><a href="#" class="dashed black"><span>Дата</span> <small>∧</small></a></div>
            <div class="col-xs-4 col-md-2 text-center"><a href="#" class="dashed black"><span>Приход, руб.</span> <small>∧</small></a></div>
            <div class="col-xs-4 col-md-2 text-center"><a href="#" class="dashed black"><span>Расход, руб.</span> <small>∧</small></a></div>
            <div class="col-xs-4 col-md-1 text-center"><a href="#" class="dashed black"><span>Статус</span> <small>∧</small></a></div>
        </div>
        <div class="bdy">
            <div class="row">
                <div class="col-xs-3 col-md-2 ico1">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico2">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico3">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico1">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico2">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2 ico3">29693926</div>
                <div class="col-xs-5 col-md-3">Вы пополнили счет на 300 руб.</div>
                <div class="col-xs-4 col-md-2 text-center">12.12.2016 15:38</div>
                <div class="col-xs-4 col-md-2 text-center">300</div>
                <div class="col-xs-4 col-md-2 text-center">&nbsp;</div>
                <div class="col-xs-4 col-md-1 text-center">Выполнено</div>
            </div>
        </div>
        <nav class="text-center">
            <ul class="pagination pagination-lg">
                <li><a href="#" aria-label="Previous"></a></li>
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#" aria-label="Next"></a></li>
            </ul>
        </nav>
    </section>
@stop