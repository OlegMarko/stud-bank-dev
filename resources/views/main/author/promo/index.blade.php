@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption hidden-md hidden-lg">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="hidden-sm">Этот сайт — биржа студенческих работ</p>
                            <p class="hidden-xs">Наш сервис позволяет заказчикам и авторам взаимодействовать без посредников и выполнять работы в режиме online.<br><strong>Начните зарабатывать прямо сейчас!</strong></p>
                            <a href="#" class="btn btn-primary">Зарегистрироваться</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption hidden-md hidden-lg">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="hidden-sm">Этот сайт — биржа студенческих работ</p>
                            <p class="hidden-xs">Наш сервис позволяет заказчикам и авторам взаимодействовать без посредников и выполнять работы в режиме online.<br><strong>Начните зарабатывать прямо сейчас!</strong></p>
                            <a href="#" class="btn btn-primary">Зарегистрироваться</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption hidden-md hidden-lg">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="hidden-sm">Этот сайт — биржа студенческих работ</p>
                            <p class="hidden-xs">Наш сервис позволяет заказчикам и авторам взаимодействовать без посредников и выполнять работы в режиме online.<br><strong>Начните зарабатывать прямо сейчас!</strong></p>
                            <a href="#" class="btn btn-primary">Зарегистрироваться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block text-center hidden-xs hidden-sm">
                <span class="icow"></span>
                <h3 class="regular">Этот сайт — биржа студенческих работ</h3>
                <p>Наш сервис позволяет заказчикам и авторам взаимодействовать без посредников и выполнять работы в режиме online.<br><strong>Начните зарабатывать прямо сейчас!</strong></p>
                <a href="#" class="btn btn-default btn-lg">Зарегистрироваться</a>
            </div>
        </div>
        <div class="row statistics">
            <div class="col-xs-6 col-md-4">
                <strong>&gt; 200 000</strong>
                заказчиков
            </div>
            <div class="col-xs-6 col-md-3">
                <strong>&gt; 3000</strong>
                заказов ежедневно
            </div>
        </div>
        <h3 class="text-center regular">Как начать работать автором?</h3>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <div class="row text-center">
                    <div class="col-xs-12 col-sm-4 nextto">
                        <span class="gicon icon13"></span>
                        <p class="register"><a href="#">Зарегистрируйтесь</a></p>
                        <p>предложения начнут поступать через несколько минут</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 nextto">
                        <span class="gicon icon15"></span>
                        <h4 class="regular">Выполняйте заказы</h4>
                        <p>и он сразу же приступит к работе</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 nextto">
                        <span class="gicon icon14"></span>
                        <h4 class="regular">Зарабатывайте деньги</h4>
                        <p>и сдайте её научному руководителю</p>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h3 class="text-center regular">Возможности</h3>
        <p>&nbsp;</p>
        <div class="row text-center iconsp">
            <div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1">
                <span class="iconp icon1"></span>
                <h4 class="text-uppercase bold">Короткие сроки</h4>
                <p>Сроки написания работы — от трех часов.</p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <span class="iconp icon2"></span>
                <h4 class="text-uppercase bold">Гарантия</h4>
                <p>Автор бесплатно будет вносить корректировки по выполненному заказу</p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <span class="iconp icon16"></span>
                <h4 class="text-uppercase bold">Минимальная стоимость</h4>
                <p>от 15 руб. за страницу</p>
            </div>
        </div>
        <div class="row text-center iconsp">
            <div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1">
                <span class="iconp icon1"></span>
                <h4 class="text-uppercase bold">Короткие сроки</h4>
                <p>Сроки написания работы — от трех часов.</p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <span class="iconp icon2"></span>
                <h4 class="text-uppercase bold">Гарантия</h4>
                <p>Автор бесплатно будет вносить корректировки по выполненному заказу</p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <span class="iconp icon16"></span>
                <h4 class="text-uppercase bold">Минимальная стоимость</h4>
                <p>от 15 руб. за страницу</p>
            </div>
        </div>
        <hr>
        <div class="text-center">
            <h2>Отзывы</h2>
            <div id="reviews" class="row affiliate-reviews">
                <div class="images">
                    <img src="/img/cnt/photo-m1.png" alt="" class="img-circle opacity45">
                    <img src="/img/cnt/photo-m.png" alt="" class="img-circle">
                    <img src="/img/cnt/photo-m2.png" alt="" class="img-circle opacity45">
                    <img src="/img/cnt/photo-m.png" alt="" class="img-circle hidden">
                    <img src="/img/cnt/photo-m1.png" alt="" class="img-circle hidden">
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <div class="review hidden">
                    <h4>Кристина Куприянова</h4>
                    <p>4 мес. на сайте</p>
                    <q>The underrated simplicity of having a time for solace and stillness is afforded as a freelance writer and this type of intangible economics is splendidly rewarding, for me personally. <br><br>Additionally, it just so happens that this relished quiet time happens to be my favorite span of the day where creative stimuli just seems to emanate so easily in my writing. This combination of not having to battle to commute in traffic, while being able to do what I absolutely love in writing, while concurrently allowing this needed creative outlet and refuge has left me at awe at how I used to do things, professionally. Everyone wants to find that place where they love their work, while having time for family.</q>
                </div>
                <a href="#prev"></a>
                <a href="#next"></a>
            </div>
        </div>
        <div class="jumbotron row text-center">
            <span class="icow"></span>
            <p class="h1">Зарегистрируйтесь как автор!</p>
            <p>Регистрация простая и бесплатная</p>
            <p><a class="btn btn-default btn-lg" href="#" role="button">Стать внештатным писателем</a></p>
        </div>
    </section>
@stop