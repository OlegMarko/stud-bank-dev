@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="thumbnail autor-block">
                    <img src="/img/cnt/photo-m.png" alt="" class="img-circle">
                    <div class="caption">
                        <h4 class="text-center">Андрей Киреев</h4>
                        <small class="text-center">kireev_1984</small>
                        <p class="location">г. Москва</p>
                        <p class="aphone">(8182) 65-01-33</p>
                        <p class="aid">1311231</p>
                        <p><a href="#" class="gray">редактировать</a></p>
                    </div>
                </div>
                <div class="block rating-block clearfix">
                    <span class="label label-success">бакалавр</span>
                    <p class="text-center">Рейтинг: 75</p>
                    <span class="rating"><span style="width:75%;"></span></span>
                    <p class="reviews"><small>Отзывы</small><a href="#" class="good">12</a> | <a href="#">3</a> | <a href="#" class="bad">0</a></p>
                    <p class="votes"><small>Успех</small><strong>100%</strong></p>
                </div>
                <h4>Типы работ:</h4>
                <ul class="list">
                    <li><a href="#">Дипломная работа</a></li>
                    <li><a href="#">Курсовая работа</a></li>
                    <li><a href="#">Реферат</a></li>
                    <li><a href="#">Магистерская диссертация</a></li>
                    <li><a href="#">Отчет по практике</a></li>
                    <li><a href="#">Статья</a></li>
                </ul>
                <p><a href="#" class="gray dashed"><span>показать все (17)</span></a></p>
                <p>&nbsp;</p>
                <h4>Предметы:</h4>
                <ul class="list">
                    <li><a href="#">Анализ хозяйственной деятельности</a></li>
                    <li><a href="#">Антикризисное управление</a></li>
                    <li><a href="#">Банковское дело</a></li>
                    <li><a href="#">Бизнес-планирование</a></li>
                    <li><a href="#">Бухгалтерский учет и аудит</a></li>
                </ul>
                <p><a href="#" class="gray dashed"><span>показать все (63)</span></a></p><span></span>
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group">
                    <input type="search" value="" placeholder="Поиск заказов" class="form-control">
                    <button class="glyphicon glyphicon-search form-control-feedback"></button>
                </div>
                <ul class="nav nav-tabs nav-sm row text-center">
                    <li role="presentation" class="col-xs-6 col-md-4 col-lg-2 active"><a href="#">Мои ставки (0)</a></li>
                    <li role="presentation" class="col-xs-6 col-md-4 col-lg-2"><a href="#">В работе (1)</a></li>
                    <li role="presentation" class="col-xs-6 col-md-4 col-lg-2"><a href="#">На гарантии (2)</a></li>
                    <li role="presentation" class="col-xs-6 col-md-4 col-lg-2"><a href="#">Завершенные (0)</a></li>
                    <li role="presentation" class="col-xs-6 col-md-4 col-lg-2"><a href="#">Избранные (5)</a></li>
                    <li role="presentation" class="col-xs-6 col-md-4 col-lg-2"><a href="#">Отмененные (15)</a></li>
                </ul>
                <div class="search">
                    <ul>
                        <li class="block">
            <span class="pull-right">
            	<strong>в аукционе</strong>
              Сдать до 18.02.2016 (осталось 5 дней)
            </span>
                            <span class="char-md">Д<small>5</small></span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                            <footer class="clearfix">
              <span class="pull-right">
                Смотреть все 30 ставок
                <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
              </span>
                                <a href="#" class="gray">Смотреть все 30 ставок</a> 1500-2500р.
                            </footer>
                        </li>
                        <li class="block">
            <span class="pull-right">
            	<strong>в аукционе</strong>
              Сдать до 18.02.2016 (осталось 5 дней)
            </span>
                            <span class="char-md">Д<small>5</small></span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                            <footer class="clearfix">
              <span class="pull-right">
                Смотреть все 30 ставок
                <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
              </span>
                                <a href="#" class="gray">Смотреть все 30 ставок</a> 1500-2500р.
                            </footer>
                        </li>
                        <li class="block">
            <span class="pull-right">
            	<strong>в аукционе</strong>
              Сдать до 18.02.2016 (осталось 5 дней)
            </span>
                            <span class="char-md">Д<small>5</small></span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                            <footer class="clearfix">
              <span class="pull-right">
                Смотреть все 30 ставок
                <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
              </span>
                                <a href="#" class="gray">Смотреть все 30 ставок</a> 1500-2500р.
                            </footer>
                        </li>
                        <li class="block">
            <span class="pull-right">
            	<strong>в аукционе</strong>
              Сдать до 18.02.2016 (осталось 5 дней)
            </span>
                            <span class="char-md">Д<small>5</small></span>
                            <small># 53197554 | Дипломная работа | Менеджмент</small>
                            <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                            <footer class="clearfix">
              <span class="pull-right">
                Смотреть все 30 ставок
                <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
              </span>
                                <a href="#" class="gray">Смотреть все 30 ставок</a> 1500-2500р.
                            </footer>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

@stop