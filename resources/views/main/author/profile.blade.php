@extends('main.layout')

@section('content')
<nav id="nav">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-md-3">
                <a href="#" class="logo"><img src="img/logo.png" alt="" class="img-responsive"></a>
            </div>
            <div class="col-xs-8 col-md-9">
                <ul class="row text-center">
                    <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Мои заказы</a></li>
                    <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Топ авторов</a></li>
                    <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Готовые работы</a></li>
                    <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Заработай!</a></li>
                    <li class="col-xs-5 col-xs-offset-1 col-sm-offset-0 col-sm-6 col-md-2"><a href="#" class="btn btn-default btn-sm"><span class="hidden-xs hidden-md">Сделать заказ</span><span class="hidden-sm hidden-lg">Заказать</span></a></li>
                    <li class="col-xs-6 col-sm-6 col-md-2 dropdown"><a href="#" class="bell"><span class="badge">14</span></a><a href="#" id="menu" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Меню<span class="glyphicon glyphicon-menu-hamburger"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="menu">
                            <li><a href="#">Мои заказы</a></li>
                            <li><a href="#">Топ авторов</a></li>
                            <li><a href="#">Готовые работы</a></li>
                            <li><a href="#">Заработай!</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<section id="section" class="container">

    @include('widgets.banners.cabinet',['banners'=>\App\Models\Main\Banner::getListByRole(\App\Models\Main\Banner::AUTHOR_CABINET)])

    <div class="row">
        <div class="col-xs-12 col-md-9">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="thumbnail autor-block">
                        <img src="img/cnt/photo-m.png" alt="" class="img-circle">
                        <div class="caption">
                            <h4 class="text-center">{{ Auth::user()->getFullName() }}</h4>
                            <small class="text-center">{{ Auth::user()->login }}</small>
                            <p class="location">{{ Auth::user()->getUserTown() }}</p>
                            <p class="aphone">(8182) 65-01-33</p>
                            <p class="aid">{{ Auth::user()->id }}</p>
                            <p><a href="{{ URL::current() }}/options" class="gray">редактировать</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <a href="#" class="gray inline">удалить всё <span class="glyphicon glyphicon-remove"></span></a>
                    <h4>Уведомления (14): &nbsp; <span class="badge">5<small>новых</small></span></h4>
                    <div class="block">
                        <ul class="notice">
                            <li>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <img src="img/cnt/photo-xs.png" alt="" class="img-circle">
                                    </div>
                                    <div class="col-xs-6">
                                        <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                        <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                    </div>
                                    <div class="col-xs-1">
                                        <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <img src="img/cnt/photo-xs.png" alt="" class="img-circle">
                                    </div>
                                    <div class="col-xs-6">
                                        <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                        <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                    </div>
                                    <div class="col-xs-1">
                                        <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <img src="img/cnt/photo-xs.png" alt="" class="img-circle">
                                    </div>
                                    <div class="col-xs-6">
                                        <h6>Андрей Киреев <time>15.03.2016  18:00</time></h6>
                                        <p>Заказчик внес изменения в проект "Гидрогазодинамика, задачи"</p>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="#" class="btn btn-default btn-sm">в заказ</a>
                                    </div>
                                    <div class="col-xs-1">
                                        <a href="#" class="gray glyphicon glyphicon-remove"></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <p class="text-center"><a href="#" class="gray dashed"><span>показать ещё 11</span></a></p><span></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="block rating-block clearfix">
                        <span class="label label-success">бакалавр</span>
                        <p class="text-center">Рейтинг: 75</p>
                        <span class="rating"><span style="width:75%;"></span></span>
                        <p class="reviews"><small>Отзывы</small><a href="#" class="good">12</a> | <a href="#">3</a> | <a href="#" class="bad">0</a></p>
                        <p class="votes"><small>Успех</small><strong>100%</strong></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="block indicators">
                        <div class="row">
                            <div class="col-xs-4">
                                <h6>Новых заказов</h6>
                                <span>2445</span>
                            </div>
                            <div class="col-xs-4">
                                <h6>Заказов в работе</h6>
                                <span>2</span>
                            </div>
                            <div class="col-xs-4">
                                <h6>Выполненных</h6>
                                <span>28</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <h4>Типы работ:</h4>
                    <ul class="list">
                        <li><a href="#">Дипломная работа</a></li>
                        <li><a href="#">Курсовая работа</a></li>
                        <li><a href="#">Реферат</a></li>
                        <li><a href="#">Магистерская диссертация</a></li>
                        <li><a href="#">Отчет по практике</a></li>
                        <li><a href="#">Статья</a></li>
                    </ul>
                    <p><a href="#" class="gray dashed"><span>показать все (17)</span></a></p>
                    <p>&nbsp;</p>
                    <h4>Предметы:</h4>
                    <ul class="list">
                        <li><a href="#">Анализ хозяйственной деятельности</a></li>
                        <li><a href="#">Антикризисное управление</a></li>
                        <li><a href="#">Банковское дело</a></li>
                        <li><a href="#">Бизнес-планирование</a></li>
                        <li><a href="#">Бухгалтерский учет и аудит</a></li>
                    </ul>
                    <p><a href="#" class="gray dashed"><span>показать все (63)</span></a></p><span></span>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="row autor-block">
                        <div class="col-xs-4">
                            <p>Мои ставки: 0</p>
                            <p>Мои заказы: 0</p>
                            <p>Ждем оплату: 0</p>
                            <p>В работе: 0</p>
                            <p>На гарантии: 0</p>
                            <p>Завершен: 0</p>
                        </div>
                        <div class="col-xs-4">
                            <p>Клиенты : 0</p>
                            <p>Постоянных: 0</p>
                            <p>Просмотров: 15</p>
                        </div>
                        <div class="col-xs-4 text-center">
                            <h4>Бюджет</h4>
                            <p><a href="#" class="dashed"><span>за все время</span></a></p>
                            <p><strong>0 руб. 0 коп.</strong></p>
                            <a href="#" class="black">Хочу больше</a>
                        </div>
                    </div>
                    <div class="row">
                        <a href="#" class="col-xs-12"><img src="img/cnt/banner1.jpg" alt="" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="block recommend">
                <p><strong>Рекомендуемые заказы</strong></p>
                <ul>
                    <li>
                        <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                        <span class="char-sm">Д</span>
                        <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                        <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                        <div class="row">
                            <div class="col-xs-6">Цена<br><span>Договор</span></div>
                            <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                        <span class="char-sm">Д</span>
                        <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                        <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                        <div class="row">
                            <div class="col-xs-6">Цена<br><span>Договор</span></div>
                            <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                        <span class="char-sm">Д</span>
                        <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                        <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                        <div class="row">
                            <div class="col-xs-6">Цена<br><span>Договор</span></div>
                            <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                        <span class="char-sm">Д</span>
                        <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                        <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                        <div class="row">
                            <div class="col-xs-6">Цена<br><span>Договор</span></div>
                            <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                        <span class="char-sm">Д</span>
                        <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                        <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                        <div class="row">
                            <div class="col-xs-6">Цена<br><span>Договор</span></div>
                            <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
@stop