@extends('main.layout')

@section('afterAllJs')
    <script src="{{ asset('js/author-edit-page.js') }}"></script>
@stop

@section('content')

    <style>
        .editTemplate{
            border: 1px solid red;
        }
    </style>
    <nav id="nav">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-md-3">
                    <a href="#" class="logo"><img src="/img/logo.png" alt="" class="img-responsive"></a>
                </div>
                <div class="col-xs-8 col-md-9">
                    <ul class="row text-center">
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Мои заказы</a></li>
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Топ авторов</a></li>
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Готовые работы</a></li>
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Заработай!</a></li>
                        <li class="col-xs-5 col-xs-offset-1 col-sm-offset-0 col-sm-6 col-md-2"><a href="#"
                                                                                                  class="btn btn-default btn-sm"><span
                                        class="hidden-xs hidden-md">Сделать заказ</span><span
                                        class="hidden-sm hidden-lg">Заказать</span></a></li>
                        <li class="col-xs-6 col-sm-6 col-md-2 dropdown"><a href="#" class="bell"><span
                                        class="badge">14</span></a><a href="#" id="menu" data-target="#"
                                                                      data-toggle="dropdown" role="button"
                                                                      aria-haspopup="true"
                                                                      aria-expanded="false">Меню<span
                                        class="glyphicon glyphicon-menu-hamburger"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="menu">
                                <li><a href="#">Мои заказы</a></li>
                                <li><a href="#">Топ авторов</a></li>
                                <li><a href="#">Готовые работы</a></li>
                                <li><a href="#">Заработай!</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <section id="section" class="container profileEdit" data-templates="{{ $templatesResponse }}">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/img/banner1.jpg" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">Все еще сомневаетесь?</p>
                            <a href="#" class="btn btn-primary">Заказать работу</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="text-center readmore"><a href="/author">Вернуться в кабинет</a></p>
        <ul class="nav nav-tabs row text-center">
            <li role="presentation" class="col-xs-12 col-sm-4 active"><a data-toggle="tab" href="#profileEditMain">Редактирование
                    профиля</a></li>
            <li role="presentation" class="col-xs-12 col-sm-4"><a data-toggle="tab" href="#responseTemplates">Шаблоны
                    ответов</a></li>
            <li role="presentation" class="col-xs-12 col-sm-4"><a data-toggle="tab" href="#edit-subscribe">Настройка
                    подписки</a></li>
        </ul>

        <div class="tab-content row">
            <div class="tab-pane fade in active">
                <div class="col-xs-12 col-sm-4 col-md-3" id="profileEditMain">
                    <h4>Моя фотография</h4>
                    <p><a href="#" class="gray">удалить фото<span class="glyphicon glyphicon-remove"></span></a></p>
                    <p><img src="/img/cnt/photo-m.png" alt="" class="img-circle"></p>
                    <p>
                        <button class="btn btn-default btn-sm">Загрузить фото</button>
                    </p>
                    <p><a href="#" class="dashed"><span>Сделать фото с веб камеры</span></a>
                    <p><a href="#" class="dashed avatar"><span>Выбрать аватар</span></a></p>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <h4>Основные данные</h4>
                    <form @submit.prevent="validateBeforeSubmitAuthorEditMain" id="customerUser">
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-4"
                                 :class="{'has-error': errors.has('name_author') }">
                                <label>Имя*</label>
                                <input type="text" value="{{ $user->name }}" name='name_author' ref="editName"
                                       v-validate="'required'" class="form-control">
                                <p class="text-danger">@{{ errors.first('name_author') }}</p>
                            </div>
                            <div class="form-group col-xs-12 col-md-4"
                                 :class="{'has-error': errors.has('surname_author') }">
                                <label>Фамилия*</label>
                                <input type="text" value="{{ $user->surname }}" name='surname_author' ref="editLastName"
                                       v-validate="'required'" class="form-control">
                                <p class="text-danger">@{{ errors.first('surname_author') }}</p>
                            </div>
                            <div class="form-group col-xs-12 col-md-4"
                                 :class="{'has-error': errors.has('login_author') }">
                                <label>Логин*</label>
                                <input type="text" value="{{ $user->login }}" name='login_author' ref="editLogin"
                                       v-validate="'required'" class="form-control">
                                <p class="text-danger">@{{ errors.first('login_author') }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-horizontal col-xs-12 col-md-11">
                                <div class="form-group">
                                    <label class="control-label col-xs-3 col-sm-6 col-md-3 col-lg-2">Дата
                                        рождения</label>
                                    <div class="col-xs-9 col-sm-6 col-md-5 col-lg-3">
                                        <input type="date" value="{{ $user->birthday }}" ref="editBirthday"
                                               class="form-control">
                                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <textarea placeholder="О себе" class="form-control"
                                          ref="editBio">{{ $user->bio }}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-3">
                                <button class="btn btn-default btn-sm vue-disable" :disabled="disabledButton">
                                    сохранить
                                </button>
                            </div>
                            <span style="float: left" v-if="completeMain">@{{ messageComplete }}</span>
                        </div>
                    </form>
                    <hr>
                    <h4>Образование</h4>
                    <form method="post">
                        <div class="row">
                            {{--<masked-input   placeholder="Date" />--}}
                            <div class="form-group col-xs-12 col-md-6">
                                <input type="text" placeholder="Название ВУЗа"
                                       value="{{ $user->university }}" name='university' class="form-control"
                                       ref="editUniversity">
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="row">
                                    <div class="form-horizontal col-xs-7">
                                        <div class="form-group">
                                            <label class="control-label col-xs-6">Год окончания</label>
                                            <div class="col-xs-6">
                                                {{ Form::select('study_finish_date', $arrayYears, $user->study_finish_date,['class'=>'form-control','ref'=>'editFinishStudy']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-5">
                                        <select class="form-control">
                                            <option>Магистр</option>
                                            <option>Студент</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-6">
                                {{ Form::select('faculty', $faculties, $user->faculty,['class'=>'form-control','placeholder'=>'Выберите факультет','ref'=>'editFaculty']) }}
                            </div>
                            <div class="form-group col-xs-12 col-md-6">
                                {{ Form::select('specialty', $specialities, $user->specialty,['class'=>'form-control','placeholder'=>'Выберите специальность','ref'=>'editSpeciality']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-horizontal col-xs-12 col-md-11">
                                <div class="form-group">
                                    <label class="control-label col-xs-3 col-sm-6 col-md-3 col-lg-2">Начало
                                        сессии</label>
                                    <div class="col-xs-9 col-sm-6 col-md-5 col-lg-3">
                                        <input type="date" value="{{ $user->session_begin_date }}" class="form-control"
                                               ref="editSessionBegin">
                                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-3">
                                <button class="btn btn-default btn-sm vue-disabled" type="button"
                                        :disabled="disabledButton" v-on:click="ajaxEditAuthorEducation">
                                    сохранить
                                </button>
                            </div>
                            <span style="float: left" v-if="completeEducation">@{{ messageComplete }}</span>
                        </div>
                    </form>
                    <hr>
                    <h4>Выполняемые работы</h4>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" @click="selectAll">Выбрать все
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if(count($typeWorks))
                            @foreach($typeWorks as $typeWork)
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"
                                                   {{ $typeWork->setCheckedCheckBox($typeWork->id) }} name="typesWork"
                                                   value="{{ $typeWork->id }}">{{ $typeWork->title }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-3">
                            <button class="btn btn-default btn-sm vue-disabled" type="button"
                                    :disabled="disabledButton" v-on:click="ajaxEditAuthorTypeWork">
                                сохранить
                            </button>
                        </div>
                        <span style="float: left" v-if="completeTypeWork">@{{ messageComplete }}</span>
                    </div>
                    <hr>
                    <h4>Дисциплины</h4>
                    <div class="row">
                        <ul class="nav nav-tabs">
                            @foreach($disciplinesWithSubjects as $nameDiscipline => $valueDiscipline)
                                <li class="{{ $valueDiscipline['model']->setActive($loop->iteration) }}">
                                    <a data-toggle="tab" href="#{{ $valueDiscipline['tabs'] }}">
                                        {{ $nameDiscipline }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($disciplinesWithSubjects as $nameDiscipline => $valueDiscipline)
                                <div id="{{ $valueDiscipline['tabs'] }}" class="tab-pane fade {{ $valueDiscipline['model']->setActive($loop->iteration,'in') }}">
                                    @if(count($valueDiscipline['child'] ))
                                        @foreach($valueDiscipline['child']  as $idSubject=>$subject)
                                            <div class="col-xs-6">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="subjects" {{ $subject['model']->setCheckedCheckBox($idSubject) }} value="{{ $idSubject }}" type="checkbox">{{ $subject['title'] }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-3">
                            <button class="btn btn-default btn-sm" v-on:click="ajaxEditAuthorSubjects" :disabled="disabledButton">сохранить</button>
                        </div>
                        <span style="float: left" v-if="completeSubjects">@{{ messageComplete }}</span>
                    </div>
                    <hr>
                    <h4>Контактные данные</h4>
                    <form method="post" @submit.prevent="validateBeforeSubmitAuthorEditContact">
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-4"
                                 :class="{'has-error': errors.has('email_author') }">
                                <label>E-mail*</label>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="email" placeholder="mail@mail" name='email_author'
                                               value="{{ $user->email }}" ref="editEmail" v-validate="'required|email'"
                                               class="form-control">
                                        <p class="text-danger">@{{ errors.first('email_author') }}</p>
                                        <span class="form-control-feedback">
                                            <a href="#" class="dashed">
                                                <span>подтвердить</span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-md-4">
                                <label>Страна</label>
                                <select class="form-control">
                                    <option>Страна</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-4">
                                <label>Город</label>
                                <select class="form-control">
                                    <option>Город</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-12">Номер мобильного &nbsp;
                                <a href="#" class="dashed"
                                   v-on:click="addFieldPhoneNumber"
                                   v-if="flagAddField && totalField < 5">
                                    + <span>добавить номер</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group" id="fields">
                                @if(count($phones))
                                    @foreach($phones as $phone)
                                        <div class="form-group col-xs-12 col-md-4">
                                            <label>Телефон</label>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input type="text" placeholder="Телефон"
                                                           name="phone_user[]"
                                                           value="{{ $phone['phone'] }}"
                                                           class="form-control phoneNumber"
                                                           mask="\+\1 (111) 1111-11" placeholder="Phone" >
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-3">
                                <button class="btn btn-default btn-sm">сохранить</button>
                            </div>
                            <span style="float: left" v-if="completeContact">@{{ messageComplete }}</span>
                        </div>
                    </form>
                    <hr>
                    <h4>Изменить пароль</h4>
                    <form @submit.prevent="validateBeforeSubmitAuthorEditPassword">
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-4"
                                 :class="{'has-error': errors.has('old_password') }">
                                <label>Введите старый пароль</label>
                                <input type="password" name='old_password' ref="editOldPassword" v-validate="'required'"
                                       class="form-control">
                                <p class="text-danger">@{{ errors.first('old_password') }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-4" :class="{'has-error': errors.has('password') }">
                                <label>Введите новый пароль</label>
                                <input type="password" name="password"
                                       v-validate="'required|confirmed:password_confirmation'" ref="editNewPassword"
                                       class="form-control">
                                <p class="text-danger">@{{ errors.first('password') }}</p>
                            </div>
                            <div class="form-group col-xs-12 col-md-4"
                                 :class="{'has-error': errors.has('password_confirmation') }">
                                <label>Введите новый пароль еще раз</label>
                                <input type="password" name="password_confirmation" v-validate="'required'"
                                       class="form-control" ref="editPasswordConfirmation">
                                <p class="text-danger">@{{ errors.first('password_confirmation') }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-3">
                                <button class="btn btn-default btn-sm vue-disabled" type="submit"
                                        :disabled="disabledButton">
                                    сохранить
                                </button>
                            </div>
                            <span style="float: left" v-if="completePassword">@{{ messageComplete }}</span>
                        </div>
                    </form>
                    <hr>
                    <p><a href="#" class="gray">удалить аккаунт<span class="glyphicon glyphicon-remove"></span></a>
                </div>
            </div>
            <div class="tab-pane fade" id="responseTemplates">

                <div class="col-xs-12 col-sm-4 col-md-3 profile">
                    <h4>Моя фотография</h4>
                    <p><a href="#" class="gray">удалить фото<span class="glyphicon glyphicon-remove"></span></a></p>
                    <p><img src="/img/cnt/photo-m.png" alt="" class="img-circle"></p>
                    <p>
                        <button class="btn btn-default btn-sm">Загрузить фото</button>
                    </p>
                    <p><a href="#" class="dashed"><span>Сделать фото с веб камеры</span></a>
                    <p><a href="#" class="dashed avatar"><span>Выбрать аватар</span></a></p>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <h4>Шаблоны комментариев к ставкам</h4>
                    <form @submit.prevent="validateBeforeSubmitAuthorAddTemplate">
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-6" :class="{'has-error': errors.has('name_template_author') }">
                                <input type="text" placeholder="Название шаблона" name="name_template_author"  ref="nameTemplateAuthor" v-validate="'required'" class="form-control">
                                <p class="text-danger">@{{ errors.first('name_template_author') }}</p>
                            </div>
                            <div class="form-group col-xs-12 col-md-6">
                                <select class="form-control" style="width:100%" name="type_template_author"  ref="typeTemplateAuthor" v-validate="'required'">
                                    <option></option>
                                    @foreach($typeWorks as $typeWork)
                                        <option value="{{ $typeWork->id }}">{{ $typeWork->title }}</option>
                                    @endforeach
                                </select>
                                <p class="text-danger">@{{ errors.first('type_template_author') }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12" :class="{'has-error': errors.has('text_template_author') }">
                                <textarea placeholder="Текст шаблона" name="text_template_author" class="form-control" ref="textTemplateAuthor" v-validate="'required'"></textarea>
                                <p class="text-danger">@{{ errors.first('text_template_author') }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-7 col-md-4">
                                <button class="btn btn-default btn-sm vue-disabled" type="submit" :disabled="disabledButton" v-if="editTemplate == false">создать новый шаблон</button>
                                <button class="btn btn-default btn-sm vue-disabled"  type="submit" :disabled="disabledButton" v-else>Редактировать шаблон</button>
                            </div>
                            <span style="float: left" v-if="completeAddTemplate">@{{ messageComplete }}</span>
                        </div>
                    </form>
                    <hr>
                    <div id="templateResponse"></div>

                    <div v-for="let (template, index) of templates" v-if="template.status == 1" :class="[template.flag == 1 ? 'editTemplate' : '']">
                        <div class="pull-right actions">
                            <a href="javascript:void(0)" v-on:click="toggleTemplateState(template.id)" class="gray" v-if="template.status == 1">
                                редактировать<span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a href="javascript:void(0)" v-on:click="deleteTemplateState(template.id)" class="gray">
                                удалить<span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </div>
                        <h4>@{{ template.title }}</h4>
                        <p>
                            @{{ template.text }}
                        </p>
                        <hr v-if="index < templates.length - 1">
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="edit-subscribe">

                <div class="row">
                    <div class="col-xs-4">
                        <h4>Настройка уведомлений</h4>
                        <div class="checkbox">
                            <label><input type="checkbox">Новостные рассылки</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Новые заказы</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Статусы заказов</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 2000 до 5000</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Новые сообщения</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Получать SMS уведомления</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Сообщения о новых ставках</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Уведомления в браузер</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Уведомление о покупке новой работы</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Советы</label>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <h4>Бюджет</h4>
                        <div class="checkbox">
                            <label><input type="checkbox">По договоренности</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 200 до 500</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 2000 до 5000</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">более 10 000 менее 200</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 500 до 2000</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 5000 до 10000</label>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <h4>Срок сдачи работы</h4>
                        <div class="checkbox">
                            <label><input type="checkbox">Менее 3х дней</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 7 до 14</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">более месяца</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 3 до 7</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">от 14 до 30</label>
                        </div>
                        <p>&nbsp;</p>
                        <h4>Количество ставок</h4>
                        <div class="checkbox">
                            <label><input type="checkbox">Нет</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Менее 2</label>
                        </div>
                    </div>
                </div>
                <p>&nbsp;</p>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-3">
                        <button class="btn btn-default btn-sm">сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop