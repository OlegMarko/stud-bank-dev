@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="thumbnail autor-block">
                    <img src="/img/cnt/photo-m.png" alt="" class="img-circle">
                    <div class="caption">
                        <h4 class="text-center">Андрей Киреев</h4>
                        <small class="text-center">kireev_1984</small>
                        <p class="location">г. Москва</p>
                        <p class="aphone">(8182) 65-01-33</p>
                        <p class="aid">1311231</p>
                        <p><a href="#" class="gray">редактировать</a></p>
                    </div>
                </div>
                <h4>Типы работ:</h4>
                <ul class="list">
                    <li><a href="#">Дипломная работа</a></li>
                    <li><a href="#">Курсовая работа</a></li>
                    <li><a href="#">Реферат</a></li>
                    <li><a href="#">Магистерская диссертация</a></li>
                    <li><a href="#">Отчет по практике</a></li>
                    <li><a href="#">Статья</a></li>
                </ul>
                <p>&nbsp;</p>
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="block author-info">
                    <span class="label label-success label-bs">Академик</span>
                    <div class="row">
                        <div class="col-xs-3 ball">
                            <p>Средняя оценка:</p>
                            <strong>4.5</strong>
                            <span class="rating"><span style="width:75%;"></span></span>
                            <p>Рейтинг: 75</p>
                        </div>
                        <div class="col-xs-3 achievements">
                            <p>Достижения</p>
                            <small>нет достижений</small>
                        </div>
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-6 done">
                                    <strong>145</strong>
                                    <small>заказов выполнено</small>
                                </div>
                                <div class="col-xs-6 underway">
                                    <strong>4</strong>
                                    <small>заказа в работе</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 canceled1">
                                    <strong>0</strong>
                                    <small>заказы отмененные автором</small>
                                </div>
                                <div class="col-xs-6 canceled2">
                                    <strong>2</strong>
                                    <small>заказы отмененные клиентами</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-lg-8">
                        <h4>Информация обо мне:</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lacinia nunc quam, non tristique metus euismod sit amet. Aliquam sit amet venenatis ante. Fusce at neque malesuada, euismod turpis sed, aliquet leo. Vivamus ut massa faucibus, venenatis purus sit amet, sodales orci. Donec sed purus malesuada, finibus nulla eget, feugiat velit. Proin ac ultrices felis, id auctor lorem. Nunc condimentum risus turpis.</p>
                        <p class="text-center"><small><a href="#" class="dashed gray"><small>∨</small> <span>подробнее</span> <small>∨</small></a></small></p>
                        <div class="row">
                            <div class="col-xs-4 col-md-3">
                                <h5>Возраст</h5>
                                <p>24 года</p>
                            </div>
                            <div class="col-xs-4 col-md-3">
                                <h5>Страна</h5>
                                <p>Россия</p>
                            </div>
                            <div class="col-xs-4 col-md-6">
                                <h5>Город</h5>
                                <p>Петропавловс-Камчатский</p>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <h5>Образование:</h5>
                        <p>Психолог: факультет психологии, Московский государственный университет (1984)<br>Специалист по связям с общественностью: Московская академия управления (1996)</p>
                        <h5>Специализация:</h5>
                        <p>Психолог, Специалист по связям с общественностью</p>
                        <p>&nbsp;</p>
                        <ul class="nav nav-tabs nav-sm row text-center" role="tablist">
                            <li role="presentation" class="col-xs-6 col-md-3 active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Отзывы (2)</a></li>
                            <li role="presentation" class="col-xs-6 col-md-3"><a href="#tab2" aria-controls="tab1" role="tab" data-toggle="tab">Выполненные (145)</a></li>
                            <li role="presentation" class="col-xs-6 col-md-3"><a href="#tab3" aria-controls="tab1" role="tab" data-toggle="tab">Статистика</a></li>
                            <li role="presentation" class="col-xs-6 col-md-3"><a href="#">Портфолио (45)</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab1">
                                <ul class="notice">
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                            </div>
                                            <div class="col-xs-7">
                                                <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                                <p>Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</p>
                                            </div>
                                            <div class="col-xs-1 text-center"><span class="glyphicon glyphicon-hand-up"></span></div>
                                            <div class="col-xs-2 text-center hand-up">В срок</div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <img src="/img/cnt/photo-xs.png" alt="" class="img-circle">
                                            </div>
                                            <div class="col-xs-7">
                                                <h6>Андрей Киреев <time>15.03.2016 18:00</time></h6>
                                                <p>Быстро икачественно, все правки выполены в срок и сдача прошла успешно. Спасибо огромное. Если будут ещё проблемы - обязательно обращусь ещё. Рекомендую, качественный исполнитель и хороший собеседник. Удачи в дальнейшей работе!</p>
                                            </div>
                                            <div class="col-xs-1 text-center"><span class="glyphicon glyphicon-hand-down"></span></div>
                                            <div class="col-xs-2 text-center hand-down">Отказ на 50%</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab2">
                                <ul class="diploms">
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-1">
                                                <span class="char-md">Д</span>
                                            </div>
                                            <div class="col-xs-8">
                                                <small>Дипломная работа | Педагогика</small>
                                                <p><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            </div>
                                            <div class="col-xs-1 text-center"><span class="glyphicon glyphicon-hand-up"></span></div>
                                            <div class="col-xs-2 text-center"><span class="glyphicon glyphicon-time"></span></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-1">
                                                <span class="char-md">Д</span>
                                            </div>
                                            <div class="col-xs-8">
                                                <small>Дипломная работа | Педагогика</small>
                                                <p><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            </div>
                                            <div class="col-xs-1 text-center"><span class="glyphicon glyphicon-hand-down"></span></div>
                                            <div class="col-xs-2 text-center hand-down">Отказ на 50%</div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-1">
                                                <span class="char-md">Д</span>
                                            </div>
                                            <div class="col-xs-8">
                                                <small>Дипломная работа | Педагогика</small>
                                                <p><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            </div>
                                            <div class="col-xs-1 text-center"><span class="glyphicon glyphicon-hand-up"></span></div>
                                            <div class="col-xs-2 text-center"><span class="glyphicon glyphicon-time"></span></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-1">
                                                <span class="char-md">Д</span>
                                            </div>
                                            <div class="col-xs-8">
                                                <small>Дипломная работа | Педагогика</small>
                                                <p><a href="#">Взаимосвязь различных видов экономического развития и экономического планирования. Различие долгосрочного и краткосрочного различия</a></p>
                                            </div>
                                            <div class="col-xs-1 text-center"><span class="glyphicon glyphicon-hand-down"></span></div>
                                            <div class="col-xs-2 text-center hand-down">Отказ на 50%</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab3">
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <p><img src="/img/cnt/grafik1.png" alt="" class="img-responsive"></p>
                                        <p>&nbsp;</p>
                                        <h4>Статистика выполненных работ:</h4>
                                        <ul class="list">
                                            <li><a href="#">Дипломная работа (34)</a></li>
                                            <li><a href="#">Курсовая работа (4)</a></li>
                                            <li><a href="#">Реферат (2)</a></li>
                                            <li><a href="#">Магистерская диссертация (12)</a></li>
                                            <li><a href="#">Отчет по практике (34)</a></li>
                                            <li><a href="#">Статья (4)</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <div class="block recommend">
                            <p><strong>Рекомендуемые заказы</strong></p>
                            <ul>
                                <li>
                                    <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                                    <span class="char-sm">Д</span>
                                    <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                                    <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                                    <div class="row">
                                        <div class="col-xs-6">Цена<br><span>Договор</span></div>
                                        <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                                    <span class="char-sm">Д</span>
                                    <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                                    <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                                    <div class="row">
                                        <div class="col-xs-6">Цена<br><span>Договор</span></div>
                                        <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                                    <span class="char-sm">Д</span>
                                    <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                                    <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                                    <div class="row">
                                        <div class="col-xs-6">Цена<br><span>Договор</span></div>
                                        <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#" class="gray glyphicon glyphicon-remove pull-right"></a>
                                    <span class="char-sm">Д</span>
                                    <small># 53197554<br>Дипломная работа<br>Менеджмент</small>
                                    <p><a href="#">Менеджмент качества в управлении персоналом. Вариант 15.</a></p>
                                    <div class="row">
                                        <div class="col-xs-6">Цена<br><span>Договор</span></div>
                                        <div class="col-xs-6">Срок сдачи<br><span>24.01.2016</span></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop