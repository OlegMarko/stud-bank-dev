@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container text-center">
        <h1>Служба заботы</h1>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <p>Здесь вы можете оставить свои пожелания , отзывы, а так же сообщения для специалистов. Не забудьте , пожалуйста, указать номер Вашего заказа, а так же выберите тему Вашего сообщения.</p>
                <div class="row">
                    <div class=" col-xs-12 col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <select class="form-control">
                                    <option>Номер заказа</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <select class="form-control">
                                    <option>Вопрос по заказу</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-10 col-md-offset-1">
                        <textarea placeholder="Текст сообщения" class="form-control"></textarea>
                    </div>
                </div>
                <div id="file" class="col-xs-12 col-md-10 col-md-offset-1">
                    <a href="#" class="dashed"><span>Прикрепить файл</span></a> или перетащите файлы сюда
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <button class="btn btn-default">отправить</button>
                        <p class="readmore1"><a href="#">отменить</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop