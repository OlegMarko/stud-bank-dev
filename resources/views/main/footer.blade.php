<div id="dado">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 phone">
                <span class="icop"></span>
                8-800-<strong>888-88-88</strong>
                <small>Время работы по будням с 10 до 20 по МСК</small>
            </div>
            <div class="col-xs-12 col-sm-6 text-right">
                <a href="#" class="googleapp clearfix">
                    <img src="/img/gplay.png" alt="" class="pull-right">
                    Приложение<br>от нашего сайта
                </a>
            </div>
        </div>
    </div>
</div>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <a href="#"><img src="{{asset('img')}}/logo.png" alt="" class="img-responsive"></a>
            </div>
            <div class="col-xs-12 col-md-6 social text-right">
                @include('main.footer.social_link',['socialLinks'=>\App\Models\Main\SiteSettings::getMultiSettings(\App\Models\Main\SiteSettings::KEYS_SOCIAL_LINKS)])
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-6 col-md-3">
                <h4>Основное</h4>
                <ul>
                    <li><a href="#">Правила использования</a></li>
                    <li><a href="#">Политика конфиденциальности</a></li>
                    <li><a href="#">Справка</a></li>
                    <li><a href="#">Как это работает</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-3">
                <h4>Авторам и студентам</h4>
                <ul>
                    <li><a href="#">Авторы студенческих работ</a></li>
                    <li><a href="#">Работа репетитором</a></li>
                    <li><a href="#">Работа для преподавателей</a></li>
                    <li><a href="#">Заработок для студентов</a></li>
                    <li><a href="#">Заказ дипломной работы</a></li>
                    <li><a href="#">Отзывы об Автор24</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-3">
                <h4>Партнерам</h4>
                <ul>
                    <li><a href="#">Партнерская программа</a></li>
                    <li><a href="#">Агентствам</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-3 payment">
                <h4>Мы принимаем</h4>
                <img src="{{asset('img')}}/payment1.png" alt="">
                <img src="{{asset('img')}}/payment2.png" alt="">
                <img src="{{asset('img')}}/payment3.png" alt="">
                <img src="{{asset('img')}}/payment4.png" alt="">
                <img src="{{asset('img')}}/payment5.png" alt="">
            </div>
            <p class="copy">© 2016 WORKPRICE</p>
        </div>
    </div>
</footer>
<div id="authForms">
    {{ Widget::run('PopUpLogin') }}
    {{ Widget::run('PopUpRegister') }}
</div>
