@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="nom text-center">Оплата услуги "Название услуги"</h1>
        <p class="text-center">Дипломная работа "Тема дипломной работы"</p>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-xs-offset-0 col-md-offset-2">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="block payment">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="price">
                                        <p>Сумма к оплате:</p>
                                        <strong>1.490 руб.</strong>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-md-offset-1">После закрепления ваш заказ окажется на самом верху ленты аукциона. Его увидят большее число потенциальных авторов.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-7 col-lg-5">
                        <h6>Выберите способ оплаты</h6>
                        <select class="form-control">
                            <option>Россия</option>
                        </select>
                    </div>
                </div>
                <p>&nbsp;</p>
                <div class="row payments payments-checked">
                    <div class="col-xs-12"><h6>Выберите способ оплаты</h6></div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label>
                            <input type="radio" name="paym" value="1" class="hide">
                            <div>
                                <p><img src="img/payment3.png" alt=""></p>
                                <h4>Яндекс-деньги</h4>
                                <p>сервисный сбор 10.53 р.</p>
                            </div>
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label>
                            <input type="radio" name="paym" value="1" class="hide" checked>
                            <div>
                                <p><img src="img/payment2.png" alt=""></p>
                                <h4>Webmoney</h4>
                                <p>сервисный сбор 10.53 р.</p>
                            </div>
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label>
                            <input type="radio" name="paym" value="1" class="hide">
                            <div>
                                <p><img src="img/payment5.png" alt=""></p>
                                <h4>PayPal</h4>
                                <p>сервисный сбор 10.53 р.</p>
                            </div>
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label>
                            <input type="radio" name="paym" value="1" class="hide">
                            <div>
                                <p><img src="img/payment1.png" alt=""></p>
                                <h4>VISA</h4>
                                <p>сервисный сбор 10.53 р.</p>
                            </div>
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label>
                            <input type="radio" name="paym" value="1" class="hide">
                            <div>
                                <p><img src="img/payment4.png" alt=""></p>
                                <h4>MasterCars</h4>
                                <p>сервисный сбор 10.53 р.</p>
                            </div>
                        </label>
                    </div>
                </div>
                <p><label><input type="checkbox" checked> <big>Согласен с условиями <a href="#">пользовательского соглашения</a></big></label></p>
                <div class="row">
                    <div class="col-xs-12 col-md-9 payment-descr">Для совершения оплаты вы будете перенаправлены на сайт платежной системы. Деньги поступят на счет в течении минуты после завершения оплаты.</div>
                    <div class="col-xs-12 col-md-3"><a href="#" class="btn btn-success btn-corn">оплата</a></div>
                </div>
            </div>
        </div>
    </section>
@stop