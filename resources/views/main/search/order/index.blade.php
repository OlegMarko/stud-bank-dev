@extends('main.layout')

@section('content')
    @include('main.header')
    <section id="section" class="container">
        <h1 class="text-center nom">Поиск заказа</h1>
        <div class="row block1">
            <div class="col-xs-12 col-md-9">
                <div class="block filter">
                    <div class="row">
                        <div class="col-xs-12 col-md-7">
                            <input type="text" placeholder="Введите тему работы" class="form-control">
                        </div>
                        <div class="col-xs-12 col-md-5">
                            <select class="form-control">
                                <option>Выберите предмет</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-3">
                            <select class="form-control">
                                <option>Все типы работ</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label><input type="checkbox" checked> В заказе меньше 3х ставок</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5">
                            <div class="form-group">
                                <label><input type="checkbox" checked> В том числе с неуказанным бюджетом</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 form-horizontal">
                            <div class="form-group form-group-sm">
                                <label class="control-label col-xs-4">Бюджет, руб.</label>
                                <div class="col-xs-4"><input type="text" value="1500" class="form-control"></div>
                                <div class="col-xs-4 limit"><input type="text" value="4500" class="form-control"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <input id="slider-line" type="text" value="" data-slider-min="0" data-slider-max="5000" data-slider-step="100" data-slider-value="[1500,4500]">
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <button class="btn btn-default">Найти</button>
                        </div>
                    </div>
                    <p class="text-center"><small><a href="#" class="dashed gray"><small>&or;</small> <span>дополнительно</span> <small>&or;</small></a></small></p>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="checkbox">
                    <label><input type="checkbox"> Заказы по моим предметам<br> &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp; <a href="#">настроить мои предметы</a></label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox"> Онлайн помощь на экзамене</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox"> Только <mark>PRO</mark></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="row hdr">
                    <div class="col-xs-6 col-md-5">Тема, предмет, тип работы</div>
                    <div class="col-xs-6 col-md-2 text-right"><a href="#" class="gray dashed"><span>Заказ размещен</span> &and;</a></div>
                    <div class="col-xs-4 col-md-2 text-right"><a href="#" class="gray dashed"><span>Срок сдачи</span> &and;</a></div>
                    <div class="col-xs-4 col-md-1 text-right"><a href="#" class="gray dashed"><span>Ставки</span> &and;</a></div>
                    <div class="col-xs-4 col-md-2 text-center"><a href="#" class="gray dashed"><span>Стоимость, руб.</span> &and;</a></div>
                </div>
                <div class="search1">
                    <ul>
                        <li class="new">
                            <span class="label label-warning">магистр</span>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д<small>5</small></span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small># 53197554 | Дипломная работа | Менеджмент</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-5">
                                            <p class="title"><a href="#">Дипломная работа по педагогике <mark>PRO</mark></a></p>
                                        </div>
                                        <div class="col-xs-2 col-md-2">Вчера</div>
                                        <div class="col-xs-2 col-md-2 text-center">18.02.2016</div>
                                        <div class="col-xs-1">2</div>
                                        <div class="col-xs-3 col-md-2 text-center">договорная</div>
                                    </div>
                                </div>
                                <a href="#fav" class="active"></a>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д<small>5</small></span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small># 53197554 | Дипломная работа | Менеджмент</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-5">
                                            <p class="title"><a href="#">Дипломная работа по педагогике <mark>PRO</mark></a></p>
                                        </div>
                                        <div class="col-xs-2 col-md-2">Вчера</div>
                                        <div class="col-xs-2 col-md-2 text-center">18.02.2016</div>
                                        <div class="col-xs-1">2</div>
                                        <div class="col-xs-3 col-md-2 text-center">договорная</div>
                                    </div>
                                </div>
                                <a href="#fav"></a>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д<small>5</small></span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small># 53197554 | Дипломная работа | Менеджмент</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-5">
                                            <p class="title"><a href="#">Дипломная работа по педагогике <mark>PRO</mark></a></p>
                                        </div>
                                        <div class="col-xs-2 col-md-2">Вчера</div>
                                        <div class="col-xs-2 col-md-2 text-center">18.02.2016</div>
                                        <div class="col-xs-1">2</div>
                                        <div class="col-xs-3 col-md-2 text-center">договорная</div>
                                    </div>
                                </div>
                                <a href="#fav"></a>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д<small>5</small></span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small># 53197554 | Дипломная работа | Менеджмент</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-5">
                                            <p class="title"><a href="#">Дипломная работа по педагогике <mark>PRO</mark></a></p>
                                        </div>
                                        <div class="col-xs-2 col-md-2">Вчера</div>
                                        <div class="col-xs-2 col-md-2 text-center">18.02.2016</div>
                                        <div class="col-xs-1">2</div>
                                        <div class="col-xs-3 col-md-2 text-center">договорная</div>
                                    </div>
                                </div>
                                <a href="#fav"></a>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д<small>5</small></span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small># 53197554 | Дипломная работа | Менеджмент</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-5">
                                            <p class="title"><a href="#">Дипломная работа по педагогике <mark>PRO</mark></a></p>
                                        </div>
                                        <div class="col-xs-2 col-md-2">Вчера</div>
                                        <div class="col-xs-2 col-md-2 text-center">18.02.2016</div>
                                        <div class="col-xs-1">2</div>
                                        <div class="col-xs-3 col-md-2 text-center">договорная</div>
                                    </div>
                                </div>
                                <a href="#fav"></a>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-1">
                                    <span class="char-md">Д<small>5</small></span>
                                </div>
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-12"><small># 53197554 | Дипломная работа | Менеджмент</small></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-5">
                                            <p class="title"><a href="#">Дипломная работа по педагогике <mark>PRO</mark></a></p>
                                        </div>
                                        <div class="col-xs-2 col-md-2">Вчера</div>
                                        <div class="col-xs-2 col-md-2 text-center">18.02.2016</div>
                                        <div class="col-xs-1">2</div>
                                        <div class="col-xs-3 col-md-2 text-center">договорная</div>
                                    </div>
                                </div>
                                <a href="#fav"></a>
                            </div>
                        </li>
                    </ul>
                </div>
                <nav class="text-center">
                    <ul class="pagination pagination-lg">
                        <li class="disabled"><a href="#" aria-label="Previous"></a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#" aria-label="Next"></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
@stop