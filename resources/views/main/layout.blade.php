<?php
/**
 * @var $seoData array
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">

    <title>{{ $seoData['title'] or null }}</title>
    <meta name="description" content="{{ $seoData['description'] or null  }}">
    <meta name="keywords" content="{{ $seoData['keywords']  or null }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,minimum-scale=0.25,maximum-scale=1.6,initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700,800&subset=latin,cyrillic"
          rel="stylesheet">
    <link href="{{ asset('css') }}/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>

@yield('content')

@include('main.footer')

<link href="{{ asset('css') }}/styles.css" rel="stylesheet">
<link href="{{ asset('css') }}/select2.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="{{ asset('js') }}/app.js"></script>
<script src="{{ asset('js') }}/authApp.js"></script>
{{--<script src="{{ asset('js/appFront') }}/bootstrap.min.js"></script>--}}
<script src="{{ asset('js/appFront') }}/select2.min.js"></script>
<script src="{{ asset('js/appFront') }}/scripts.js"></script>
@yield('afterAllJs')
</body>
</html>
