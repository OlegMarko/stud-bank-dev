@extends('main.layout')

@section('content')
    <nav id="nav">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-md-3">
                    <a href="#" class="logo"><img src="img/logo.png" alt="" class="img-responsive"></a>
                </div>
                <div class="col-xs-8 col-md-9">
                    <ul class="row text-center">
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Мои заказы</a></li>
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Топ авторов</a></li>
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Готовые работы</a></li>
                        <li class="col-md-2 hidden-xs hidden-sm"><a href="#">Заработай!</a></li>
                        <li class="col-xs-5 col-xs-offset-1 col-sm-offset-0 col-sm-6 col-md-2"><a href="#" class="btn btn-default btn-sm"><span class="hidden-xs hidden-md">Сделать заказ</span><span class="hidden-sm hidden-lg">Заказать</span></a></li>
                        <li class="col-xs-6 col-sm-6 col-md-2 dropdown"><a href="#" class="bell"><span class="badge">14</span></a><a href="#" id="menu" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Меню<span class="glyphicon glyphicon-menu-hamburger"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="menu">
                                <li><a href="#">Мои заказы</a></li>
                                <li><a href="#">Топ авторов</a></li>
                                <li><a href="#">Готовые работы</a></li>
                                <li><a href="#">Заработай!</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <section id="section" class="container">
        <h1>Инструкции &nbsp;&nbsp;&nbsp; <a href="#" class="small">Вернуться в кабинет</a></h1>
        <div class="row">
            <aside class="col-xs-12 col-sm-4 col-md-3">
                @include('pages.manual.sidebar',['menu'=>\App\Models\Main\Manual::getListMenu()])
            </aside>
            <article class="col-xs-12 col-sm-7 col-md-8 col-sm-offset-1">
                <h2>{{ $page->h2 }}</h2>
                {!! $page->text !!}
            </article>
        </div>
    </section>
@stop