<ul class="nav block">
    @foreach($menu as $item)
        <li role="presentation" class="{{ $item->setActive($item->slug) }}"><a href="{{ $item->slug }}">{{  $item->h2 }}</a></li>
    @endforeach
</ul>