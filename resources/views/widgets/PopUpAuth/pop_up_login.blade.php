<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="orderLabel">
    <div class="modal-dialog modal-sm modal-auth">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Вход</h3>
            </div>
            <div class="modal-body">
                <form @submit.prevent="validateAuthBeforeSubmit">
                    <div class="form-group" :class="{'has-error': errors.has('auth_email') }">
                        <input type="email" placeholder="E-mail*" name="auth_email" ref="authEmail" v-validate="'required|email'" v-model="authEmail" class="form-control">
                        <p v-show="errors.has('auth_email')" class="text-danger">@{{ errors.first('auth_email') }}</p>
                    </div>
                    <div class="form-group" :class="{'has-error': errors.has('auth_password') }">
                        <input type="password" placeholder="Пароль*" name="auth_password" ref="auth_password" v-model="authPassword" v-validate="'required'" class="form-control">
                        <p v-show="errors.has('auth_password')" class="text-danger">@{{ errors.first('auth_password') }}</p>
                    </div>
                    <div class="form-group">
                        <button :disabled="disabledButton" class="btn btn-default btn-lg vue-disable">Войти</button>
                        <p class="text-danger" v-if="authError">@{{ authError }}</p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <p>Еще не зарегистрированы?</p>
                <a href="#" class="btn btn-link" @click.prevent="toggleModals('login')">Регистрация</a>
            </div>
        </div>
    </div>
</div>
