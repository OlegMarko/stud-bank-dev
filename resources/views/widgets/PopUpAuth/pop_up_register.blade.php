<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="orderLabel">
    <div class="modal-dialog modal-lg modal-auth">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Регистрация</h3>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs nav-sm row text-center" role="tablist">
                    <li role="presentation" class="col-xs-6 active">
                        <a href="#register1" aria-controls="register1" role="tab" data-toggle="tab">Как заказчик<br>
                            <small>Я хочу размещать заказы</small>
                        </a>
                    </li>
                    <li role="presentation" class="col-xs-6">
                        <a href="#register2" aria-controls="register2" role="tab" data-toggle="tab">Как автор<br>
                            <small>Я хочу выполнять заказы</small>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="register1">

                        <form @submit.prevent="validateBeforeSubmitCustomer" id="customerUser">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('name_student') }">
                                    <input type="text" placeholder="Имя" name="name_student" ref="regName" v-model="regName" v-validate="'required'" class="form-control">
                                    <p class="text-danger">@{{ errors.first('name_student') }}</p>
                                </div>
                                <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('login_student') }">
                                    <input type="text" placeholder="Логин*" name="login_student" ref="regLogin" v-model="regLogin" v-validate="'required|min:2'" class="form-control">
                                    <p class="text-danger">@{{ errors.first('login_student') }}</p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('email_student') }">
                                    <input type="text" placeholder="E-mail*" name="email_student" ref="regEmail" v-model="regEmail" v-validate="'required|email'" class="form-control">
                                    <p class="text-danger">@{{ errors.first('email_student') }}</p>
                                </div>
                                <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('phone') }">
                                    <input type="text" placeholder="Телефон" name="phone" ref="regPhone" v-model="regPhone" v-validate="'required|min:2'" class="form-control">
                                    <p class="text-danger">@{{ errors.first('phone') }}</p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('password_student') }">
                                    <input type="password" placeholder="Пароль*" name="password_student" ref="regPass" v-model="regPass" v-validate="'required|min:6|confirmed:password_confirmation_student'" class="form-control">
                                    <p class="text-danger">@{{ errors.first('password_student') }}</p>
                                </div>
                                <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('password_confirmation_student') }">
                                    <input type="password" placeholder="Повторите пароль*" v-validate="'required|min:6'" v-model="regRepeatPass" name="password_confirmation_student"  ref="regRepeatPass" class="form-control">
                                    <p class="text-danger">@{{ errors.first('password_confirmation_student') }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <p>&nbsp;</p>
                                <div class="checkbox">
                                    <label>
                                        <input name="agency" type="checkbox" ref="regAgency" >Я агентство
                                    </label>
                                </div>
                                <div class="checkbox" :class="{'has-error': errors.has('rules_student') }">
                                    <label>
                                        <input name="rules_student" type="checkbox" ref="regRules" v-model="regRules" v-validate="'required|not_in:0'" >Я прочитал и согласен с <a href="#">условиями договора</a>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="notifyStudent" type="checkbox" ref="regNotifyStudent" v-model="regNotifyStudent">
                                        «Оповещать меня по почте, когда отвечают на проект или пишут личное сообщение»
                                    </label>
                                    <p class="text-danger">@{{ regNotifyStudentError }}</p>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="notifyAndNews" type="checkbox" ref="regNotifyAndNews" v-model="regNotifyAndNews">
                                        Я хочу получать уведомления и новости сайта на почту
                                    </label>
                                </div>
                                <p>&nbsp;</p>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row captcha">
                                        <div class="col-xs-6">
                                            <img src="{{asset('img')}}/cnt/captcha.png" alt="" class="img-responsive">
                                        </div>
                                        <div class="col-xs-6"><input type="text" placeholder="Введите символы"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <button class="btn btn-default btn-lg vue-disable" type="submit">Регистрация</button>
                                </div>
                                <p class="text-danger">@{{ regFormError }}</p>
                                <p class="text-danger" v-for="(value, key, index) in regAuthorFormError">@{{ value }}</p>
                            </div>
                        </form>
                    </div>


                    <div role="tabpanel" class="tab-pane" id="register2">
                        <form @submit.prevent="validateBeforeSubmit" id="registerForm">
                            <div class="row form-group">
                            <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('name_author') }">
                                <input type="text" placeholder="Имя"  name="name_author" ref="regAuthorName" v-validate="'required|min:2'" v-model="regAuthorName" class="form-control">
                                <p v-show="errors.has('name_author')" class="text-danger">@{{ errors.first('name_author') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('family') }">
                                <input type="text" placeholder="Фамилия*" v-validate="'required|min:2'" name="family" ref="regAuthorFamily" v-model="regAuthorFamily" class="form-control">
                                <p v-show="errors.has('family')" class="text-danger">@{{ errors.first('family') }}</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('email') }">
                                <input type="text" placeholder="E-mail*"  name="email" v-validate="'required|email'" ref="regEmail" v-model="regAuthorEmail" class="form-control">
                                <p v-show="errors.has('email')" class="text-danger">@{{ errors.first('email') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('login') }">
                                <input type="text" placeholder="Логин*" v-validate="'required|min:2'" name="login" ref="regAuthorLogin" v-model="regAuthorLogin" class="form-control">
                                <p v-show="errors.has('login')" class="text-danger">@{{ errors.first('login') }}</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('password') }">
                                <input type="password" placeholder="Пароль*" v-validate="'required|min:6|confirmed'" name="password" ref="regAuthorPass" v-model="regAuthorPass" class="form-control">
                                <p v-show="errors.has('password')" class="text-danger">@{{ errors.first('password') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('password_confirmation') }">
                                <input type="password" placeholder="Повторите пароль*" v-validate="'required|min:6'" name="password_confirmation"  ref="regAuthorRepeatPass" v-model="regAuthorRepeatPass" class="form-control">
                                <p v-show="errors.has('password_confirmation')" class="text-danger">@{{ errors.first('password_confirmation') }}</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12 col-sm-6" :class="{'has-error': errors.has('phone') }">
                                <input type="text" placeholder="Телефон" name="phone" ref="regAuthorPhone" v-model="regAuthorPhone" class="form-control">
                                <p v-show="errors.has('phone')" class="text-danger">@{{ errors.first('phone') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <select class="form-control" id='town' name='town'>
                                    <option></option>
                                    <option value="Town" >Город</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <input type="text" placeholder="Название ВУЗа" name='university'  ref="regAuthorUniversity" v-model="regAuthorUniversity" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12 col-sm-7">
                                <input type="text" placeholder="Специальность" class="form-control" name='speciality'  ref="regAuthorSpeciality" v-model="regAuthorSpeciality">
                            </div>
                            <div class="col-xs-12 col-sm-5 form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-xs-6">Год окончания</label>
                                    <div class="col-xs-6">
                                        <select class="form-control" id="endLearnYear" name='endLearnYear'>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <p>&nbsp;</p>
                            <div class="checkbox" :class="{'has-error': errors.has('rules') }">
                                <label>
                                    <input name="rules" type="checkbox" ref="regAuthorRules" v-model="regAuthorRules"  v-validate="'required'" >Я прочитал и согласен с <a href="#">условиями договора</a>
                                </label>
                            </div>
                            <div class="checkbox"><label>
                                    <input name="notifyAndNews" type="checkbox" ref="regNotifyAndNews" v-model="regAuthorNotifyAndNews">Я хочу получать уведомления и новости сайта на почту
                                </label>
                            </div>
                            <p>&nbsp;</p>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row captcha">
                                    <div class="col-xs-6">
                                        <img src="{{asset('img')}}/cnt/captcha.png" alt="" class="img-responsive">
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" placeholder="Введите символы">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <button class="btn btn-default btn-lg vue-disable" :disabled="disabledRegButton">Регистрация</button>
                            </div>
                            <p class="text-danger" v-for="(value, key, index) in regAuthorFormError">@{{ value }}</p>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p>Уже зарегистрированы?</p>
                <p><a href="#" class="btn btn-link" @click.prevent="toggleModals('register')">Войдите</a></p>
                <p>
                    <small>
                        Внимание! Общение между авторами и заказчиками возможно только на сайте<br>Запрещается обмен
                        контактными данными (e-mail, icq, skype, телефон и т. д.). При выявлении таких попыток обмена
                        контактными данными Администрация ресурса блокирует аккаунт пользователя.
                    </small>
                </p>
            </div>
        </div>
    </div>
</div>