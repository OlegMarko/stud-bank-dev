<?php
/**
 * @var $widget \App\Widgets\FeedbackWidget
 */
?>
@if($widget->totalCount)
    <div class="container text-center">
        <h3>Более 5000 студентов используют биржу WorkPrice <br>для заказа студенческих работ.</h3>
        <div id="reviews" class="row">
            <div class="images">
                @foreach($widget->items as $item)
                    <img src="{{ asset($item->image) }}" alt="{{ $item->name }}" class="img-circle {{ $widget->getImageClass($loop->iteration) }}">
                @endforeach
            </div>
            @foreach($widget->items as $item)
                <div class="review {{ $widget->getTextClass($loop->iteration) }}">
                    <q>{{ $item->text }}</q>
                    <p>{{ $item->name }}</p>
                </div>
            @endforeach
            @if($widget->displayArrows)
                <a href="#prev"></a>
                <a href="#next"></a>
            @endif
        </div>
    </div>
@endif