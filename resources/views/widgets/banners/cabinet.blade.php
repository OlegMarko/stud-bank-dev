@if($banners->count())
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($banners as $key=>$banner )
                <li data-target="#carousel" data-slide-to="{{ $loop->index }}" class="{{ $banner->setActiveItem($loop->index) }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner" role="listbox">
            @foreach($banners as $key=>$banner )
                <div class="item {{ $banner->setActiveItem($loop->index) }}">
                    <img src="{{ $banner->dir }}/{{ $banner->image }}" alt="">
                    <div class="carousel-caption">
                        <div class="col-xs-6 col-md-4 col-xs-offset-6">
                            <p class="h1">{{ $banner->title }}</p>
                            <a href="{{ $banner->url }}" class="btn btn-primary">{{ $banner->button_text }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif