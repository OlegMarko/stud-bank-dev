@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}"
                         class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">Меню</li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>{{ trans('backpack::base.dashboard') }}</span>
                    </a>
                </li>

                {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-font"></i> <span>Контент</span> <i--}}
                                {{--class="fa fa-angle-left pull-right"></i></a>--}}

                    {{--<ul class="treeview-menu">--}}
                        {{--<li class="treeview">--}}
                            {{--<a href="#">--}}
                                {{--<i class="fa fa-object-group"></i>--}}
                                {{--<span>Элементы главной</span>--}}
                                {{--<i class="fa fa-angle-left pull-right"></i>--}}
                            {{--</a>--}}
                            {{--<ul class="treeview-menu">--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<i class="fa fa-group"></i> <span>Айтем</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<i class="fa fa-key"></i>--}}
                                        {{--<span>Айтем</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="{{ url(config('backpack.base.route_prefix', 'admin').'/pages') }}">--}}
                                {{--<i class="fa fa-newspaper-o">--}}
                                {{--</i> <span>Страницы</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}

                {{--</li>--}}


                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/seo') }}">
                        <i class="fa fa-coffee"></i>
                        <span>SEO данные</span>
                    </a>
                </li>
                <li>
                    <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/elfinder') }}">
                        <i class="fa fa-files-o"></i> <span>Файловая система</span>
                    </a>
                </li>

                <!-- ======================================= -->
                {{--<li class="header">Пользователи</li>--}}
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/users') }}">
                        <i class="fa fa-user"></i> <span>Все пользователи</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i> <span>Контент</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="">
                            <a href="#"><i class="fa fa-circle-o"></i> На главной
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li>
                                    <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/feedback') }}">
                                        <i class="fa fa-files-o"></i> <span>Отзывы</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/manual') }}">
                        <i class="fa fa-files-o"></i> <span>Инструкции</span>
                    </a>
                </li>



                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/banner') }}">
                        <i class="fa fa-user"></i> <span>Банеры</span>
                    </a>
                </li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}">
                        <i class="fa fa-user"></i> <span>Все пользователи</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i> <span>Списки</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/type-work') }}">
                                <i class="fa fa-files-o"></i> <span>Типы работ</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/academic-level') }}">
                                <i class="fa fa-files-o"></i> <span>Учебная степень</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/science') }}">
                                <i class="fa fa-files-o"></i> <span>Науки</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/discipline') }}">
                                <i class="fa fa-files-o"></i> <span>Дисциплины</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/subject') }}">
                                <i class="fa fa-files-o"></i> <span>Предметы</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/faculty') }}">
                                <i class="fa fa-files-o"></i> <span>Название факультета</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/speciality') }}">
                                <i class="fa fa-files-o"></i> <span>Специальности</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{ url(config('backpack.base.route_prefix', 'admin') . '/town') }}">
                                <i class="fa fa-files-o"></i> <span>Города</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/sitesettings') }}">
                        <i class="fa fa-gears"></i> <span>Настройки приложения</span>
                    </a>
                </li>


            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
@endif
