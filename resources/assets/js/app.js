import axios from 'axios';
import VeeValidate from 'vee-validate';

require('./bootstrap');

window.Vue = require('vue');
window.Vue.use(VeeValidate);
window.axios = axios;
