$(document).ready(function(){
	var i = 0;
	$('input[type="checkbox"]').each(function(){
		if($(this).hasClass('hide')) return;
		if($(this).closest('label').length) {
			var attr = '';
			if($(this).attr('style')) {
				attr += ' style="' + $(this).attr('style') + '"';
			}
			if($(this).attr('class')) {
				attr += ' class="' + $(this).attr('class') + '"';
			}
			$(this).after('<span class="chkbx"'+attr+'></span>');
		} else {
			i++;
			var id = 'chk'+i;
			if($(this).attr('id')) {
				id = $(this).attr('id');
			} else {
				$(this).attr('id', id);
			}
			$(this).after('<label class="chkbx" for="'+id+'"></label>');
		}
		$(this).addClass('disapp');
	});
	i = 0;
	$('input[type="radio"]').each(function(){
		if($(this).hasClass('hide')) return;
		if($(this).closest('label').length) {
			var attr = '';
			if($(this).attr('style')) {
				attr += ' style="' + $(this).attr('style') + '"';
			}
			if($(this).attr('class')) {
				attr += ' class="' + $(this).attr('class') + '"';
			}
			$(this).after('<span class="rdobx"'+attr+'></span>');
		} else {
			i++;
			var id = 'rdo'+i;
			if($(this).attr('id'))  {
				id = $(this).attr('id');
			} else {
				$(this).attr('id', id);
			}
			$(this).after('<label class="rdobx" for="'+id+'"></label>');
		}
		$(this).addClass('disapp');
	});
	if($('select').length) $('select').select2({minimumResultsForSearch: 10});
	var authorsCnt = 6,
			latestCnt = 4,
			reviewsCnt = 0,
			reviewsPos = 2;
	if($('#authors').length) {
		$('#authors a[href="#prev"]').click(function(){
			$('#authors .opacity40').removeClass('opacity40');
			$('#authors .opacity70').removeClass('opacity70');
			var tmp = $('#authors .row > div:last-child').removeClass('hidden').addClass('opacity40').remove();
			$('#authors .row > div:nth-child('+authorsCnt+')').addClass('hidden');
			$('#authors .row > div:first-child').addClass('opacity70');
			$('#authors .row').prepend(tmp);
			$('#authors .row > div:nth-child('+(authorsCnt-1)+')').addClass('opacity70');
			$('#authors .row > div:nth-child('+authorsCnt+')').addClass('opacity40');
			return false;
		});
		$('#authors a[href="#next"]').click(function(){
			$('#authors .opacity40').removeClass('opacity40');
			$('#authors .opacity70').removeClass('opacity70');
			var tmp = $('#authors .row > div:first-child').removeClass('opacity40').addClass('hidden').remove();
			$('#authors .row > div:first-child').addClass('opacity40');
			$('#authors .row > div:nth-child(2)').addClass('opacity70');
			$('#authors .row > div:nth-child('+(authorsCnt-1)+')').addClass('opacity70');
			$('#authors .row > div:nth-child('+authorsCnt+')').removeClass('hidden').addClass('opacity40');
			$('#authors .row').append(tmp);
			return false;
		});
	}
	if($('#latest').length) {
		$('#latest a[href="#prev"]').click(function(){
			$('#latest .opacity50').removeClass('opacity50');
			var tmp = $('#latest .row > div:last-child').removeClass('hidden').addClass('opacity50').remove();
			$('#latest .row > div:nth-child('+latestCnt+')').addClass('hidden');
			$('#latest .row').prepend(tmp);
			$('#latest .row > div:nth-child('+latestCnt+')').addClass('opacity50');
			return false;
		});
		$('#latest a[href="#next"]').click(function(){
			$('#latest .opacity50').removeClass('opacity50');
			var tmp = $('#latest .row > div:first-child').removeClass('opacity50').addClass('hidden').remove();
			$('#latest .row > div:first-child').addClass('opacity50');
			$('#latest .row > div:nth-child('+latestCnt+')').removeClass('hidden').addClass('opacity50');
			$('#latest .row').append(tmp);
			return false;
		});
	}
	if($('#reviews').length) {
		reviewsCnt = $('#reviews .images img').length;
		$('#reviews a[href="#prev"]').click(function(){
			$('#reviews .opacity45').removeClass('opacity45');
			var tmp = $('#reviews img:last-child').removeClass('hidden').addClass('opacity45').remove();
			$('#reviews img:nth-child(3)').addClass('hidden');
			$('#reviews .images').prepend(tmp);
			$('#reviews img:nth-child(3)').addClass('opacity45');
			reviewsPos--;
			if(reviewsPos<1) reviewsPos = reviewsCnt;
			$('#reviews .review').fadeOut(100, function(){
				$('#reviews .review').addClass('hidden');
				$('#reviews .review:nth-child('+(reviewsPos+1)+')').removeClass('hidden').fadeIn(100);
			});
			return false;
		});
		$('#reviews a[href="#next"]').click(function(){
			$('#reviews .opacity45').removeClass('opacity45');
			var tmp = $('#reviews img:first-child').removeClass('opacity45').addClass('hidden').remove();
			$('#reviews img:first-child').addClass('opacity45');
			$('#reviews img:nth-child(3)').removeClass('hidden').addClass('opacity45');
			$('#reviews .images').append(tmp);
			reviewsPos++;
			if(reviewsPos>reviewsCnt) reviewsPos = 1;
			$('#reviews .review').fadeOut(100, function(){
				$('#reviews .review').addClass('hidden');
				$('#reviews .review:nth-child('+(reviewsPos+1)+')').removeClass('hidden').fadeIn(100);
			});
			return false;
		});
	}
	$(window).resize(function(){
		if($(window).width()<766) {
			authorsCnt = 2;
			latestCnt = 1;
		} else if(($(window).width()>=766 && $(window).width()<992)) {
			authorsCnt = 3;
			latestCnt = 2;
		} else if(($(window).width()>=992 && $(window).width()<1200)) {
			authorsCnt = 4;
			latestCnt = 2;
		} else {
			authorsCnt = 6;
			latestCnt = 4;
		}
		if($('#authors').length) {
			$('#authors .row > div').addClass('hidden');
			$('#authors .opacity40').removeClass('opacity40');
			$('#authors .opacity70').removeClass('opacity70');
			for(var i=1;i<=authorsCnt;i++) {
				$('#authors .row > div:nth-child('+i+')').removeClass('hidden');
			}
			if(authorsCnt>2) {
				$('#authors .row > div:first-child').addClass('opacity40');
				$('#authors .row > div:nth-child(2)').addClass('opacity70');
				$('#authors .row > div:nth-child('+(authorsCnt-1)+')').addClass('opacity70');
				$('#authors .row > div:nth-child('+authorsCnt+')').addClass('opacity40');
			}
		}
		if($('#latest').length) {
			$('#latest .row > div').addClass('hidden');
			$('#latest .opacity50').removeClass('opacity50');
			for(var i=1;i<=latestCnt;i++) {
				$('#latest .row > div:nth-child('+i+')').removeClass('hidden');
			}
			if(latestCnt>2) {
				$('#latest .row > div:first-child').addClass('opacity50');
				$('#latest .row > div:nth-child('+latestCnt+')').addClass('opacity50');
			}
		}
	}).resize();
	if($('#slider-line').length) {
		$("#slider-line").slider({
			'tooltip_split': true,
			'tooltip_position': 'bottom'
		});
	}
});