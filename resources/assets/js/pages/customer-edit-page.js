function buildFormData(dictionary) {
    let data = new FormData();
    Object.keys(dictionary).forEach(key => data.append(key, dictionary[key]));
    return data;
}

const profileEdit = new Vue({
    el: '.profileEdit',
    data: {
        disabledButton: false,
        phone: '',

        qwe: '',
        allSelected: false,

        completeMain: false,
        completeEducation: false,
        completeContact: false,
        completePassword: false,

        messageComplete: '',
        totalField: 0,
        cntField: 0,
        flagAddField: true,
        sampleElement: ' <div class="form-group col-xs-12 col-md-4"><label>Телефон</label> ' +
            '<div class="row">' +
            '<div class="col-xs-12">' +
            '<input type="text" placeholder="Телефон" name="phone_user[]" value="" class="form-control phoneNumber"></div></div></div>',
    },



    methods: {
        validateBeforeSubmitCustomerEditMain(e) {
            e.preventDefault();

            const dataCustomerEditMain = {
                'name_customer': this.$refs.editName.value,
                'surname_customer': this.$refs.editLastName.value,
                'login_customer': this.$refs.editLogin.value,
            };

            this.$validator.validateAll(dataCustomerEditMain).then(result => {
                if (result) {
                    this.ajaxEditCustomerMain();
                }
            });
        },

        ajaxEditCustomerMain() {
            const data = buildFormData({
                'name': this.$refs.editName.value,
                'surname': this.$refs.editLastName.value,
                'login': this.$refs.editLogin.value,
                'birthday': this.$refs.editBirthday.value,
                'bio': this.$refs.editBio.value
            });

            axios.post('/ajax/edit-customer/main', data).then((response) => {
                var data = response.data.data;

                console.log(data);
                if (response.data.result) {
                    this.completeMain = true;
                    this.clearText('Вы успешно отредактировали основную информацию');
                } else {
                    this.disabledButton = true;
                    this.authError = data;
                }
            }).catch(response => {
                Object.keys(response.response.data).forEach(test => {
                    this.errors.add(test + '_customer', 'Ошибка в поле');
                });
            });
        },
        ajaxEditCustomerEducation() {
            const data = new FormData();
            data.append('university', this.$refs.editUniversity.value);
            data.append('study_finish_date', this.$refs.editFinishStudy.value);
            data.append('session_begin_date', this.$refs.editSessionBegin.value);
            data.append('specialty', this.$refs.editSpeciality.value);
            data.append('faculty', this.$refs.editFaculty.value);

            axios.post('/ajax/edit-customer/education', data).then((response) => {
                var data = response.data.data;

                console.log(data);

                if (response.data.result) {
                    this.completeMain = true;
                    this.clearText('Вы успешно отредактировали основную информацию');
                } else {
                    this.disabledButton = true;
                    this.authError = data;
                }
            }).catch(response => {
                Object.keys(response.response.data).forEach(test => {
                    this.errors.add(test + '_customer', 'Ошибка в поле');
                });
            });
        },
        //================ EditPassword
        validateBeforeSubmitCustomerEditPassword(e) {
            e.preventDefault();

            const dataCustomerEditPassword = {
                'old_password': this.$refs.editOldPassword.value,
                'password': this.$refs.editNewPassword.value,
                'password_confirmation': this.$refs.editPasswordConfirmation.value,
            };

            this.$validator.validateAll(dataCustomerEditPassword).then(result => {
                if (result) {
                    this.disabledButton = true;
                    this.ajaxEditCustomerPassword();
                }
            });
        },
        ajaxEditCustomerPassword() {
            const data = new FormData();
            data.append('old_password', this.$refs.editOldPassword.value);
            data.append('password', this.$refs.editNewPassword.value);
            data.append('password_confirmation', this.$refs.editPasswordConfirmation.value);

            axios.post('/ajax/edit-customer/password', data).then((response) => {
                var data = response.data.data;

                console.log(response);
                if (response.data.result) {
                    this.completePassword = true;
                    this.$refs.editOldPassword.value = '';
                    this.$refs.editNewPassword.value = '';
                    this.$refs.editPasswordConfirmation.value = '';

                    this.clearText('Вы успешно отредактировали пароль');
                } else {
                    this.disabledButton = true;
                }
            }).catch(response => {
                if (response.response.data.old_password[0] != '') {
                    this.errors.add('old_password', response.response.data.old_password[0]);
                }
                this.disabledButton = false;
            });
        },
        //=========== Contactdata
        validateBeforeSubmitCustomerEditContact(e) {
            e.preventDefault();
            const dataCustomerEditContact = {
                'email_author': this.$refs.editEmail.value,
            };

            this.$validator.validateAll(dataCustomerEditContact).then(result => {
                if (result) {
                    this.disabledButton = true;
                    this.ajaxEditCustomerContact();
                }
            });
        },
        addFieldPhoneNumber(e) {
            e.preventDefault();
            var element = $('#fields').append(this.sampleElement);
            this.cntField = this.totalField++;

            if (this.cntField == 4) {
                this.flagAddField = false;
            }
        },

        ajaxEditCustomerContact() {
            const data = new FormData();
            data.append('email', this.$refs.editEmail.value);

            $('.phoneNumber').each(function(key, val) {
                data.append('phone[' + key + ']', $(val).val());
            });

            axios.post('/ajax/edit-customer/contact', data).then((response) => {
                var data = response.data.data;

                if (response.data.result) {
                    this.completeContact = true;
                    this.clearText('Вы успешно отредактировали контактные данные');
                } else {
                    this.disabledButton = true;
                }
            }).catch(response => {
                this.disabledButton = false;
            });
        },

        clearText(str) {
            this.disabledButton = false;
            this.messageComplete = str;

            setTimeout(() => {
                this.changeFlagMessage();
                this.messageComplete = '';
            }, 2000);
        },
        changeFlagMessage() {
            this.completeEdit = false;
            this.completeMain = false;
            this.completeEdit = false;
            this.completeEducation = false;
            this.completePassword = false;
            this.completeContact = false;
        },


    },
});