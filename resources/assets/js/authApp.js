// import Form from './components/Form.vue';
// import InputTextComponent from './components/InputTextComponent.vue';
// import InputDatePickerComponent from './components/InputDatePickerComponent.vue';
// import TextAreaComponent from './components/TextAreaComponent.vue';
// import ButtonComponent from './components/ButtonFormComponent.vue';

// const myForm = new Vue({
//     el:'#myForm',
//     data:{
//         testParent:'testing'
//     },
//     created:{
//
//     },
//     components:{
//         'basic-data-form':Form,
//     }
// });

/////=======================
const app = new Vue({
    el: '#authForms',
    data: {
        emailError: '',
        passError: '',
        disabledButton: false,
        disabledRegButton: false,
        formError: '',
        regFormError: '',

        regEmail: '',
        regName: '',
        regPhone: '',
        regPass: '',
        regRepeatPass: '',
        regLogin: '',
        regSurname: '',
        regAcademy: '',
        regYear: '',
        regSpec: '',
        regEndYear: '',

        regRules:0,
        regNotifyAndNews: 0,
        regAgency:0,
        regNotifyStudent:0,

        regNotifyAndNewsError: '',
        regNotifyStudentError:'',



        regAuthorFormError:'',
        regAuthorName:'',
        regAuthorFamily:'',
        regAuthorEmail:'',
        regAuthorLogin:'',
        regAuthorPass:'',
        regAuthorRepeatPass:'',
        regAuthorPhone:'',
        regAuthorUniversity:'',
        regAuthorSpeciality:'',
        regAuthorRules:'',
        regAuthorNotifyAndNews:'',

        authEmail:'',
        authError:'',
        authPassword:'',

        searchQuery: '',
        searchQueryIsDirty: false,
        isCalculating: false
    },
    watch: {
        authEmail: function () {
            this.disabledButton = false
        },
        authPassword: function () {
            this.disabledButton = false
        }
    },
    methods: {
        createFormData(){
            const data = new FormData();

            Object.keys(this.data).forEach(name => {
                name && this.data[name] && form.data(name, this.data[name]);
            });

            return form;
        },


        //////========Auth
        validateAuthBeforeSubmit(e){
            e.preventDefault();

            const dataAuth = {
                'auth_email':this.authEmail,
                'auth_password':this.authPassword,
            };
            this.$validator.validateAll(dataAuth).then(result => {
                if (result) {
                    this.ajaxLoginUser();
                }
                this.disabledButton = true;
            });
        },
        ajaxLoginUser () {
            // this.disabledButton = true;
            const data = new FormData();
            data.append('email', this.authEmail);
            data.append('password', this.authPassword);

            axios.post('/ajax-login', data).then((response) => {
                var data = response.data.data;
                if(response.data.result) {
                    document.location = data;
                } else {
                    this.disabledButton = true;
                    this.authError = data;
                }
            });
                //.catch((error) => console.log(error.response.data))
        },

        //////========Register Author
        validateBeforeSubmit(e) {
            e.preventDefault();

            const dataAuthor = {
                'name_author':this.regAuthorName,
                'family':this.regAuthorFamily,
                'email':this.regAuthorEmail,
                'login':this.regAuthorLogin,
                'password':this.regAuthorPass,
                'password_confirmation':this.regAuthorRepeatPass,
                'rules':this.regAuthorRules,
            };

            this.$validator.validateAll(dataAuthor).then(result => {
                if (result) {
                    this.ajaxRegisterAuthor();
                }
            });
        },
        ajaxRegisterAuthor () {
            const data = new FormData();

            data.set('name', this.regAuthorName);
            data.set('surname', this.regAuthorFamily);
            data.set('email', this.regAuthorEmail);
            data.set('login', this.regAuthorLogin);
            data.set('password', this.regAuthorPass);
            data.set('phone', this.regAuthorPhone);
            data.set('city', this.getValueInSelectById('town'));
            data.set('university', this.regAuthorUniversity);
            data.set('specialty', this.regAuthorSpeciality);
            data.set('study_finish_date', this.getValueInSelectById('endLearnYear'));
            data.set('mail_subscription', this.getIntegerValueByBoolean(this.regAuthorNotifyAndNews));
            data.set('role', '2');

            axios.post('/ajax-register', data).then((response) => {
                if(response.data.result) {
                    window.location.assign('/profile')
                } else {
                    this.regAuthorFormError = data;
                }
            }).catch(response => {
                this.regAuthorFormError = response.response.data;

                // this.errors.add('old_password',response.response.data.old_password[0]);
            });
        },
        validateBeforeSubmitCustomer(e){
            e.preventDefault();
            const dataStudent = {
                'name_student':this.regName,
                'login_student':this.regLogin,
                'email_student':this.regEmail,
                'password_student':this.regPass,
                'password_confirmation_student':this.regRepeatPass,
                'rules_student':this.regRules,
            };

            console.log(dataStudent);
            this.$validator.validateAll(dataStudent).then(result => {
                if (result) {
                    this.ajaxRegisterStudent();
                }
            });
        },
        ajaxRegisterStudent () {
            const data = new FormData();

            data.set('name', this.regName);
            data.set('email', this.regEmail);
            data.set('login', this.regLogin);
            data.set('password', this.regPass);
            data.set('phone', this.regPhone);
            data.set('role', '1');
            data.set('is_agent', '1');
            data.set('mail_subscription', this.getIntegerValueByBoolean(this.regNotifyAndNews));
            data.set('mail_message_notify', this.getIntegerValueByBoolean(this.regNotifyStudent));

            axios.post('/ajax-register', data).then((response) => {
                if(response.data.result) {
                    window.location.assign('/profile')
                } else {
                    this.regFormError = data;
                }
            }).catch(response => {
                this.regFormError = response.response.data;
            });
        },
        ///========================

        getValueInSelectById(id){
            // const  element =  document.getElementById(id);
            // // element = document.getElementById(id);
            // value = element.options[element.selectedIndex].value;

            return $('#' + id).val();
            // return value;
        },
        getIntegerValueByBoolean(bool){
            return bool ? 1 : 0
        },

        ////====
        validateLoginFileds(email, password) {
            if (!email && !password) {
                this.emailError = 'Необходимо заполнить "E-mail"';
                this.passError = 'Необходимо заполнить "Пароль"';
                this.disabledButton = false;
                return false;
            }

            if (!email) {
                this.emailError = 'Необходимо заполнить "E-mail"';
                this.disabledButton = false;
                return false;
            }

            if (!this.emailValidation(email)) {
                this.emailError = 'Не валидный "E-mail"';
                this.disabledButton = false;
                return false;
            }

            if (!password) {
                this.passError = 'Необходимо заполнить "Пароль"';
                this.disabledButton = false;
                return false;
            }

            return true;
        },
        clearLoginErrors(){
            this.emailError = '';
            this.passError = '';
            this.formError = '';
        },
        clearRegisterErrors(){
            this.emailError = '';
            this.passError = '';
            this.formError = '';
        },
        emailValidation(email){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        toggleModals(currentModal){
            if(currentModal == 'login') {
                $('#login').modal('hide');
                $('#register').modal('show');
            } else {
                $('#login').modal('show');
                $('#register').modal('hide');
            }
        }
    }
});