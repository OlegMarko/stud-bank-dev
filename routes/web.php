<?php
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** ============================FRONTEND============================ */
Route::group([
    'namespace' => 'Main',
    'middleware' => 'web'
], function () {
    /*
     * Main routes...
     */
    Route::get('/', 'MainController@index');
//    Route::group(['middleware'=>['auth']], function () {

    Route::get('/care', 'CareController@index');
    Route::get('/checkout', 'CheckoutController@index');
    Route::get('/last', 'LastController@index');
    Route::get('/order', 'OrderController@index');
    Route::get('/order-success', 'OrderController@success');
    Route::get('/payment', 'PaymentController@index');


    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'ProfileController@index');
        Route::get('/options', 'ProfileOptionController@index');
//        Route::get('/bill', 'ProfileBillController@index');
//        Route::get('/messages', 'ProfileMessagesController@index');
//        Route::get('/orders', 'ProfileOrdersController@index');
//        Route::get('/pro', 'ProfileProController@index');
//        Route::get('/profile', 'ProfileProfileController@index');
//        Route::get('/promo', 'ProfilePromoController@index');
//        Route::get('/search/orders', 'ProfileSearchController@index');
//        Route::get('/works', 'ProfileWorksController@index');
    });

    Route::get('affiliate', 'AffiliateController@index');
    Route::get('authors', 'MainController@authors');
    Route::get('advices', 'MainController@advices');

    Route::group(['prefix' => 'manual'], function () {
        Route::get('/', 'ManualController@index');
        Route::get('/{url}', 'ManualController@view');
    });

//    });
    /*
     * Ajax routes...
     */
    /** ============================BACKEND============================ */
    Route::group([
        'middleware' => ['ajax'],
    ],function (){

        Route::post('ajax/edit-author/{action}', 'AuthorOptionController@doAction');
        Route::post('ajax/edit-customer/{action}', 'CustomerOptionController@doAction');
//        Route::post('ajax/edit-author/{action}', 'AuthorOptionController@edit');

    });

    Route::post('ajax-login', 'AuthAjaxController@login')->name('ajaxLogin');
    Route::post('ajax-register', 'RegisterAjaxController@register');

//    Route::post('ajax/edit/author/{what}', 'AuthorOptionController@edit');

    Route::get('/logout', function () {
        Auth::logout();
        return redirect('/');
    });
});

/** --------------------------------------------------------------- */


/** ============================BACKEND============================ */
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['admin'],
    'namespace' => 'Admin'
], function () {

    CRUD::resource('banner', 'BannerCrudController');
    CRUD::resource('seo', 'SeoCrudController');
    CRUD::resource('sitesettings', 'SiteSettingsCrudController');
    CRUD::resource('users', 'UsersCrudController');
    CRUD::resource('feedback', 'FeedbackCrudController');
    CRUD::resource('manual', 'ManualCrudController');
    CRUD::resource('type-work', 'TypeWorkCrudController');
    CRUD::resource('academic-level', 'AcademicLevelCrudController');
    CRUD::resource('faculty', 'FacultyCrudController');
    CRUD::resource('speciality', 'SpecialityCrudController');
    CRUD::resource('town', 'TownCrudController');
    CRUD::resource('science', 'ScienceCrudController');
    CRUD::resource('discipline', 'DisciplineCrudController');
    CRUD::resource('subject', 'SubjectCrudController');
});

/** --------------------------------------------------------------- */


/** ============================OTHER============================= */
/*
 * Seo redirect rout
 */
Route::get('redirect', function () {
    return redirect()->to(Input::get('go'));
});

/** --------------------------------------------------------------- */